package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/prime"
)

var (
	n          = flag.Int("n", 100, "find totient numbers up to n")
	v          = flag.Bool("v", false, "verbose")
	sieve_size = flag.Int("sieve_size", 1000000, "size of prime sieve")
)

func NaivePhi(n int) int {
	sum := 0
	for i := 1; i < n; i++ {
		gcd := prime.Gcd(n, i)
		if gcd == 1 {
			sum++
		}
	}
	return sum
}

func FactorPhi(n int, s *prime.Sieve) int {
	// phi(n) - n . (1-1/p1) . (1-1/p2) for prime factors of n
	factors, _ := s.Divisors(n)
	// log.Print(factors)
	product := 1.0
	for p, _ := range factors {
		product *= (1.0 - 1/float64(p))
	}
	product *= float64(n)
	return int(product)
}

func FactorNPhi(n int, s *prime.Sieve) float64 {
	// phi(n) - n . (1-1/p1) . (1-1/p2) for prime factors of n
	// return n/phi(n) -> 1/phi n
	factors, _ := s.Divisors(n)
	// log.Print(factors)
	product := 1.0
	for p, _ := range factors {
		product *= (1.0 - 1/float64(p))
	}
	return 1.0 / product
}

func main() {
	flag.Parse()
	s := prime.NewSieve(*sieve_size)
	max_nphi := 0.0
	max_phi := 0
	max_n := 0
	for i := 2; i < *n; i++ {
		phi := FactorPhi(i, s)
		// phi := FactorPhi(i, s)
		nphi := float64(i) / float64(phi)
		if *v {
			fmt.Println(phi, nphi)
		}
		if nphi > max_nphi {
			max_nphi = nphi
			max_phi = phi
			max_n = i
		}
	}
	factors, _ := s.Divisors(max_n)
	fmt.Printf("max: %v (%v) %v %v\n", max_n, factors, max_phi, max_nphi)
}
