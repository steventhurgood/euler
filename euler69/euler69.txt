Euler's Totient function, φ(n) [sometimes called the phi function], is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.

n	Relatively Prime	φ(n)	n/φ(n)
2	1	1	2
3	1,2	2	1.5
4	1,3	2	2
5	1,2,3,4	4	1.25
6	1,5	2	3
7	1,2,3,4,5,6	6	1.1666...
8	1,3,5,7	4	2
9	1,2,4,5,7,8	6	1.5
10	1,3,7,9	4	2.5
It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.

Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.

*****

Brute force totient function:

func Phi(n int) int {
sum := 0 
  for i:=2; i < n; i++ {
  if gcd(n, i) == 1 {
sum++ 
}
  }

Other potential approach, a Totient object that pre-computes values.

naive approach here:

totient := make([]int, max)
for i:=0 ; i < max; i++ {
totient[i] = i
}
for i:= 2; i< max; i++ {
  j:= 2
  for {
    k := i*j
    totient[i]--
    j++
    if j > max {
      break
    }
  } 
}
