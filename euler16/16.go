package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"strconv"
)

type BigNum []byte

func (b *BigNum) String() string {
	reversed := make([]byte, len(*b))
	end := len(*b)
	for i := 0; i < end; i++ {
		reversed[i] = '0' + (*b)[end-1-i]
	}
	buf := bytes.NewBuffer(reversed)
	return buf.String()
}

func (b *BigNum) Double() BigNum {
	// 119 * 2 = 238
	n := make(BigNum, 0, len(*b)+1)
	carry := byte(0)
	part := byte(0)
	doubledPart := byte(0)
	for _, v := range *b {
		doubledPart = 2 * v
		part = doubledPart % 10
		n = append(n, part+carry)
		carry = (doubledPart - part) / 10
	}
	if carry > 0 {
		n = append(n, carry)
	}
	return n
}

func NewBigNum(n int64) BigNum {
	b := make(BigNum, 0, 100)
	for n > 0 {
		part := n % 10
		b = append(b, byte(part))
		n = (n - part) / 10
	}
	return b
}

func BigPower(n int64) BigNum {
	number := NewBigNum(1)
	for n > 0 {
		number = number.Double()
		n--
	}
	return number
}
func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		argNum, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		n := BigPower(argNum)
		sum := 0
		for _, v := range n {
			sum += int(v)
		}
		fmt.Println(n.String())
		fmt.Println("sum =", sum)
	}
}
