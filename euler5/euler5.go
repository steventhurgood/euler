package main

import (
	"flag"
	"fmt"
	// "log"
)

func SmallestMultiple(n int) int {
	divisable := false
	r := 1
	for ; !divisable; r++ {
		divisable = true
		for i := 1; i <= n; i++ {
			if r%i != 0 {
				divisable = false
				break
			} else {
				// log.Print(r, "/", i)
			}
		}
		if divisable {
		 return r
		}

	}
	return r
}

func main() {
	n := flag.Int("n", 10, "number")
	flag.Parse()

	d := SmallestMultiple(*n)
	fmt.Println(*n, d)
}
