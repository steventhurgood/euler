package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"sort"
	"strconv"
)

func (s *Sieve) Primes() []int {
	r := make([]int, 0, 1024)
	for i, divisable := range s.divisable {
		if !divisable {
			r = append(r, i)
		}
	}
	return r
}

type Sieve struct {
	divisable []bool
}

func NewSieve(n int) *Sieve {
	s := &Sieve{}
	s.divisable = make([]bool, n)
	s.divisable[0] = true
	s.divisable[1] = true
	s.divisable[2] = false
	maxDivisor := int(math.Ceil(math.Sqrt(float64(n))))
	for i := 2; i <= maxDivisor; i++ {
		if !s.divisable[i] {
			for j := i * 2; j < n; j += i {
				s.divisable[j] = true
			}
		}
	}
	return s
}

func (s *Sieve) NumDivisors(n int) int {
	p := s.Primes()
	i := 0
	numPrimes := len(p)
	numDivisors := 1
	for {
		maxDivisor := int(math.Ceil(math.Sqrt(float64(n))))
		if i > maxDivisor {
			break
		}
		if i >= numPrimes {
			log.Fatal("Insufficient primes to factor:", n)
		}
		if n%p[i] == 0 {
			pow := 1
			for n%p[i] == 0 && n > 0 {
				pow++
				n = n / p[i]
			}
			numDivisors *= pow
		}
		i++
	}
	return numDivisors
}

func (s *Sieve) Divisors(n int) (map[int]int, int) {
	if n == 0 {
		return map[int]int{}, 0
	}
	p := s.Primes()
	divisors := make(map[int]int)
	i := 0
	numPrimes := len(p)
	for {
		if n == 1 {
			break
		}
		if i >= numPrimes {
			log.Fatal("Insufficient primes to factor:", n)
		}
		if n%p[i] == 0 {
			divisors[p[i]]++
			n = n / p[i]
		} else {
			i++
		}
	}
	numDivisors := 1
	for _, powers := range divisors {
		numDivisors *= (powers + 1)
	}
	return divisors, numDivisors
}

func subFactorsToDivisors(primes []int, factors map[int]int) []int {
	numDivisors := 1
	for _, powers := range factors {
		numDivisors *= (powers + 1)
	}
	divisors := make([]int, 0, numDivisors)
	if len(primes) == 0 {
		return []int{1}
	}
	prime := primes[0]
	maxPower := factors[prime]
	remainingDivisors := subFactorsToDivisors(primes[1:], factors)
	for i := 0; i <= maxPower; i++ {
		pow := int(math.Pow(float64(prime), float64(i)))
		for _, j := range remainingDivisors {
			divisors = append(divisors, pow*j)
		}
	}
	return divisors
}

// Take a map of prime factors/powers and expand it to divisors
func FactorsToDivisors(factors map[int]int) []int {
	primes := make([]int, 0, len(factors))
	for prime, _ := range factors {
		primes = append(primes, prime)
	}
	divisors := subFactorsToDivisors(primes, factors)
	sort.Ints(divisors)
	return divisors
}

func Sum(list []int) int {
	sum := 0
	for _, n := range list {
		sum += n
	}
	return sum
}

func (s *Sieve) ProperDivisors(a int) []int {
	factors, _ := s.Divisors(a)
	divisors := FactorsToDivisors(factors)
	return divisors[:len(divisors)-1]
}

func (s *Sieve) AmicablesUpTo(n int) []int {
	tested := make(map[int]bool)
	amicables := make([]int, 0, n)
	for i := 1; i < n; i++ {
		if tested[i] {
			continue
		}
		a, b, amicable := s.Amicable(i)
		if amicable {
			tested[a] = true
			tested[b] = true
			amicables = append(amicables, a)
			amicables = append(amicables, b)
		}
	}
	sort.Ints(amicables)
	return amicables
}

func (s *Sieve) Amicable(a int) (int, int, bool) {
	// log.Print("Amicable(", a, ")")
	divisors := s.ProperDivisors(a)
	// log.Print("Divisors: ", divisors)
	sum := Sum(divisors)
	// log.Print("Sum: ", sum)
	sumDivisors := s.ProperDivisors(sum)
	// log.Print("Sum Divisors: ", sumDivisors)
	sumSum := Sum(sumDivisors)
	// log.Print("Sum Sum: ", sumSum)
	return a, sum, a == sumSum && a != sum
}

func main() {
	sieveSize := flag.Int("sieve_size", 1000, "the size of the sieve to use for finding primes")
	upTo := flag.Int("upTo", -1, "Count amicable numbers up to this value")
	flag.Parse()
	s := NewSieve(*sieveSize)
	if *upTo > 0 {
		log.Print("up to ", *upTo)
		amicables := s.AmicablesUpTo(*upTo)
		sum := 0
		for _, v := range amicables {
			if v < *upTo {
				sum += v
			}
		}
		fmt.Println(amicables)
		fmt.Println(sum)
	}
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		a, b, amicable := s.Amicable(int(n))
		aDivisors := s.ProperDivisors(a)
		bDivisors := s.ProperDivisors(b)
		fmt.Println(a, aDivisors)
		fmt.Println(b, bDivisors)
		fmt.Println("Amicable:", amicable)
	}
}
