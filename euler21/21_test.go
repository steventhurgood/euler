package main

import (
	"testing"
)

func Test_Divisors(t *testing.T) {
	s := NewSieve(1000)
	// Divisors(10) = 1, 2, 5, 10; 2^1 * 5^1
	divisors, num := s.Divisors(10)
	if num != 4 {
		t.Error("10 should have 4 divisors, got:", num, divisors)
	}
	if len(divisors) != 2 {
		t.Error("Divisors of 10 should be map[2:1 5:1]. Got", divisors)
	}
	if divisors[2] != 1 {
		t.Error("Divisors of 10 should be map[2:1 5:1]. Got", divisors)
	}
	if divisors[5] != 1 {
		t.Error("Divisors of 10 should be map[2:1 5:1]. Got", divisors)
	}
}

func Test_Sum(t *testing.T) {
	n := Sum([]int{})
	if n != 0 {
		t.Error("Sum(0) expected 0. Got", n)
	}
	n = Sum([]int{1, 2, 3})
	if n != 6 {
		t.Error("Sum([1,2,3]) expected 6. Got", n)
	}
}

func Test_ProperDivisors(t *testing.T) {
	s := NewSieve(1000)
	divisors := s.ProperDivisors(1)
	if len(divisors) != 0 {
		t.Error("expected divisors of 1 to be [], got", divisors)
	}
	divisors = s.ProperDivisors(10)
	if len(divisors) != 3 {
		t.Error("expected divisors of 10 to be [1, 2, 5], got", divisors)
	}
}

func BenchmarkAmicable(b *testing.B) {
	b.StopTimer()
	s := NewSieve(20000)
	b.StartTimer()
	for i := 1; i < b.N; i++ {
		s.Amicable(i)
	}
}

func Test_Amicable(t *testing.T) {
	s := NewSieve(1000)
	a, b, amicable := s.Amicable(220)
	if !amicable {
		t.Error("Expected 220, 284 to be amicable. Got:", a, b, amicable)
	}
	if a != 220 || b != 284 {
		t.Error("Expected 220, 284 to be amicable. Got:", a, b, amicable)
	}
}
