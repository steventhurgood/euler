package gaussian

import (
	"math"
	"testing"
)

const (
	testEpsilon = 0.0001
)

type NormTest struct {
	p, want float64
}

func TestPNorm(t *testing.T) {
	tests := []NormTest{
		NormTest{0.001, -3.090232},
		NormTest{0.025, -1.959964},
		NormTest{0.1, -1.281552},
		NormTest{0.5, 0},
		NormTest{0.75, 0.6744898},
		NormTest{0.975, 1.959964},
	}

	for _, test := range tests {
		got := PNorm(test.p)
		if math.Abs(got-test.want) > testEpsilon {
			t.Errorf("PNorm(%v) got: %v, want: %v", test.p, got, test.want)
		}
	}
}

type WilsonTest struct {
	p                    float64
	n                    int
	confidence           float64
	lowerWant, upperWant float64
}

func TestWilson(t *testing.T) {
	tests := []WilsonTest{
		WilsonTest{0.5, 40, 0.95, 0.3406, 0.6594},
	}
	for _, test := range tests {
		lowerGot, upperGot := WilsonInterval(test.p, test.n, test.confidence)
		if lowerGot != test.lowerWant || upperGot != test.upperWant {
			t.Errorf("WilsonInterval(%v, %v, %v) got: (%v, %v) want: (%v, %v)", test.p, test.n, test.confidence, lowerGot, upperGot, test.lowerWant, test.upperWant)
		}
	}
}
