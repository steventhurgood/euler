package main

import (
	"testing"
)

func TestMinimal(t *testing.T) {
	a := Fraction{4, 8}
	minimalA := a.Minimal()
	if !(minimalA.Numerator == 1 && minimalA.Denominator == 2) {
		t.Error("Expected 1/2. Got: ", minimalA)
	}
}

func TestEqual(t *testing.T) {
	a := Fraction{5, 15}
	b := Fraction{1, 3}
	if !a.Equals(b) {
		t.Error("expected ", a, " == ", b)
	}
}

func TestTrivialCancel(t *testing.T) {
	tests := []Fraction{
		Fraction{12, 34},
		Fraction{13, 53},
		Fraction{13, 17},
		Fraction{11, 17},
		Fraction{12, 21},
	}
	expect := [][]Fraction{
		[]Fraction{},
		[]Fraction{
			Fraction{1, 5},
		},
		[]Fraction{
			Fraction{3, 7},
		},
		[]Fraction{
			Fraction{1, 7},
		},
		[]Fraction{
			Fraction{1, 1},
			Fraction{2, 2},
		},
	}

	for i := range tests {
		m := tests[i].TrivialCancel()
		e := expect[i]
		t.Log("trivially simplified: ", m, "expected: ", e)
		if len(m) != len(e) {
			t.Error("expected: ", e, " got ", m)
		}
		for _, result := range m {
			found := false
			for _, expectedResult := range e {
				if result.Equals(expectedResult) {
					found = true
					t.Log(result, expectedResult)
					break
				}
			}
			if !found {
				t.Error("Couldn't find ", m, "in", e)
			}
		}
	}
}
