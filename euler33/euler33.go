package main

import (
	"fmt"

	"bitbucket.org/steventhurgood/euler/prime"
)

type Fraction struct {
	Numerator   int
	Denominator int
}

func (a Fraction) Minimal() Fraction {
	gcd := prime.Gcd(a.Numerator, a.Denominator)
	newA := Fraction{a.Numerator / gcd, a.Denominator / gcd}
	return newA
}

func (a Fraction) Equals(b Fraction) bool {
	minimalA := a.Minimal()
	minimalB := b.Minimal()
	return minimalA.Numerator == minimalB.Numerator && minimalA.Denominator == minimalB.Denominator
}

func Digits(n int) []int {
	digits := []int{}
	for n > 0 {
		digit := n % 10
		digits = append(digits, digit)
		n = (n - digit) / 10
	}
	return digits
}

func (a Fraction) TrivialCancel() []Fraction {
	fractions := []Fraction{}
	numDigits := Digits(a.Numerator)
	denDigits := Digits(a.Denominator)
	lastNum, lastDen := -1, -1
	if a.Numerator%10 == 0 && a.Denominator%10 == 0 {
		return fractions
	}
	for i, a := range numDigits {
		for j, b := range denDigits {
			if a == b && a != lastNum && b != lastDen && denDigits[1-j] != 0 {
				fractions = append(fractions, Fraction{numDigits[1-i], denDigits[1-j]})
			}
			lastNum = b
		}
		lastDen = a
	}
	return fractions
}

func main() {
	fractions := [][]Fraction{}

	for a := 11; a < 100; a++ {
		for b := a + 1; b < 100; b++ {
			fraction := Fraction{a, b}
			trivial := fraction.TrivialCancel()
			for _, t := range trivial {
				if fraction.Equals(t) {
					fractions = append(fractions, []Fraction{fraction, t.Minimal()})
				}
			}
		}
	}
	numProduct := 1
	denProduct := 1
	for _, f := range fractions {
		fmt.Println(f)
		numProduct *= f[1].Numerator
		denProduct *= f[1].Denominator
	}
	product := Fraction{numProduct, denProduct}
	fmt.Println("product: ", product.Minimal())
}
