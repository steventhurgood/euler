package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/prime"
)

func NumChars(n int) []int {
	chars := []int{}
	for n > 0 {
		digit := n % 10
		chars = append(chars, digit)
		n = (n - digit) / 10
	}
	return chars
}

func RotatedNum(chars []int, rotated int) int {
	// num = 123, chars = 3,2,1. rotated = 231;
	// i=0, offset=1
	// i=1, offset=0
	// i=2, offset=2
	l := len(chars)
	n := 0
	for i := 0; i < l; i++ {
		n = n*10 + chars[((l-1-i)+rotated)%l]
	}
	return n
}

func Rotate(n int) []int {
	rotations := []int{n}
	chars := NumChars(n)
	for i := 1; i < len(chars); i++ {
		rotations = append(rotations, RotatedNum(chars, i))
	}
	return rotations
}

func main() {
	upto := flag.Int("upto", 100, "find circular primes up to")
	flag.Parse()
	s := prime.NewSieve(*upto)
	circular := [][]int{}
	for _, p := range s.Primes() {
		rotations := Rotate(p)
		allPrime := true
		for _, r := range rotations {
			if isPrime, err := s.IsPrime(r); !isPrime || err != nil {
				allPrime = false
			}
		}
		if allPrime {
			circular = append(circular, rotations)
		}
	}
	for _, c := range circular {
		fmt.Println(c)
	}
	fmt.Println("count: ", len(circular))
}
