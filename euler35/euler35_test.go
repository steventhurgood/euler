package main

import "testing"

func TestNumChars(t *testing.T) {
	r := NumChars(12345)
	expected := []int{5, 4, 3, 2, 1}
	for i := range r {
		if r[i] != expected[i] {
			t.Error("Got: ", r, " expected: ", expected)
		}
	}
}

func TestRotatedNum(t *testing.T) {
	n := 12345
	chars := NumChars(n)
	r := RotatedNum(chars, 1)
	expected := 51234
	if r != expected {
		t.Error("got: ", r, " expected: ", expected)
	}
}

func TestRotate(t *testing.T) {
	n := 12345
	rotations := Rotate(n)
	expected := []int{
		12345,
		51234,
		45123,
		34512,
		23451,
	}
	for i := range rotations {
		if rotations[i] != expected[i] {
			t.Error("got: ", rotations, "expected: ", expected)
		}
	}
}
