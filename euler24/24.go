package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"sort"
	"strconv"
)

type Permutations [][]byte

func (p Permutations) Len() int {
	return len(p)
}

func (p Permutations) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p Permutations) Less(i, j int) bool {
	// p[i] = [1, 2, 3]
	// p[j] = [1, 2, 1]
	for k := 0; k < len(p[i]); k++ {
		if p[i][k] < p[j][k] {
			return true
		}
		if p[i][k] > p[j][k] {
			return false
		}

	}
	return false
}

// Return a lexical permutation of the characters in data
func LexicalPermute(data []byte) Permutations {
	if len(data) == 0 {
		return Permutations{}
	}
	if len(data) == 1 {
		return Permutations{data}
	}
	permutes := make(Permutations, 0)
	for i, char := range data {
		remainder := make([]byte, len(data)-1)
		j := 0
		for k, otherChar := range data {
			if i != k {
				remainder[j] = otherChar
				j++
			}
		}
		remainderPermute := LexicalPermute(remainder)
		for _, permute := range remainderPermute {
			newPermute := make([]byte, len(data))
			newPermute[0] = char
			for k, otherChar := range permute {
				newPermute[k+1] = otherChar
			}
			permutes = append(permutes, newPermute)
		}
	}
	sort.Sort(permutes)
	return permutes
}

func main() {
	find := flag.Int("find", -1, "the Nth lexical permutation to find")
	flag.Parse()
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		digits := make([]byte, int(n))
		for i := 0; i < int(n); i++ {
			digits[i] = byte(i)
		}
		permutations := LexicalPermute(digits)
		foundPermutation := []byte{}
		for n, permutation := range permutations {
			if n+1 == *find {
				foundPermutation = permutation
			}
			fmt.Println(n+1, permutation)
		}
		for i := range foundPermutation {
			foundPermutation[i] += '0'
		}
		buf := bytes.NewBuffer(foundPermutation)
		fmt.Println("Found:", buf.String())
	}
}
