package main

import (
	"fmt"
	"sort"
	"testing"
)

func TestLexicalPermute(t *testing.T) {
	digits := []byte{0, 1, 2}
	expected := [][]byte{[]byte{0, 1, 2}, []byte{0, 2, 1}, []byte{1, 0, 2}, []byte{1, 2, 0}, []byte{2, 0, 1}, []byte{2, 1, 0}}
	expectedStr := fmt.Sprint(expected)
	permutes := LexicalPermute(digits)
	permuteStr := fmt.Sprint(permutes)
	if expectedStr != permuteStr {
		t.Error("Expected:", expectedStr, ", got:", permuteStr)
	}
}

func TestSort(t *testing.T) {
	// [[0 2 1] [0 1 2] [1 2 0] [1 0 2] [2 1 0] [2 0 1]
	p := Permutations{[]byte{0, 2, 1}, []byte{0, 1, 2}, []byte{1, 2, 0}, []byte{1, 0, 2}, []byte{2, 1, 0}, []byte{2, 0, 1}}
	start := Permutations{[]byte{0, 2, 1}, []byte{0, 1, 2}, []byte{1, 2, 0}, []byte{1, 0, 2}, []byte{2, 1, 0}, []byte{2, 0, 1}}
	expected := Permutations{[]byte{0, 1, 2}, []byte{0, 2, 1}, []byte{1, 0, 2}, []byte{1, 2, 0}, []byte{2, 0, 1}, []byte{2, 1, 0}}
	sort.Sort(p)
	pStr := fmt.Sprint(p)
	expectedStr := fmt.Sprint(expected)
	if pStr != expectedStr {
		t.Error("Expected: ", expectedStr, ", got: ", pStr, "starting with: ", start)
	}
}

func TestPermutationsCompare(t *testing.T) {
	p := Permutations{[]byte{0, 1, 2}, []byte{0, 2, 2}, []byte{0, 2, 2}}
	if !p.Less(0, 1) {
		t.Error(fmt.Sprint("Expected p[0] < p[1]", p))
	}
	if p.Less(1, 2) {
		t.Error(fmt.Sprint("Expected p[1] == p[2]", p))
	}
}

func BenchmarkPermute(b *testing.B) {
	n := 5
	digits := make([]byte, int(n))
	for i := 0; i < int(n); i++ {
		digits[i] = byte(i)
	}
	for i := 0; i < b.N; i++ {
		LexicalPermute(digits)
	}
}
