package main

import "testing"

func TestGrow(t *testing.T) {
	c := NewChampernowne(0)
	c.Grow()
	if len(c.limits) != 4 {
		t.Error("expected new 4-digit limit. Got: ", c)
	}
	if c.limits[len(c.limits)-1] != 190 {
		t.Error("expected new 4-digit limit to be 190. Got: ", c)
	}
	c.Grow()
	if len(c.limits) != 5 {
		t.Error("expected new 5-digit limit. Got: ", c)
	}
	if c.limits[len(c.limits)-1] != 2890 {
		t.Error("expected new 4-digit limit to be 190. Got: ", c)
	}
	c.Grow()
	c.Grow()
	c.Grow()
	c.Grow()
	c.Grow()
}

func TestMinDigits(t *testing.T) {
	n := MinDigits(3)
	if n != 100 {
		t.Error("minimum 3 digit number = 100. Got: ", n)
	}
	n = MinDigits(1)
	if n != 0 {
		t.Error("minimum 1 digit number = 0. Got: ", n)
	}
}

func TestMaxDigits(t *testing.T) {
	n := MaxDigits(3)
	if n != 999 {
		t.Error("maximum 3 digit number = 999. Got: ", n)
	}
	n = MaxDigits(1)
	if n != 9 {
		t.Error("maximum 1 digit number = 9. Got: ", n)
	}
}

func TestChampernowneDigit(t *testing.T) {
	d := 12
	expected := 1
	observed := ChampernowneDigit(d)
	if observed != expected {
		t.Error(observed, " != ", expected)
	}
	d = 1
	expected = 1
	observed = ChampernowneDigit(d)
	if observed != expected {
		t.Error(observed, " != ", expected)
	}
}

func TestChampernowneObjectDigit(t *testing.T) {
	c := NewChampernowne(6)
	d := 12
	expected := 1
	observed := c.Digit(d)
	if observed != expected {
		t.Error(observed, " != ", expected, " c: ", c)
	}
	d = 1
	expected = 1
	observed = c.Digit(d)
	if observed != expected {
		t.Error(observed, " != ", expected)
	}
}

func BenchmarkChampernowneDigit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = ChampernowneDigitsList(1000000)
	}
}

func BenchmarkChampernowneObjectDigit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		c := NewChampernowne(6)
		c.Digit(1)
		c.Digit(10)
		c.Digit(100)
		c.Digit(1000)
		c.Digit(10000)
		c.Digit(100000)
		c.Digit(1000000)
	}
}
