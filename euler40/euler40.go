package main

import (
	"flag"

	"bitbucket.org/steventhurgood/euler/intargs"

	"fmt"
)

func MinDigits(n int) int {
	if n == 1 {
		return 0
	}
	r := 1
	for n > 1 {
		r *= 10
		n--
	}
	return r
}

func MaxDigits(n int) int {
	r := 1
	for n > 0 {
		r *= 10
		n--
	}
	r--
	return r
}

type Champernowne struct {
	limits []int
}

func NewChampernowne(n int) Champernowne {
	c := Champernowne{
		limits: []int{0, 0, 10},
	}
	for ; n-2 > 0; n-- {
		c.Grow()
	}
	return c
}

func (c *Champernowne) Grow() {
	digits := len(c.limits) - 1
	offset := c.limits[digits]
	min := MinDigits(digits)
	max := MaxDigits(digits)
	newOffset := ((max - min) * digits) + offset + digits
	c.limits = append(c.limits, newOffset)
}

func (c *Champernowne) Digit(offset int) int {
	for offset > c.limits[len(c.limits)-1] {
		c.Grow()
	}
	i := len(c.limits) - 1
	for i >= 0 {
		if c.limits[i] <= offset {
			break
		}
		i--
	}
	// i contains the number of digits in this number contained at digit n
	relativeOffset := offset - c.limits[i]
	remainder := relativeOffset % i
	num := (relativeOffset / i) + MinDigits(i)
	// now we need the remainderth digit of num
	// eg, for 12345, i=5, 0 is 1, 1 is 2, 4 is 5, etc.
	// i = 2, remainder=0, shift = 1
	// i = 2, remainder=1, shift = 0
	// i = 3, remainder=0, shift = 2
	// i = 3, remainder=1, shift = 1
	// i = 3, remainder=2, shift = 0
	shift := (i - 1) - remainder
	for shift > 0 {
		num = num / 10
		shift--
	}
	digitAtOffset := num % 10
	return digitAtOffset
}

func ChampernowneDigits() chan int {
	ch := make(chan int)
	go func() {
		i := 0
		for {
			n := i
			digits := make([]int, 0, 10)
			for n > 0 {
				digit := n % 10
				digits = append(digits, digit)
				n = (n - digit) / 10
			}
			end := len(digits) - 1
			for j := 0; j < len(digits); j++ {
				ch <- digits[end-j]
			}
			i++
		}
	}()
	return ch
}

func ChampernowneDigit(n int) int {
	l := ChampernowneDigitsList(n)
	return l[n]
}

func ChampernowneDigitsList(n int) []int {
	digits := make([]int, n+1)
	i := 1
	for j := range ChampernowneDigits() {
		digits[i] = j
		i++
		if i > n {
			break
		}
	}
	return digits
}

func main() {
	flag.Parse()
	product := 1
	c := NewChampernowne(6)
	// l := ChampernowneDigitsList(1000000)
	for _, n := range intargs.IntArgs(flag.Args()) {
		digit := c.Digit(n)
		fmt.Println(n, digit)
		product *= digit
	}
	fmt.Println("product: ", product)
}
