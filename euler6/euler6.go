package main

import (
	"flag"
	"fmt"
)

func SumOfSquares(n int) int {
	sum := 0
	for i := 1; i <= n; i++ {
	  sum += (i * i)
	}
	return sum
}

func SquareOfSums(n int) int {
  sum := 0
  for i:= 1; i<= n; i++ {
  sum += i
  }
  return sum * sum
}

func main() {
	n := flag.Int("n", 10, "number up to which to find the difference between the sum of squares and square of sums")
	flag.Parse()
	sumOfSquares := SumOfSquares(*n)
	squareOfSums := SquareOfSums(*n)
	fmt.Println("n:", n)
	fmt.Println("Sum of Squares:", sumOfSquares)
	fmt.Println("Square of Sums:", squareOfSums)
	fmt.Println("Difference:", squareOfSums - sumOfSquares)
}
