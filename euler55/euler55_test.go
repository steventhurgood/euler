package main

import "testing"

func TestReverse(t *testing.T) {
	tests := [][]uint64{
		[]uint64{12345, 54321},
		[]uint64{123456, 654321},
		[]uint64{171, 171},
		[]uint64{9, 9},
		[]uint64{0, 0},
	}
	for _, test := range tests {
		got := Reverse(test[0])
		want := test[1]
		t.Logf("test: %v, got: %v, want: %v", test, got, want)
		if got != want {
			t.Errorf("Reverse(%v) => %v. Want: %v", test[0], got, want)
		}
	}
}

func TestIsPalindrome(t *testing.T) {
	palindromeTests := []uint64{
		12321,
		1,
		1223443221,
	}
	notPalindromeTests := []uint64{
		123432,
		12,
		12322,
	}
	for _, test := range palindromeTests {
		got := IsPalindrome(test)
		if !got {
			t.Errorf("IsPalindrome(%v) => false.", test)
		}
	}
	for _, test := range notPalindromeTests {
		got := IsPalindrome(test)
		if got {
			t.Errorf("IsPalindrome(%v) => true.", test)
		}
	}
}

func TestIsLychrel(t *testing.T) {
	lychrelTests := []uint64{
		196,
	}
	notLychrelTests := []uint64{
		47,
		349,
	}

	for _, test := range lychrelTests {
		got := IsLychrel(test)
		if !got {
			t.Errorf("IsLychrel(%v) => false", test)
		}
	}
	for _, test := range notLychrelTests {
		got := IsLychrel(test)
		if got {
			t.Errorf("IsLychrel(%v) => true", test)
		}
	}
}

func BenchmarkIsLychrel(b *testing.B) {
	max := uint64(b.N)
	for i := uint64(0); i < max; i++ {
		IsLychrel(i)
	}
}

func BenchmarkIsLychrel2(b *testing.B) {
	max := uint64(b.N)
	for i := uint64(0); i < max; i++ {
		IsLychrel2(i)
	}
}
