package main

import (
	"flag"
	"fmt"
)

func Reverse(n uint64) uint64 {
	var digit, r uint64
	r = 0
	for n > 0 {
		digit = n % 10
		r = r*10 + digit
		n = n / 10
	}
	return r
}

func IsPalindrome(n uint64) bool {
	return Reverse(n) == n
}

func IsLychrel(n uint64) bool {
	for i := 0; i <= 50; i++ {
		rev := Reverse(n)
		if rev == n {
			return false
		}
		n = n + rev
	}
	return true
}

func IsLychrel2(n uint64) bool {
	for i := 0; i <= 50; i++ {
		rev := Reverse(n)
		n = n + rev
		if IsPalindrome(n) {
			return false
		}
	}
	return true
}

func main() {
	upto := flag.Int("upto", 10000, "find Lychrel numbers up to")
	flag.Parse()
	count := 0
	for i := 1; i < *upto; i++ {
		if IsLychrel(uint64(i)) {
			fmt.Println(i)
			count++
		}
	}
	fmt.Printf("Total: %v\n", count)
}
