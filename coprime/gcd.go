package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

func Gcd(a, b int) int {
	if a == 1 || b == 1 {
		return 1
	}
	for {
		if a == b {
			return a
		}
		for a > b {
			a = a - b
		}
		if a == 0 {
			return b
		}
		for b > a {
			b = b - a
		}
		if b == 0 {
			return a
		}
	}
}

func Coprimes(maxA, maxB int) [][]int {
	coprimes := make([][]int, 0)
	for a := 1; a < maxA; a++ {
		for b := 1; b < maxB; b++ {
			gcd := Gcd(a, b)
			if gcd == 1 {
				coprimes = append(coprimes, []int{a, b})
			}
		}
	}
	return coprimes
}

func main() {
	uptoA := flag.Int("upto_a", -1, "count values of A up to")
	uptoB := flag.Int("upto_b", -1, "count values of B up to")
	flag.Parse()
	if *uptoA > 0 && *uptoB > 0 {
		for _, coprimes := range Coprimes(*uptoA, *uptoB) {
			fmt.Printf("%d, %d\n", coprimes[0], coprimes[1])
		}
	} else {
		if len(flag.Args()) < 2 {
			log.Fatal("usage: gcd a b")
		}
		aStr, bStr := flag.Args()[0], flag.Args()[1]
		a, err := strconv.ParseInt(aStr, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		b, err := strconv.ParseInt(bStr, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		gcd := Gcd(int(a), int(b))
		fmt.Println(int(a), int(b), gcd)
	}

}
