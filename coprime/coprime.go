package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

type Coprime [2]int

func (p Coprime) String() string {
	return fmt.Sprintf("%d, %d", p[0], p[1])
}

func ExtendCoprimes(coprimes []Coprime) []Coprime {
	newCoprimes := make([]Coprime, 0, 4*len(coprimes))
	for _, pair := range coprimes {
		m, n := pair[0], pair[1]
		newCoprimes = append(newCoprimes, pair)
		//Branch 1: (2m-n,m)
		newCoprimes = append(newCoprimes, Coprime{2*m - n, m})
		//Branch 2: (2m+n,m)
		newCoprimes = append(newCoprimes, Coprime{2*m + n, m})
		//Branch 3: (m+2n,n)
		newCoprimes = append(newCoprimes, Coprime{m + 2*n, n})
	}
	return newCoprimes
}

func Coprimes(n int) []Coprime {
	coprimes := []Coprime{Coprime{2, 1}, Coprime{3, 1}}
	for i := 0; i < n; i++ {
		coprimes = ExtendCoprimes(coprimes)
	}
	return coprimes
}

func CoprimesUpto(n int) []Coprime {
	coprimes := []Coprime{Coprime{2, 1}, Coprime{3, 1}}
	maxA, maxB := 0, 0
	for maxA < n || maxB < n {
		coprimes = ExtendCoprimes(coprimes)
		for _, pair := range coprimes {
			if pair[0] > maxA {
				maxA = pair[0]
			}
			if pair[1] > maxB {
				maxB = pair[1]
			}
		}
	}
	return coprimes
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		for _, coprime := range CoprimesUpto(int(n)) {
			fmt.Println(coprime)
		}

	}
}
