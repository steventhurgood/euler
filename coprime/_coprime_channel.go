package main

import (
	"flag"
	"fmt"
	"time"
)

type IntPair [2]int

func (pair IntPair) String() string {
	return fmt.Sprintf("%d, %d", pair[0], pair[1])
}

func expandCoprimes(out, in chan IntPair, maxm int, maxn int) {
	for {
		pair := IntPair{}
		select {
		case pair = <-in:
			{
				out <- pair
				m, n := pair[0], pair[1]
				if m < maxm && n < maxn && m > -maxm && n > -maxn {
					select {
					case in <- IntPair{2*m - n, m}:
					case in <- IntPair{2*m + n, m}:
					case in <- IntPair{m + 2*n, n}:
					default:
						{
							time.Sleep(100 * time.Millisecond)
						}
					}
				}
			}
		}
	}
}

func Coprimes(maxm, maxn int) chan IntPair {
	out := make(chan IntPair)
	in := make(chan IntPair, 200000)
	in <- IntPair{2, 1}
	in <- IntPair{3, 1}
	go expandCoprimes(out, in, maxm, maxn)
	return out
}

func main() {
	maxm, maxn := flag.Int("m", 100, "maximum value of m"), flag.Int("n", 100, "maximum value of n")
	flag.Parse()
	ch := Coprimes(*maxm, *maxn)
	for i := range ch {
		fmt.Println(i)

	}
}
