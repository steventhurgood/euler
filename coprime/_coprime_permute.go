package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"math"
	"strconv"
)

type Sieve struct {
	Divisable []bool
}

func NewSieve(size int) Sieve {
	s := Sieve{
		Divisable: make([]bool, size),
	}
	maxFactor := int(math.Floor(math.Sqrt(float64(size))))
	s.Divisable[0], s.Divisable[1] = true, true
	for i := 2; i < maxFactor; i++ {
		if !s.Divisable[i] {
			for j := i + i; j < size; j += i {
				s.Divisable[j] = true
			}
		}
	}
	return s
}

func Permutations(choices []int) [][]int {
	permutations := [][]int{[]int{}}
	for i := len(choices) - 1; i >= 0; i-- {
		// need to create new permutations based on current permutations
		// both with and without item [i]
		newPermutations := [][]int{}
		for _, p := range permutations {
			newPermutation := make([]int, 1, len(p)+1)
			newPermutation[0] = choices[i]
			for _, choice := range p {
				newPermutation = append(newPermutation, choice)
			}
			newPermutations = append(newPermutations, newPermutation)
			newPermutations = append(newPermutations, p)
		}
		permutations = newPermutations
	}
	return permutations
}

func (s *Sieve) Primes() []int {
	primes := make([]int, 0)
	for i, divisable := range s.Divisable {
		if !divisable {
			primes = append(primes, i)
		}
	}
	return primes
}

type IntPartition [][]int

// create pre-generated permutations of lists of length 1:n
func GenPermutations(n int) map[int][][]int {
	permutationMap := make(map[int][][]int)
	choices := []int{}
	for i := 0; i < n; i++ {
		permutations := Permutations(choices)
		permutationMap[i] = permutations
		choices = append(choices, i)
	}
	return permutationMap
}

func Coprimes(primes []int) [][]int {
	partitions := Partition(primes)
	coprimes := [][]int{}
	permutations := GenPermutations(len(primes))
	for _, partition := range partitions {
		left, right := partition[0], partition[1]
		for _, leftPermutation := range permutations[len(left)] {
			leftNum := 1
			for _, i := range leftPermutation {
				leftNum *= left[i]
			}
			for _, rightPermutation := range permutations[len(right)] {
				rightNum := 1
				for _, i := range rightPermutation {
					rightNum *= right[i]
				}
				coprimes = append(coprimes, []int{leftNum, rightNum})
			}
		}
	}
	return coprimes
}

func (p IntPartition) String() string {
	b := bytes.NewBuffer([]byte{})
	left := p[0]
	right := p[1]
	b.WriteString(fmt.Sprint(left))
	b.WriteString(", ")
	b.WriteString(fmt.Sprint(right))
	b.WriteString("\n")
	return b.String()
}

func Partition(choices []int) []IntPartition {
	setPairs := []IntPartition{}
	if len(choices) < 2 {
		return setPairs
	}
	setPairs = []IntPartition{
		IntPartition{[]int{choices[0]}, []int{choices[1]}},
	}
	for i := 2; i < len(choices); i++ {
		newPairs := []IntPartition{}

		if len(setPairs) > 0 {
			pair := setPairs[0]
			left := pair[0]
			right := pair[1]
			newMerged := make([]int, len(left)+len(right))
			copy(newMerged, left)
			copy(newMerged[len(left):], right)
			newUnique := []int{choices[i]}
			newPairs = append(newPairs, IntPartition{newMerged, newUnique})

		}
		for _, pair := range setPairs {
			left := pair[0] // type: []int
			right := pair[1]

			newLeft := make([]int, len(left)+1)
			newLeft[0] = choices[i]
			copy(newLeft[1:], left)

			newRight := make([]int, len(right)+1)
			newRight[0] = choices[i]
			copy(newRight[1:], right)
			newPairs = append(newPairs, IntPartition{newLeft, right})
			newPairs = append(newPairs, IntPartition{left, newRight})
			// leftPair := [][]int{}
			// rightPair := [][]int{}
		}
		setPairs = newPairs
	}
	return setPairs
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		s := NewSieve(int(n))
		p := s.Primes()
		fmt.Println(p)
		cp := Coprimes(p)
		for _, pair := range cp {
			fmt.Printf("%d, %d\n", pair[0], pair[1])
		}
	}
}
