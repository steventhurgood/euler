package main

import (
	"testing"
)

func TestPermutations(t *testing.T) {
	choices := []int{0, 1, 2, 3}
	permutations := Permutations(choices)
	if len(permutations) != 16 {
		t.Error("permutations of [0, 1,2,3] should be 16 long. Got: ", permutations)
	}
}

func TestPartition(t *testing.T) {
	p := []int{2}
	expected := []IntPartition{}
	actual := Partition(p)
	if len(actual) != len(expected) {
		t.Error("expected:\n", expected, "\ngot:\n", actual)
	}

	p = []int{2, 3}
	expected = []IntPartition{
		[][]int{[]int{2}, []int{3}},
	}
	actual = Partition(p)
	if len(actual) != len(expected) {
		t.Error("expected:\n", expected, "\ngot:\n", actual)
	}

	p = []int{2, 3, 5}
	expected = []IntPartition{
		[][]int{[]int{2}, []int{3, 5}},
		[][]int{[]int{3}, []int{2, 5}},
		[][]int{[]int{5}, []int{3, 7}},
	}
	actual = Partition(p)
	if len(actual) != len(expected) {
		t.Error("expected:\n", expected, "\ngot:\n", actual)
	}
	p = []int{2, 3, 5, 7}
	expected = []IntPartition{
		[][]int{[]int{2, 3, 5}, []int{7}},
		[][]int{[]int{2, 3, 7}, []int{5}},
		[][]int{[]int{2, 5, 7}, []int{3}},
		[][]int{[]int{2, 5, 7}, []int{2}},
		// picking 2 things out of 4, there are duplicate pairs:
		// 1,2 ; 1,3 ; 1,4 ; 2,3 [=1, 4] ; 3,4 [=1,2],
		// 12, 13, 14, X21, x23, x24, X31, x32, x34, X41, x42, x43
		[][]int{[]int{2, 3}, []int{5, 7}},
		[][]int{[]int{2, 5}, []int{3, 7}},
		[][]int{[]int{2, 7}, []int{3, 5}},
	}
	actual = Partition(p)
	if len(actual) != len(expected) {
		t.Error("expected:\n", expected, "\ngot:\n", actual)
	}
}
