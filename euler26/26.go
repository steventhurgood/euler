package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"
)

type Rational struct {
	numerator   int
	denominator int
}

func (r Rational) Divide() (int, int, error) {
	data, rem, err := r.DivideBytes()
	if err != nil {
		return 0, 0, err
	} else {
		return BytesToInt(data), rem, nil
	}
}

func (r Rational) DivDecimal() (string, error) {
	integerPart, rem, err := r.DivideBytes()
	if err != nil {
		return "", err
	}
	fractionPart, recurringPart := r.DivRemainderRecurring(rem)
	out := make([]byte, 0, len(integerPart)+len(fractionPart)+len(recurringPart)+3)
	leading := true
	for _, c := range integerPart {
		if leading && c == 0 {
			continue
		}
		leading = false
		out = append(out, c+'0')
	}
	if leading {
		out = append(out, '0')
	}
	if len(fractionPart) > 0 || len(recurringPart) > 0 {
		out = append(out, '.')
	}
	for _, c := range fractionPart {
		out = append(out, c+'0')
	}
	if len(recurringPart) > 0 {
		out = append(out, '(')
		for _, c := range recurringPart {
			out = append(out, c+'0')
		}
		out = append(out, ')')
		out = append(out, '.')
	}
	buf := bytes.NewBuffer(out)
	return buf.String(), nil
}

func (r Rational) DivRemainderRecurring(rem int) ([]byte, []byte) {
	i := 0
	den := r.denominator
	out := []byte{}
	num := rem * 10
	div := 0
	lastSeenRemainder := make(map[int]int)

	for rem != 0 {
		pos, seen := lastSeenRemainder[rem]
		if seen {
			// pos was where this was last seen, so we're going to recur up to that point
			// return out[:pos], out[pos:]
			return out[:pos-1], out[pos-1:]
		}
		i++
		if den <= num {
			lastSeenRemainder[rem] = i
			rem = num % den
			div = (num - rem) / den
			out = append(out, byte(div))
			num = rem * 10
		} else {
			num = num * 10
			out = append(out, 0)
		}
	}
	return out, []byte{}
}

func (r Rational) DivideBytes() ([]byte, int, error) {
	// work out num/den
	numBytes := IntToBytes(r.numerator)
	den := r.denominator
	if den == 0 {
		return nil, 0, errors.New("Attempt to divide by zero. Oh shi-")
	}
	out := []byte{}
	rem := 0

	numStart, numEnd := 0, 1
	for {
		if numStart == len(numBytes) {
			break
		}
		num := (rem * 10) + BytesToInt(numBytes[numStart:numEnd])
		if den <= num {
			rem = num % den
			div := (num - rem) / den
			out = append(out, byte(div))
			numStart, numEnd = numEnd, numEnd+1
		} else {
			out = append(out, 0)
			if numEnd == len(numBytes) {
				rem = BytesToInt(numBytes[numStart:numEnd])
				return out, rem, nil
			}
			numEnd++
		}
	}
	return out, rem, nil
}

func IntToBytes(n int) []byte {
	data := make([]byte, 0)
	for {
		digit := n % 10
		n = (n - digit) / 10
		data = append(data, byte(digit))
		if n == 0 {
			break
		}
	}
	reverse := make([]byte, len(data))
	for i, _ := range data {
		reverse[i] = data[len(data)-1-i]
	}
	return reverse
}

func BytesToInt(data []byte) int {
	n := 0
	for _, value := range data {
		n = (n * 10) + int(value)
	}
	return n
}
func main() {
	showall := flag.Bool("showall", false, "show all results")
	upto := flag.Int("upto", -1, "iterate through numbers up to 1/n to find the longest recurring part")
	flag.Parse()
	if *upto > 0 {
		maxRecursion := -1
		maxRecurringN := -1
		for i := 1; i < *upto; i++ {
			r := Rational{1, i}
			_, recurringPart := r.DivRemainderRecurring(1)
			if *showall {
				n, err := r.DivDecimal()
				if err != nil {
					log.Fatal(err)
				}
				fmt.Println("1 / ", i, " = ", n)
			}
			if len(recurringPart) > maxRecursion {
				maxRecursion = len(recurringPart)
				maxRecurringN = i
			}

		}
		r := Rational{1, maxRecurringN}
		n, _ := r.DivDecimal()
		fmt.Println("1 /", maxRecurringN, "(", maxRecursion, ") =", n)

	}
	for _, arg := range flag.Args() {
		argNums := strings.SplitN(arg, "/", 2)
		if len(argNums) != 2 {
			log.Fatal("Error parsing fraction: ", arg)
		}
		num, err := strconv.ParseInt(argNums[0], 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		den, err := strconv.ParseInt(argNums[1], 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		r := Rational{int(num), int(den)}
		s, err := r.DivDecimal()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(arg, "=", s)
	}
}
