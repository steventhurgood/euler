package main

import (
	"fmt"
	"testing"
)

func TestDivideZero(t *testing.T) {
	r := Rational{1, 0}
	div, rem, err := r.Divide()
	if err == nil {
		t.Error("Expected error for 1/0. Got div: ", div, ", rem: ", rem, ", err:", err)
	}
}

func TestDivideNoop(t *testing.T) {
	r := Rational{4, 1}
	div, rem, err := r.Divide()
	if err != nil {
		t.Error(err)
	}
	if div != 4 || rem != 0 {
		t.Error("4/1 expected 4, remainder 0. Got: ", div, rem)
	}
}

func TestDivideSmall(t *testing.T) {
	r := Rational{1, 4}
	div, rem, err := r.Divide()
	if err != nil {
		t.Error(err)
	}
	if div != 0 || rem != 1 {
		t.Error("1/4 expected 0, remainder 1. Got: ", div, rem)
	}
}

func TestDivideBasic(t *testing.T) {
	r := Rational{4, 2}
	div, rem, err := r.Divide()
	if err != nil {
		t.Error(err)
	}
	if div != 2 || rem != 0 {
		t.Error("4/2 expected 2, remainder 0. Got: ", div, rem)
	}
}

func TestDivDecimal(t *testing.T) {
	r := Rational{100, 32}
	expected := "3.125"
	actual, err := r.DivDecimal()
	if err != nil {
		t.Error(err)
	}
	if actual != expected {
		t.Error("1/2 expected: ", expected, " actual: ", actual)
	}
}

func TestDivRemainderRecurringRecursion(t *testing.T) {
	r := Rational{13, 600}
	_, rem, _ := r.DivideBytes()
	expectedFraction := []byte{0, 2, 1}
	expectedFractionStr := fmt.Sprint(expectedFraction)
	expectedRecurring := []byte{6}
	expectedRecurringStr := fmt.Sprint(expectedRecurring)

	fraction, recurring := r.DivRemainderRecurring(rem)
	fractionStr := fmt.Sprint(fraction)
	recurringStr := fmt.Sprint(recurring)
	t.Log(fraction, recurring)
	if fractionStr != expectedFractionStr {
		t.Error("Expected: ", expectedFractionStr, ", got: ", fractionStr)
	}
	if recurringStr != expectedRecurringStr {
		t.Error("Expected: ", expectedRecurringStr, ", got: ", recurringStr)
	}
}

func TestDivRemainderRecurring(t *testing.T) {
	r := Rational{1, 2} // 1/2 == 0, remainder 1
	expected := []byte{5}
	expectedStr := fmt.Sprint(expected)
	actual, recurring := r.DivRemainderRecurring(1)
	actualStr := fmt.Sprint(actual)
	if expectedStr != actualStr {
		t.Error("expected: ", expectedStr, ", got: ", actualStr)
	}
	if len(recurring) != 0 {
		t.Error("expected no recurring part; got: ", recurring)
	}
}

func TestDivideBorrow(t *testing.T) {
	r := Rational{10, 3}
	div, rem, err := r.Divide()
	if err != nil {
		t.Error(err)
	}
	if div != 3 || rem != 1 {
		t.Error("10/3 expected 3, remainder 1. Got: ", div, rem)
	}
}

func TestDivide(t *testing.T) {
	r := Rational{12345, 17}
	div, rem, err := r.Divide()
	if err != nil {
		t.Error(err)
	}
	if div != 726 || rem != 3 {
		t.Error("12345/17 expected 726, remainder 3. Got: ", div, rem)
	}
}

func TestIntToBytes(t *testing.T) {
	b := IntToBytes(22)
	expected := []byte{2, 2}
	expectedStr := fmt.Sprint(expected)
	actualStr := fmt.Sprint(b)
	if expectedStr != actualStr {
		t.Error("Expected: ", expectedStr, "; actual: ", actualStr)
	}
}

func TestBytesToInt(t *testing.T) {
	i := BytesToInt([]byte{1, 2, 3})
	expected := 123
	if i != expected {
		t.Error("Expected []byte{1, 2, 3} == 123")
	}
	i = BytesToInt([]byte{0})
	expected = 0
	if i != expected {
		t.Error("Expected []byte{1, 2, 3} == 123")
	}
}
