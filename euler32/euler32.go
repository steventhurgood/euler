package main

import (
	"fmt"
	"math"
)

// return the smallest number with n digits where no digit occurs twice
func MinDigits(n int) int {
	r := math.Pow(10.0, float64(n-1))
	// for i := 1; i <= n; i++ {
	// 	r = (r * 10) + i
	// }
	return int(r)
}

func MaxDigits(n int) int {
	// r := 0
	r := math.Pow(10.0, float64(n))
	// for i := 9; i > 9-n; i-- {
	// 		r = (r * 10) + i
	//	}
	return int(r)
}

func NumDigits(a int) int {
	return int(math.Ceil(math.Log10(float64(a))))
}

func IsPanDigital(a, b, c int) bool {
	present := [10]bool{}
	seen := 0
	nums := [3]int{a, b, c}
	for _, num := range nums {
		for num > 0 {
			digit := num % 10
			if digit == 0 {
				return false
			}
			if present[digit] {
				return false
			}
			present[digit] = true
			seen++
			num = (num - digit) / 10
		}
	}
	return seen == 9
}

func MinMax(a int) (int, int, int, int) {
	aDigits := NumDigits(a)
	bDigits := (9 - aDigits) / 2
	cDigits := 9 - (aDigits + bDigits)
	// log.Print("a digits: ", aDigits)
	// log.Print("b digits: ", bDigits)
	// log.Print("c digits: ", cDigits)

	cMin, cMax := MinDigits(cDigits), MaxDigits(cDigits)
	// log.Print("c min: ", cMin, " c max: ", cMax)
	bMin, bMax := cMin/a, cMax/a
	if NumDigits(bMin) < bDigits {
		bMin = MinDigits(bDigits)
	}
	if NumDigits(bMax) < bDigits {
		bMax = MaxDigits(bDigits)
	}
	// log.Print("b min: ", bMin, " b max: ", bMax)

	return bMin, bMax, bDigits, cDigits
}

func main() {
	pandigitals := [][3]int{}

	for a := 2; ; a++ {
		bMin, bMax, _, cDigits := MinMax(a)
		// log.Print("a = ", a, " b=[", bMin, ", ", bMax, "] b digits = ", bDigits, " c digits = ", cDigits)
		if a > bMax {
			break
		}
		if NumDigits(a*bMin)+cDigits > 9 {
			continue
		}
		for b := bMin; b <= bMax; b++ {
			c := a * b
			if IsPanDigital(a, b, c) {
				pandigitals = append(pandigitals, [3]int{a, b, c})
			}

		}
	}
	sum := 0
	products := make(map[int]string)
	for _, p := range pandigitals {
		fmt.Println(p)
		products[p[2]] = fmt.Sprintf("%d x %d", p[0], p[1])
	}
	for product, formula := range products {
		fmt.Println(product, formula)
		sum += product
	}
	fmt.Println("sum: ", sum)
}
