package main

import (
	"testing"
)

func TestMaxDigits(t *testing.T) {
	d := MaxDigits(3)
	if d != 987 {
		t.Error("expected 987. Got: ", d)
	}
	d = MaxDigits(1)
	if d != 9 {
		t.Error("expected 1. Got: ", d)
	}
}

func TestMinDigits(t *testing.T) {
	d := MinDigits(5)
	if d != 12345 {
		t.Error("expected 12345. Got: ", d)
	}
}
