package main

import (
	"flag"
	"fmt"
	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

func main() {
	flag.Parse()
	args := intargs.IntArgs(flag.Args())
	gcd := prime.GcdList(args)
	fmt.Println(args, gcd)
}
