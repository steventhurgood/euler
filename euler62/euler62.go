package main

import (
	"bytes"
	"flag"
	"fmt"
	"sort"
)

var (
	permutations = flag.Int("p", 3, "Number of permutations")
)

type TrieNode struct {
	values []int
	edges  []*TrieNode
}

func (t *TrieNode) String() string {
	var b bytes.Buffer
	t.StringRec("", &b)
	return b.String()

}

func (t *TrieNode) StringRec(prefix string, b *bytes.Buffer) {
	fmt.Fprintf(b, "%v values: %v\n", prefix, t.values)
	for digit, edge := range t.edges {
		if edge == nil {
			fmt.Fprintf(b, "%v %v: nil\n", prefix, digit)
		} else {
			newPrefix := fmt.Sprintf("%v %v  ", prefix, digit)
			edge.StringRec(newPrefix, b)
		}
	}
}

func Digits(n int) []int {
	digits := make([]int, 0)
	for n > 0 {
		digit := n % 10
		digits = append(digits, digit)
		n /= 10
	}
	return digits
}

func (t *TrieNode) Insert(n int) []int {
	digits := Digits(n)
	sort.Ints(digits)
	return t.InsertRec(digits, n)
}

func (t *TrieNode) InsertRec(n []int, value int) []int {
	if len(n) == 0 {
		t.values = append(t.values, value)
		return t.values
	}
	digit := n[0]
	if t.edges[digit] == nil {
		t.edges[digit] = NewTrie()
	}
	return t.edges[digit].InsertRec(n[1:len(n)], value)
}

func NewTrie() *TrieNode {
	return &TrieNode{
		values: make([]int, 0),
		edges:  make([]*TrieNode, 10),
	}
}

func main() {
	flag.Parse()
	t := NewTrie()
	n := 1
	for {
		c := n * n * n
		v := t.Insert(c)
		if len(v) >= *permutations {
			fmt.Println(v)
			break
		}
		n++
	}
}
