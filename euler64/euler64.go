package main

import (
	"flag"
	"fmt"
	"math"

	"bitbucket.org/steventhurgood/euler/intargs"
)

var (
	IsSquare = make(map[int]bool)
)

func Sqrt(n int) []int {
	r := make([]int, 0)
	m := 0
	d := 1
	a := int(math.Floor(math.Sqrt(float64(n))))
	a0 := a

	r = append(r, a)
	done := false
	seen := make(map[[3]int]bool)

	for !done {
		// fmt.Printf("m: %v, d: %v, a: %v\n", m, d, a)
		mnext := d*a - m
		dnext := (n - mnext*mnext) / d
		anext := int(math.Floor((float64(a0) + float64(mnext)) / float64(dnext)))

		key := [3]int{mnext, dnext, anext}
		if seen[key] {
			return r
		}
		seen[key] = true
		r = append(r, anext)
		m = mnext
		d = dnext
		a = anext
	}
	return r
}

func main() {
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		for i := 0; i < arg; i++ {
			IsSquare[i*i] = true
		}
		IsSquare[2] = false
		// fmt.Println(IsSquare)
		sum := 0
		for i := 1; i <= arg; i++ {
			// fmt.Println(i)
			if IsSquare[i] {
				// fmt.Println("skipping")
				continue
			}
			r := Sqrt(i)
			fmt.Printf("Sqrt(%v) -> %v; [%v]\n", i, r[0], r[1:len(r)])
			if len(r)%2 == 0 {
				sum++ // r is even, so r[1:] is odd.
			}
		}
		fmt.Printf("Number of odd cycles: %v\n", sum)
	}
}
