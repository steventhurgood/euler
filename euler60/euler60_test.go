package main

import (
	"testing"

	"bitbucket.org/steventhurgood/euler/prime"
)

func BenchmarkMassivePrimeSieve(b *testing.B) {
	for i := 0; i < b.N; i++ {
		prime.NewSieve(100000000)
	}
}
