package main

import (
	"flag"
	"fmt"

	"bytes"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

var (
	set_size   = flag.Int("set_size", 5, "size of set to find")
	sieve_size = flag.Int("sieve_size", 100000000, "sieze of prime sieve")
)

// a set of prime numbers
type PrimeSet struct {
	size   int   // the number of primes
	primes []int // in order
}

type PrimeSets struct {
	max    int          // the size of the largest set in sets
	n      int          // the rank of the maximum prime in any set
	sets   [][]PrimeSet // the sets of primes, the index of which is the size
	sieve  *prime.Sieve
	primes []int
	digits []int
}

func (p *PrimeSets) PrintSet(set PrimeSet) string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "[")
	for _, prime := range set.primes {
		fmt.Fprintf(&b, "%v ", p.primes[prime])
	}
	fmt.Fprintf(&b, "]")
	return b.String()
}

func (p *PrimeSets) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "Prime sets up to %v-set\n", p.max)
	for i := 2; i < len(p.sets); i++ {
		fmt.Fprintf(&b, "%v-sets: ", i)
		for j := 0; j < len(p.sets[i]); j++ {
			fmt.Fprintf(&b, "[")
			for _, prime := range p.sets[i][j].primes {
				// fmt.Fprintf(&b, "%v(%v) ", p.primes[prime], prime)
				fmt.Fprintf(&b, "%v ", p.primes[prime])
			}
			fmt.Fprintf(&b, "]")
			// fmt.Fprintf(&b, "%v", p.sets[i][j].primes)
			if j < len(p.sets[i])-1 {
				fmt.Fprintf(&b, ", ")
			}
		}
		fmt.Fprintf(&b, "\n")
	}
	return b.String()
}

// Create a new set of primesets up to max-set (eg, 5-set is sets of 5 primes)
func NewPrimeSets(max int, nPrimes int) *PrimeSets {
	sieve := prime.NewSieve(nPrimes)
	allPrimes := sieve.Primes()
	primes := make([]int, len(allPrimes)-2) // exclude 2 and 5
	primes[0] = allPrimes[1]
	copy(primes[1:len(primes)], allPrimes[3:len(allPrimes)]) // exclude indexes 0 and 2
	digits := make([]int, len(primes))
	for i, n := range primes {
		digits[i] = Multiplier(n)
	}
	sets := [][]PrimeSet{
		[]PrimeSet{},                         // 0-sets does not make sense, but keeps indexes consistent
		[]PrimeSet{},                         // the 1-sets are effectively all primes, so ignore
		[]PrimeSet{PrimeSet{2, []int{0, 1}}}, // 2-set contains 0,1 = 37 and 73
	}
	for i := 0; i < max-2; i++ {
		sets = append(sets, []PrimeSet{}) // no valid n-sets
	}

	p := &PrimeSets{
		max:    max,
		n:      1, // initialise with primes 0 and 1
		sieve:  sieve,
		primes: primes,
		digits: digits,
		sets:   sets,
	}
	return p
}

// StillValid - is the set still valid concatenated-primewise if we add the ith prime to it
func (p *PrimeSets) StillValid(set PrimeSet, i int) bool {
	for _, prime := range set.primes {
		if !p.IsValidPair(prime, i) {
			// fmt.Printf("Invalid pair: %v(%v), %v(%v)\n", prime, p.primes[prime], i, p.primes[i])
			return false
		}
	}
	return true
}

func (p *PrimeSets) PrimeSetSum(set PrimeSet) int {
	sum := 0
	for _, prime := range set.primes {
		sum += p.primes[prime]
	}
	return sum
}

// Is the pair i, j concatenated-primewise valid
func (p *PrimeSets) IsValidPair(i, j int) bool {
	na := p.Concat(i, j)
	nb := p.Concat(j, i)
	// fmt.Printf("testing valid pair: %v, %v = %v\n", i, j, n)
	// fmt.Println("testing prime: ", na, i, j)
	isa, _ := p.sieve.IsPrime(na)
	// fmt.Println("testing prime: ", nb, j, i)
	isb, _ := p.sieve.IsPrime(nb)
	return isa && isb

}

func (p *PrimeSets) Concat(i, j int) int {
	a, b := p.primes[i], p.primes[j]
	n := a*p.digits[j] + b
	return n
}

// Grow - increase the primesets to the n+1th prime.
func (p *PrimeSets) Grow() bool {
	nextPrime := p.n + 1
	for setSize := p.max - 1; setSize > 1; setSize-- {
		nextSet := setSize + 1
		for _, set := range p.sets[setSize] {
			// eg., go through all 4-sets looking for a valid primeset.
			if p.StillValid(set, nextPrime) {
				newPrimes := make([]int, nextSet)
				copy(newPrimes, set.primes)
				newPrimes[setSize] = nextPrime
				newSet := PrimeSet{
					size:   nextSet,
					primes: newPrimes,
				}
				p.sets[nextSet] = append(p.sets[nextSet], newSet)
			}
		}
	}
	// generate all 2-sets primes
	for i := 0; i < nextPrime; i++ {
		if p.IsValidPair(i, nextPrime) {
			newSet := PrimeSet{
				size:   2,
				primes: []int{i, nextPrime},
			}
			p.sets[2] = append(p.sets[2], newSet)
		}
	}
	p.n = nextPrime
	// fmt.Printf("p.max: %v, len(p.sets[p.max]) = %v\n", p.max, len(p.sets[p.max]))
	return len(p.sets[p.max]) > 0
}

// Multiplier - digit multiplier for concatenating a number with n on the left hand side
func Multiplier(n int) int {
	switch {
	case n < 10:
		{
			return 10
		}
	case n < 100:
		{
			return 100
		}
	case n < 1000:
		{
			return 1000
		}
	case n < 10000:
		{
			return 10000
		}
	default:
		{
			r := 1
			for n > 0 {
				r *= 10
				n /= 10
			}
			return r
		}
	}
}

func main() {
	flag.Parse()
	p := NewPrimeSets(*set_size, *sieve_size)
	fmt.Println(p)
	if len(flag.Args()) == 0 {
		found := false
		// i := 0
		for !found {
			found = p.Grow()
			/*if i%100 == 0 {
				for _, set := range p.sets[4] {
					fmt.Println(p.PrintSet(set))
				}
			}
			i++*/
		}
		fmt.Println(p)
		fmt.Println("Sum: ", p.PrimeSetSum(p.sets[p.max][0]))

	}
	for _, arg := range intargs.IntArgs(flag.Args()) {
		for i := 0; i < arg; i++ {
			p.Grow()
			fmt.Println(p)
		}
	}
}
