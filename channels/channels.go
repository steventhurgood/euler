package main

import (
	"fmt"
	"math/rand"
	"time"
)

type StopCounter struct {
	ch      chan int
	stop    chan bool
	stopped bool
}

func Counter(start, offset, end int) StopCounter {
	ch := make(chan int)
	stop := make(chan bool)
	go func() {
		for i := start; i < end; i += offset {
			ch <- i
			time.Sleep(time.Duration((rand.Int63() % 1000)) * time.Millisecond)
		}
		stop <- true
		close(ch)
		close(stop)
	}()
	return StopCounter{ch, stop, false}
}

func DeMux(c []StopCounter) chan int {
	ch := make(chan int)
	go func() {
		allStopped := false
		for !allStopped {
			allStopped = true
			for i := range c {
				if !c[i].stopped {
					allStopped = false
					select {
					case _ = <-c[i].stop:
						{
							c[i].stopped = true
						}
					case num := <-c[i].ch:
						{
							ch <- num
						}
					default:
						{
						}
					}
				}
			}
		}
		close(ch)
	}()
	return ch
}

func main() {
	c := []StopCounter{
		Counter(1, 1, 100),
		Counter(20, 10, 2000),
		Counter(300, 100, 4000),
	}
	m := DeMux(c)
	for i := range m {
		fmt.Println(i)
	}
}
