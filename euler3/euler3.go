package main

import (
	"flag"
	"log"
	"fmt"
	"math"
)

// return n^k
func Power(n, k int) int {
	product := 1
	for i := 0; i < k; i++ {
		product *= n
	}
	return product
}

func PrimesUpTo(n int) []int {
	sieve := make([]bool, n)
	primes := make([]int, 0, 1024)
	sieve[0] = true
	sieve[1] = true

	for i := 2; i < n; i++ {
		p := 2
		for {
			product := i * p
			if product >= n {
				break
			}
			sieve[product] = true
			p++
		}
	}

	for i, v := range sieve {
		if !v {
			primes = append(primes, i)
		}
	}
	return primes
}

func PrimeFactors(n int) []int {
	max := int(math.Ceil(math.Sqrt(float64(n))))
	primes := PrimesUpTo(max+1)
	log.Print(primes)
	primeFactors := make([]int, 0, 1024)
	for i := len(primes)-1; i >= 0; i-- {
		prime := primes[i]
		// log.Print("prime: ", prime, " n:", n, " i:", i)
		if n%prime == 0 {
			primeFactors = append(primeFactors, prime)
		}
	}
	if len(primeFactors) == 0 {
	  return []int{n}
	}

	return primeFactors
}

func main() {
	n := flag.Int("n", 13195, "the number whose largest prime factor to find")
	flag.Parse()
	for _, i := range PrimeFactors(*n) {
		fmt.Println(i)
	}
}
