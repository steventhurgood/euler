package intargs

import (
	"log"
	"strconv"
)

func IntArgs(args []string) []int {
	intargs := []int{}
	for _, arg := range args {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		intargs = append(intargs, int(n))
	}
	return intargs
}
