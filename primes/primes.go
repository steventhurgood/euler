package main

import (
	"flag"
	"fmt"
	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

func main() {
	flag.Parse()
	for _, n := range intargs.IntArgs(flag.Args()) {
		s := prime.NewSieve(n)
		for _, p := range s.Primes() {
			fmt.Println(p)
		}
	}
}
