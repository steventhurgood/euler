package main

import (
	"fmt"
	"sort"
)

type Product struct {
	Product int
	X, Y int
}
func (p Product) String() string {
	return fmt.Sprintf("%d (%d x %d)", p.Product, p.X, p.Y)
}

type ByProduct []Product

func (p ByProduct) Len() int { return len(p) }
func (p ByProduct) Swap(i, j int) {p[i], p[j] = p[j], p[i]}
func (p ByProduct) Less(i, j int) bool {return p[i].Product >p[j].Product}

func IsPalindrome(s string) bool {
	for i := 0; i < len(s)/2; i++ {
		if s[i] != s[len(s)-1-i] {
			return false
		}
	}
	return true
}

func IsPalindromeInt(n int) bool {
	s := fmt.Sprintf("%d", n)
	return IsPalindrome(s)
}

func PalindromesUpTo(n int) []Product {
	p := make([]Product, 0, 1024)
	for i := 1; i <= n; i++ {
		for j := 1; j <= n; j++ {
			product := i * j
			if IsPalindromeInt(product) {
			p = append(p, Product{Product: product, X: i, Y: j})
			}
		}
	}
	return p
}

func main() {
	p := PalindromesUpTo(999)
	sort.Sort(ByProduct(p))
	fmt.Println(p[0])
}
