package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
)

func SplitNames(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// cases:
	// Start: fast forward to next ", then fast forward to the quote after that. Return the bit inbetween
	firstCommaIndex := bytes.IndexByte(data, '"')
	if firstCommaIndex < 0 {
		if atEOF {
			return 0, nil, errors.New(fmt.Sprint("Expected to find \" in input: ", data))
		}
		return 0, nil, nil
	}
	lastCommaIndex := bytes.IndexByte(data[firstCommaIndex+1:], '"')
	if lastCommaIndex < 0 {
		if atEOF {
			lastCommaIndex = len(data) - 1
		}
		return 0, nil, nil
	}
	name := data[firstCommaIndex+1 : lastCommaIndex+firstCommaIndex+1]
	return lastCommaIndex + firstCommaIndex + 2, bytes.ToLower(name), nil

}

func ReadNames(r io.Reader) []string {
	scanner := bufio.NewScanner(r)
	names := make([]string, 0)
	scanner.Split(SplitNames)
	for scanner.Scan() {
		name := scanner.Text()
		names = append(names, name)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	sort.Strings(names)
	return names
}

func main() {
	url := flag.String("url", "", "url from which to fetch names")
	flag.Parse()
	var input io.Reader
	if *url != "" {
		resp, err := http.Get(*url)
		if err != nil {
			log.Fatal(err)
		}
		input = resp.Body
	} else {
		input = os.Stdin
	}
	names := ReadNames(input)
	sum := 0
	for pos, name := range names {
		charSum := 0
		for _, char := range name {
			charVal := char + 1 - rune('a')
			charSum += int(charVal)
		}
		score := (pos + 1) * charSum
		sum += score
		fmt.Println(pos+1, name, score, charSum, sum)
	}
	fmt.Println(names)
	fmt.Println("score:", sum)
}
