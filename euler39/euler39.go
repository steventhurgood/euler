package main

import (
	"flag"
	"fmt"
)

func MakeSquares(max int) []int {
	s := make([]int, max+1)
	for i := 1; i < max+1; i++ {
		s[i] = i * i
	}
	return s
}

func main() {
	maxP := flag.Int("maxP", 100, "maximum permiter")
	flag.Parse()
	squares := MakeSquares(*maxP)

	triangles := [][]int{}
	pCount := make([]int, *maxP+1)
	for p := 0; p <= *maxP; p++ {
		for c := 1; c < p/2; c++ {
			for a := 1; a < c; a++ {
				b := (p - c) - a
				if squares[c] == squares[a]+squares[b] {
					triangles = append(triangles, []int{p, a, b, c})
					pCount[p]++
				}
			}
		}
	}
	for _, t := range triangles {
		fmt.Println(t)
	}
	max := 0
	bestP := 0
	for i, count := range pCount {
		if count > max {
			max = count
			bestP = i
		}
	}
	fmt.Println("best permiter =", bestP, " with", max, "possible triangles")
}
