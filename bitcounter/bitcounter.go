package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/intargs"
)

var (
	n      = flag.Int("n", 0, "print bits up to n")
	filter = flag.Int("filter", 0, "only show strings with this many bits")
)

func Bits(n uint32) int {
	count := 0
	for n > 0 {
		if n&1 == 1 {
			count++
		}
		n >>= 1
	}
	return count
}

func BitString(n uint32) string {
	s := make([]byte, 32)
	for i := 0; i < 32; i++ {
		if n&1 == 1 {
			s[i] = '1'
		} else {
			s[i] = '0'
		}
		n >>= 1
	}
	return string(s)
}

func Choose5(n int) [][]int {
	choices := make([][]int, 0)
	for _, choice := range Choose4(n - 1) {
		newChoice := make([]int, 5)
		copy(newChoice, choice)
		newChoice[4] = n
		choices = append(choices, newChoice)
	}
	return choices
}

// Choose4 - return all of the ways of choosing 4 items from n
func Choose4(n int) [][]int {
	choices := make([][]int, 0)
	for a := 0; a < (n - 3); a++ {
		for b := a + 1; b < (n - 2); b++ {
			for c := b + 1; c < (n - 1); c++ {
				for d := c + 1; d < n; d++ {
					choices = append(choices, []int{a, b, c, d})
				}
			}
		}

	}
	return choices
}

func main() {
	flag.Parse()
	if *n > 0 {
		for i := 0; i < *n; i++ {
			b := Bits(uint32(i))
			if *filter > 0 {
				if b == *filter {
					fmt.Printf("%v(%v): %v\n", i, b, BitString(uint32(i)))
				}
			} else {
				fmt.Printf("%v(%v): %v\n", i, b, BitString(uint32(i)))
			}
		}
	}
	for _, arg := range intargs.IntArgs(flag.Args()) {
		choices := Choose5(arg)
		for _, choice := range choices {
			fmt.Println(choice)
		}
	}
}
