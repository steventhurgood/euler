package main

import (
	"flag"
	"fmt"
	"math/big"

	"bitbucket.org/steventhurgood/euler/intargs"
)

func PowerSeries(n int) chan *big.Int {
	ch := make(chan *big.Int)
	go func() {
		defer close(ch)
		for i := 1; i <= n; i++ {
			r := big.NewInt(0)
			r.Exp(big.NewInt(int64(i)), big.NewInt(int64(i)), nil)
			ch <- r
		}
	}()
	return ch
}

func PowerSeriesSum(n int) *big.Int {
	sum := big.NewInt(0)
	for n := range PowerSeries(n) {
		sum.Add(sum, n)
	}
	return sum
}

func main() {
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		r := PowerSeriesSum(arg)
		fmt.Printf("PowerSeriesSum(%v): %v\n", arg, r)
	}
}
