package main

import (
	"math/big"
	"testing"
)

func TestPowerSeries(t *testing.T) {
	want := []*big.Int{
		big.NewInt(1),    // 1^1
		big.NewInt(4),    // 2^2
		big.NewInt(27),   // 3^3
		big.NewInt(256),  // 4^4
		big.NewInt(3125), // 5^5
	}
	n := len(want)
	i := 0
	for got := range PowerSeries(n) {
		if got.Cmp(want[i]) != 0 {
			t.Errorf("PowerSeries[%v] => %v. Want: %v", i, got, want[i])
		} else {
			t.Logf("PowerSeries[%v] => %v", i, got)
		}
		i++
	}
}

func TestPowerSeriesSum(t *testing.T) {
	n := 10
	want := big.NewInt(10405071317)
	got := PowerSeriesSum(n)
	if got.Cmp(want) != 0 {
		t.Errorf("PowerSeriesSum(%v) => %s. want: %s", n, got, want)
	}
}
