package main

import (
	"flag"
	"fmt"
	"math"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

func GeneratePatterns(n int) [][]bool {
	patterns := GeneratePatterns2(n)
	patterns = patterns[1:len(patterns)]
	return patterns
	// return patterns[1:len(patterns)]
}

func GeneratePatterns2(n int) [][]bool {
	if n == 1 {
		return [][]bool{
			[]bool{false},
		}
	}
	if n == 2 {
		return [][]bool{
			[]bool{false, false},
			[]bool{true, false},
		}
	}
	subPatterns := GeneratePatterns2(n - 1)
	patterns := make([][]bool, 0)
	for _, p := range subPatterns {
		patterns = append(patterns, append([]bool{false}, p...))
	}
	for _, p := range subPatterns {
		patterns = append(patterns, append([]bool{true}, p...))
	}
	return patterns
}

func IntToSlice(num int) []int {
	out := make([]int, 0, 10)
	for num > 0 {
		digit := num % 10
		out = append(out, digit)
		num = (num - digit) / 10
	}
	return out
}

func SliceToInt(num []int) int {
	var out int
	for i := len(num) - 1; i >= 0; i-- {
		digit := num[i]
		out = out * 10
		out += digit
	}
	return out
}

func ApplyPatternSlice(pattern []bool, num []int) [][]int {
	out := [][]int{}
	start := 0
	end := len(num)
	if pattern[0] {
		start = 1 // don't attempt a leading 0
	}
	for i := start; i < 10; i++ {
		newNum := make([]int, len(num))
		for j := range num {
			if pattern[end-j-1] {
				newNum[j] = i
			} else {
				newNum[j] = num[j]
			}
		}
		out = append(out, newNum)
	}
	return out
}

func ApplyPattern(pattern []bool, num int) []int {
	numSlice := IntToSlice(num)
	outSlice := ApplyPatternSlice(pattern, numSlice)
	out := make([]int, len(outSlice))
	for i := range outSlice {
		out[i] = SliceToInt(outSlice[i])
	}
	return out
}

func NDigitPrimes(s *prime.Sieve, digits int) []int {
	maxNum := int(math.Pow(10.0, float64(digits)))
	minNum := int(math.Pow(10.0, float64(digits-1)))
	out := []int{}
	for _, p := range s.Primes() {
		if p >= minNum && p < maxNum {
			out = append(out, p)
		}
	}
	return out
}

type PrimeFamily struct {
	base    int
	primes  int
	pattern []bool
}

func PrimeFamilies(digits int) PrimeFamily {
	maxPrime := int(math.Pow(10.0, float64(digits)))
	s := prime.NewSieve(maxPrime)
	seen := make(map[int]bool)
	primes := NDigitPrimes(s, digits)
	patterns := GeneratePatterns(digits)
	maxVariants := 0
	var maxPattern []bool
	var maxBase int
	for _, p := range primes {
		if s := seen[p]; s {
			continue
		}
		for _, pattern := range patterns {
			count := 0
			variants := ApplyPattern(pattern, p)
			for _, variant := range variants {
				seen[variant] = true
				if isP, err := s.IsPrime(variant); isP && err == nil {
					count++
				}
				if count > maxVariants {
					maxVariants = count
					maxPattern = pattern
					maxBase = p
				}
			}
		}
	}
	return PrimeFamily{base: maxBase, primes: maxVariants, pattern: maxPattern}
}

func main() {
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		f := PrimeFamilies(arg)
		fmt.Println(f)
		fmt.Println(ApplyPattern(f.pattern, f.base))
	}
}
