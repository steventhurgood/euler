package main

import (
	"reflect"
	"testing"

	"bitbucket.org/steventhurgood/euler/prime"
)

func TestGeneratePatterns(t *testing.T) {
	want := [][]bool{
		[]bool{false, false, true, false},
		[]bool{false, true, false, false},
		[]bool{false, true, true, false},
		[]bool{true, false, false, false},
		[]bool{true, false, true, false},
		[]bool{true, true, false, false},
		[]bool{true, true, true, false},
	}
	got := GeneratePatterns(4)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("GeneratePatterns(4) =>\n%v\nwant:\n%v", got, want)
	}
}

func TestApplyPattern(t *testing.T) {
	pattern := []bool{true, true, false, false}
	num := 1234
	want := []int{
		1134,
		2234,
		3334,
		4434,
		5534,
		6634,
		7734,
		8834,
		9934,
	}
	got := ApplyPattern(pattern, num)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("ApplyPattern(%v, %v) =>\n%v\nwant:\n%v", pattern, num, got, want)
	}
}

func TestIntToSlice(t *testing.T) {
	want := []int{6, 5, 4, 3, 2, 1, 0, 1}
	n := 10123456
	got := IntToSlice(n)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("IntToSlice(%v) => %v. Want: %v", n, got, want)
	}
}

func TestSliceToInt(t *testing.T) {
	want := 1234
	n := []int{4, 3, 2, 1}
	got := SliceToInt(n)
	if got != want {
		t.Errorf("SliceToInt(%v) => %v. Want: %v", n, got, want)
	}
}

func TestApplyPatternSlice(t *testing.T) {
	pattern := []bool{false, false, true, false}
	num := []int{4, 3, 2, 1} // 1234 -> 1204,1214,1224,1234,1244,1254,1264,1274,1284,1294
	want := [][]int{
		[]int{4, 0, 2, 1},
		[]int{4, 1, 2, 1},
		[]int{4, 2, 2, 1},
		[]int{4, 3, 2, 1},
		[]int{4, 4, 2, 1},
		[]int{4, 5, 2, 1},
		[]int{4, 6, 2, 1},
		[]int{4, 7, 2, 1},
		[]int{4, 8, 2, 1},
		[]int{4, 9, 2, 1},
	}
	got := ApplyPatternSlice(pattern, num)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("ApplyPatternSlice(%v, %v) =>\n%v\nwant:\n%v", pattern, num, got, want)
	}
}

func TestNDigitPrimes(t *testing.T) {
	want := []int{2, 3, 5, 7}
	s := prime.NewSieve(100)
	got := NDigitPrimes(s, 1)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("NDigitPrimes(2) => %v. Want: %v", got, want)
	}
}
