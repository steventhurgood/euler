package main

import (
	"flag"
	"fmt"
	"math/big"
)

func SqrtTwo(iterations int) *big.Rat {
	one := big.NewRat(1, 1)
	s := recSqrtTwo(iterations)
	s = s.Add(s, one)
	return s
}

func recSqrtTwo(iterations int) *big.Rat {
	two := big.NewRat(2, 1)
	if iterations == 0 {
		return big.NewRat(1, 2)
	} else {
		s := recSqrtTwo(iterations - 1)
		s.Add(s, two)
		s = s.Inv(s)
		return s
	}
}

func NumDigits(n int) int {
	count := 0
	if n == 0 {
		return 1
	}
	for n > 0 {
		n /= 10
		count++
	}
	return count
}

func main() {
	upto := flag.Int("upto", 1000, "find expanded sqrt(2) up to n iterations")
	flag.Parse()
	count := 0
	for i := 0; i <= *upto; i++ {
		f := SqrtTwo(i)
		n := len(f.Num().String())
		d := len(f.Denom().String())
		if n > d {
			count++
			// fmt.Printf("%v %v ✔\n", i+1, f.String())
		} else {
			// fmt.Printf("%v %v\n", i+1, f.String())
		}
	}
	fmt.Printf("n > d: %v\n", count)

}
