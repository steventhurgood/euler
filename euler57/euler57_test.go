package main

import (
	"math/big"
	"testing"
)

func TestSqrtTwo(t *testing.T) {
	tests := []*big.Rat{
		big.NewRat(3, 2),
		big.NewRat(7, 5),
		big.NewRat(17, 12),
		big.NewRat(41, 29),
	}
	for i, want := range tests {
		got := SqrtTwo(i)
		if got.Cmp(want) != 0 {
			t.Errorf("SqrtTwo(%v) -> %v. Want: %v", i, got, want)
		}

	}
}
