package spiral

import (
	"log"
	"testing"
)

func TestSpiralBase(t *testing.T) {
	pairs := SpiralPath(1, 1)
	log.Print(pairs, len(pairs))
	expected := Position{0, 0}
	if len(pairs) != 1 || pairs[0] != expected {
		t.Error("spiral(1, 1) != [(0,0)]. Got: ", pairs)
	}
}

func TestSpiralPath(t *testing.T) {
	path := SpiralPath(3, 3)
	expected := []Position{
		Position{2, 1},
		Position{2, 2},
		Position{1, 2},
		Position{0, 2},
		Position{0, 1},
		Position{0, 0},
		Position{1, 0},
		Position{2, 0},
	}
	if len(path) != len(expected) {
		t.Error("actual path: ", path, " expected: ", expected)
	}
}
