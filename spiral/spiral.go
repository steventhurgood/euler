package spiral

import (
	"bytes"
	"fmt"
	"log"
	"math"
)

type Spiral [][]int

type Position [2]int

func DiagonalPath(squareSize int) []Position {
	positions := []Position{}
	for i := 0; i < squareSize; i++ {
		positions = append(positions, Position{i, i})
		if i != squareSize/2 {
			positions = append(positions, Position{i, squareSize - 1 - i})
		}
	}
	return positions
}
func SpiralPath(squareSize, spiralSize int) []Position {
	if spiralSize > squareSize {
		log.Fatal("square size (", spiralSize, ") > spiral size (", squareSize, ")")
	}
	if spiralSize == 1 {
		return []Position{[2]int{squareSize / 2, squareSize / 2}}
	}
	// num := (spiralSize-2)*(spiralSize-2) + 1
	offset := (squareSize - spiralSize) / 2
	pos := Position{spiralSize - 1 + offset, 1 + offset}
	positions := []Position{pos}
	// DOWN
	for i := 0; i < spiralSize-2; i++ {
		pos[1]++
		positions = append(positions, pos)
	}
	// LEFT
	for i := 0; i < spiralSize-1; i++ {
		pos[0]--
		positions = append(positions, pos)
	}
	// UP
	for i := 0; i < spiralSize-1; i++ {
		pos[1]--
		positions = append(positions, pos)
	}
	for i := 0; i < spiralSize-1; i++ {
		pos[0]++
		positions = append(positions, pos)
	}
	return positions
}

func NewSpiral(size int) Spiral {
	if size%2 != 1 {
		log.Fatal("Spiral must be an odd number size. Got: ", size)
	}
	n := 1
	s := make([][]int, size)
	for i := range s {
		s[i] = make([]int, size)
	}
	for spiralSize := 1; spiralSize <= size; spiralSize += 2 {
		path := SpiralPath(size, spiralSize)
		for _, p := range path {
			x, y := p[0], p[1]
			s[y][x] = n
			n++
		}
	}
	return s
}

func (s Spiral) String() string {
	buf := bytes.NewBuffer([]byte{})
	maxChars := int(math.Ceil(math.Log10(float64(len(s) * len(s)))))
	formatString := fmt.Sprintf("%%%dd ", maxChars)
	for _, row := range s {
		for _, col := range row {
			buf.WriteString(fmt.Sprintf(formatString, col))
		}
		buf.WriteString("\n")
	}
	return buf.String()
}
