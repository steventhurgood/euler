package main

import (
	"flag"
	"fmt"

	"bytes"

	"bitbucket.org/steventhurgood/euler/intargs"
)

type Combs [][]int

func (combs Combs) String() string {
	var out bytes.Buffer
	for n := range combs {
		fmt.Fprintf(&out, "%vCc -", n)
		for c := range combs[n] {
			fmt.Fprintf(&out, " %v", combs[n][c])
		}
		fmt.Fprintf(&out, "\n")
	}
	return out.String()
}

func AllCombs(maxn, threshold int) Combs {
	combs := make([][]int, maxn+1)
	for n := 0; n <= maxn; n++ {
		combs[n] = make([]int, n+1)
		combs[n][0] = 1
		combs[n][n] = 1
		for c := 1; c <= (n+1)/2; c++ {
			intermediate := combs[n-1][c-1]
			if intermediate <= threshold {
				intermediate = n * intermediate / c
			}
			combs[n][c] = intermediate
			combs[n][n-c] = intermediate
		}
	}
	return combs
}

func CombOver(n, c, threshold int) int {
	if c == 0 {
		return 1
	}
	if c > (n / 2) {
		return CombOver(n, n-c, threshold)
	}
	intermediate := CombOver(n-1, c-1, threshold)
	if intermediate > threshold {
		return intermediate
	} else {
		return n * intermediate / c
	}
}

// Comb - return the number of ways of choosing c items out of n
func Comb(n, c uint64) uint64 {
	if c == 0 {
		return 1
	}
	if c > (n / 2) {
		return Comb(n, n-c)
	}
	return n * Comb(n-1, c-1) / c
}

func main() {
	threshold := flag.Int("threshold", 1000000, "threshold")
	precompute := flag.Bool("precompute", false, "precompute combination values")
	flag.Parse()
	count := 0
	var allCombs Combs
	for _, arg := range intargs.IntArgs(flag.Args()) {
		if *precompute {
			allCombs = AllCombs(arg, *threshold)
		}
		for n := 1; n <= arg; n++ {
			for c := 0; c <= n; c++ {
				var result int
				if *precompute {
					result = allCombs[n][c]

				} else {
					result = CombOver(n, c, *threshold)
				}
				// if (result > uint64(*threshold) && result2 <= *threshold) || (result <= uint64(*threshold) && result2 > *threshold) {
				//	fmt.Printf("Diff: %v <> %v\n", result, result2)
				//}
				if result > *threshold {
					count++
					// fmt.Printf("%v C %v: %v\n", n, c, result)
				}
			}
		}
	}
	fmt.Printf("Total: %v\n", count)
}
