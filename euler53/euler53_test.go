package main

import "testing"

func TestComb(t *testing.T) {
	tests := [][]uint64{
		[]uint64{5, 0, 1},
		[]uint64{5, 3, 10},
	}
	for _, test := range tests {
		want := test[2]
		n, c := test[0], test[1]
		got := Comb(n, c)
		if got != want {
			t.Errorf("Comb(%v, %v) => %v. Want: %v", n, c, got, want)
		}
	}
}
