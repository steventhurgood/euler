package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"bitbucket.org/steventhurgood/euler/prime"
)

var (
	threshold  = flag.Float64("threshold", 0.1, "minimum ratio of diagonal primes")
	cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
)

// The diagonals of an spiral of natural numbers
type SpiralDiagonal struct {
	n, primes, count int
	sieve            *prime.Sieve
}

func NewSpiral(sieve *prime.Sieve) *SpiralDiagonal {
	return &SpiralDiagonal{
		n:      3,
		primes: 3,
		count:  5,
		sieve:  sieve,
	}
}

func NewSpiralSize(n int, sieve *prime.Sieve) *SpiralDiagonal {
	s := NewSpiral(sieve)
	for n > 1 {
		s.Grow()
		n -= 2
	}
	return s
}

func (s *SpiralDiagonal) Grow() {
	s.n += 2
	s.count += 4
	max := 4
	if s.n%3 == 0 {
		max = 3
	}
	for i := 1; i < max; i++ {
		candidate := s.n*s.n - i*(s.n-1)
		if is, err := s.sieve.IsLargePrime(candidate); is {
			if err != nil {
				log.Fatal(err)
			}
			s.primes++
		}

	}
}

func (s *SpiralDiagonal) Ratio() float64 {
	return float64(s.primes) / float64(s.count)
}

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	sieve := prime.NewSieve(10000000)
	s := NewSpiralSize(1, sieve)
	for {
		// fmt.Println(s, s.Ratio())
		if s.Ratio() < *threshold {
			fmt.Println(s, s.Ratio())
			break
		}
		s.Grow()
	}
}
