package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

func CollatzLength(n int) int {
	length := 0
	for n > 1 {
		length++
		if n%2 == 0 {
			n = n / 2
		} else {
			n = (3 * n) + 1
		}
	}
	return length
}

func Collatz(n int) []int {
	sequence := make([]int, 0)
	for n > 1 {
		sequence = append(sequence, n)
		if n%2 == 0 {
			n = n / 2
		} else {
			n = (3 * n) + 1
		}
	}
	sequence = append(sequence, n)
	return sequence
}

func FindLongest(upto int) []int {
	longestLength := 0
	longestStart := -1
	for i := 2; i <= upto; i++ {
		length := CollatzLength(i)
		if length > longestLength {
			longestStart = i
			longestLength = length
		}
	}
	return Collatz(longestStart)
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		l := FindLongest(int(n))
		fmt.Println(l)
	}
}
