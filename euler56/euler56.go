package main

import (
	"flag"
	"fmt"
	"log"
)

type BigInt struct {
	base   int
	digits []uint64
}

func NewBigInt(base int) *BigInt {
	return &BigInt{
		base:   base,
		digits: make([]uint64, 0),
	}
}

func (b *BigInt) Set(n uint64) *BigInt {
	b.digits = make([]uint64, 0)
	base := uint64(b.base)
	for n > 0 {
		digit := n % base
		b.digits = append(b.digits, (digit))
		n = n / base
	}
	return b
}

func (b *BigInt) Int() uint64 {
	r := uint64(0)
	base := uint64(b.base)
	end := len(b.digits) - 1
	for i := range b.digits {
		r *= base
		r += b.digits[end-i]
	}
	return r
}

func (a *BigInt) Mul(b *BigInt) *BigInt {
	if a.base != b.base {
		log.Fatalf("Invalid bases: %v, %v", a.base, b.base)
	}
	digits := make([]uint64, len(a.digits)+len(b.digits))
	for i := range a.digits {
		for j := range b.digits {
			power := i + j
			digits[power] += a.digits[i] * b.digits[j]
		}
	}
	return &BigInt{
		base:   a.base,
		digits: digits,
	}
}

func (a *BigInt) Flatten() *BigInt {
	var carry uint64
	base := uint64(a.base)
	for i := range a.digits {
		a.digits[i] += carry
		carry = 0
		if a.digits[i] >= base {
			carry = a.digits[i] / base
			a.digits[i] = a.digits[i] % base
		}
		for a.digits[i] >= base {
			a.digits[i] -= base
			carry++
		}
	}
	for carry > 0 {
		digit := carry % base
		a.digits = append(a.digits, digit)
		carry = carry / base
	}
	rightMostZeroes := 0
	end := len(a.digits) - 1
	for i := range a.digits {
		if a.digits[end-i] != 0 {
			break
		}
		rightMostZeroes++
	}
	a.digits = a.digits[0 : len(a.digits)-rightMostZeroes]
	return a
}

func (a *BigInt) Pow(n uint64) *BigInt {
	r := NewBigInt(a.base)
	base := &BigInt{
		base:   a.base,
		digits: make([]uint64, len(a.digits)),
	}
	copy(base.digits, a.digits)

	r.Set(1)
	i := 0
	for n > 0 {
		if n&1 == 1 {
			r = r.Mul(base)
		}
		r.Flatten()
		base.Flatten()
		i++
		n >>= 1
		base = base.Mul(base)
	}
	r.Flatten()
	return r
}

func (a *BigInt) Pow2(n uint64) *BigInt {
	r := NewBigInt(a.base)
	r.Set(1)
	for i := uint64(0); i < n; i++ {
		if i%10 == 0 {
			r.Flatten()
		}
		r = r.Mul(a)
	}
	r.Flatten()
	return r
}

func (a *BigInt) Sum() uint64 {
	var sum uint64
	for _, digit := range a.digits {
		sum += digit
	}
	return sum
}

func main() {
	upto := flag.Uint64("upto", 100, "find max digit sum of a^b for a, b < upto")
	flag.Parse()
	var max, maxBase, maxPower uint64
	var maxProduct *BigInt
	for i := uint64(0); i < *upto; i++ {
		a := NewBigInt(10)
		a.Set(i)
		for j := uint64(0); j < *upto; j++ {
			b := a.Pow(j)
			count := b.Sum()
			if count > max {
				max = count
				maxBase = i
				maxPower = j
				maxProduct = b
			}
		}
	}
	fmt.Printf("%v ^ %v -> %v (%v digit sum)", maxBase, maxPower, maxProduct, max)
}
