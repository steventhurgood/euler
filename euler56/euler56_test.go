package main

import (
	"reflect"
	"testing"
)

type BigIntTest struct {
	n      uint64
	digits []uint64
}

func TestBigInt(t *testing.T) {
	tests := []BigIntTest{
		BigIntTest{123, []uint64{3, 2, 1}},
		BigIntTest{0, []uint64{}},
		BigIntTest{99, []uint64{9, 9}},
	}
	for _, test := range tests {
		i := NewBigInt(10)
		i.Set(test.n)
		got := i.digits
		if !reflect.DeepEqual(got, test.digits) {
			t.Errorf("BigInt.Set(%v) -> %v. Want: %v", test.n, got, test.digits)
		}
	}

}

func TestInt(t *testing.T) {
	tests := []BigIntTest{
		BigIntTest{123, []uint64{3, 2, 1}},
		BigIntTest{0, []uint64{}},
		BigIntTest{99, []uint64{9, 9}},
	}
	for _, test := range tests {
		i := NewBigInt(10)
		i.Set(test.n)
		got := i.Int()
		t.Logf("BigInt(%v) => %v", test.n, i)
		if got != test.n {
			t.Errorf("BigInt.Int(%v) -> %v. Want: %v", i, got, test.n)
		}
	}
}

func TestMul(t *testing.T) {
	tests := [][]uint64{
		[]uint64{1, 1, 1},
		[]uint64{7, 10, 70},
		[]uint64{129, 20, 2580},
	}
	for _, test := range tests {
		a := NewBigInt(10)
		a.Set(test[0])
		b := NewBigInt(10)
		b.Set(test[1])
		got := a.Mul(b)
		t.Logf("%v * %v = %v", a, b, got)
		if got.Int() != test[2] {
			t.Errorf("BigInt(%v).Mul(%v) -> %v. Want: %v", a, b, got, test[2])
		}
	}
}

func TestFlatten(t *testing.T) {
	tests := [][]uint64{
		[]uint64{1, 1, 1},
		[]uint64{7, 10, 70},
		[]uint64{129, 20, 2580},
	}
	for _, test := range tests {
		a := NewBigInt(10)
		a.Set(test[0])
		b := NewBigInt(10)
		b.Set(test[1])
		got := a.Mul(b)
		got.Flatten()
		t.Logf("%v * %v = %v", a, b, got)
		if got.Int() != test[2] {
			t.Errorf("BigInt(%v).Mul(%v).Flatten() -> %v. Want: %v", a, b, got, test[2])
		}
		for _, digit := range got.digits {
			if digit > 9 {
				t.Errorf("Flatten() -> %v", got)
			}
		}
	}
}

func TestBigPow(t *testing.T) {
	base := NewBigInt(10).Set(9)
	pow := uint64(99)
	want := &BigInt{
		10,
		[]uint64{9, 8, 8, 4, 0, 0, 1, 1, 4, 9, 0, 7, 2, 3, 3, 0, 2, 0, 8, 7, 4, 3, 3, 4, 3, 4, 9, 6, 7, 6, 7, 3, 6, 4, 8, 8, 3, 6, 0, 6, 8, 8, 2, 3, 8, 3, 4, 0, 7, 1, 5, 2, 7, 2, 9, 5, 3, 4, 1, 3, 6, 3, 7, 7, 9, 1, 6, 2, 2, 0, 8, 4, 3, 5, 7, 8, 4, 1, 2, 5, 7, 2, 5, 6, 0, 3, 4, 5, 6, 6, 2, 1, 5, 9, 2},
	}
	got := base.Pow(pow)
	t.Logf("%v^%v ->\n%v", base, pow, got)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("%v^%v ->\n%v\nwant:\n%v", base, pow, got, want)
	}

}

func TestPow(t *testing.T) {
	tests := [][]uint64{
		[]uint64{10, 2, 100},
		[]uint64{1, 100, 1},
		[]uint64{33, 0, 1},
		[]uint64{7, 7, 823543},
		[]uint64{99, 9, 913517247483640899},
	}
	for _, test := range tests {
		a := NewBigInt(10)
		a.Set(test[0])
		b := test[1]
		got := a.Pow(b)
		t.Logf("%v * %v = %v", a, b, got)
		if got.Int() != test[2] {
			t.Errorf("BigInt(%v).Pow(%v) -> %v. Want: %v", a, b, got, test[2])
		}
		for _, digit := range got.digits {
			if digit > 9 {
				t.Errorf("Flatten() -> %v", got)
			}
		}
	}
}

func TestSum(t *testing.T) {
	tests := [][]uint64{
		[]uint64{123, 6},
		[]uint64{99999, 45},
	}
	for _, test := range tests {
		a := NewBigInt(10)
		a.Set(test[0])
		got := a.Sum()

		if got != test[1] {
			t.Errorf("Sum(%v) -> %v. Want: %v", a, got, test[1])
		}
	}
}

func BenchmarkPow(b *testing.B) {
	for i := 0; i < b.N; i++ {
		a := NewBigInt(10)
		a.Set(uint64(i))
		a.Pow(30)
	}
}

func BenchmarkPow2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		a := NewBigInt(10)
		a.Set(uint64(i))
		a.Pow2(30)
	}
}
