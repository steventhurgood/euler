package main

import (
	"testing"
)

var (
	testCases = map[int]string{
		0:     "zero",
		7:     "seven",
		11:    "eleven",
		21:    "twenty one",
		30:    "thirty",
		101:   "one hundred and one",
		221:   "two hundred and twenty one",
		300:   "three hundred",
		420:   "four hundred and twenty",
		1000:  "one thousand",
		2300:  "two thousand three hundred",
		7001:  "seven thousand and one",
		9091:  "nine thousand and ninety one",
		19999: "nineteen thousand nine hundred and ninety nine",
	}
)

func Test_NumToWords(t *testing.T) {
	for num, expected := range testCases {
		sw := NumToWords(num)
		if sw.String() != expected {
			t.Error("Did not succesfully identify:", expected, ", got:", sw.String())
		}
	}
}
