package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"
)

var (
	units = []string{
		"zero",
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten",
		"eleven",
		"twelve",
		"thirteen",
		"fourteen",
		"fifteen",
		"sixteen",
		"seventeen",
		"eighteen",
		"nineteen"}
	tens = []string{
		"zero",
		"ten",
		"twenty",
		"thirty",
		"forty",
		"fifty",
		"sixty",
		"seventy",
		"eighty",
		"ninety",
	}
)

type NumWord []string

func (n *NumWord) String() string {
	return strings.Join(*n, " ")
}

func NumToWords(n int) NumWord {
	s := make([]string, 0, 10)
	if n < 20 {
		s = append(s, units[n])
		return s
	}
	if n < 100 {
		unitPart := n % 10
		tenPart := (n - unitPart) / 10
		s = append(s, tens[tenPart])
		if unitPart > 0 {
			for _, w := range NumToWords(unitPart) {
				s = append(s, w)
			}
		}
		return s
	}
	if n < 1000 {
		tenPart := n % 100
		hundredPart := (n - tenPart) / 100
		s = append(s, units[hundredPart])
		s = append(s, "hundred")
		if tenPart > 0 {
			s = append(s, "and")
			for _, w := range NumToWords(tenPart) {
				s = append(s, w)
			}
		}
		return s
	}
	if n < 1000000 {
		hundredPart := n % 1000
		thousandPart := (n - hundredPart) / 1000
		s = NumToWords(thousandPart)
		s = append(s, "thousand")
		if hundredPart > 0 {
			if hundredPart < 100 {
				s = append(s, "and")
			}
			for _, w := range NumToWords(hundredPart) {
				s = append(s, w)
			}
		}
		return s
	}
	log.Fatal("Number too large:", n)
	return nil
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		sum := 0
		for i := 1; i <= int(n); i++ {
			sw := NumToWords(i)
			for _, word := range sw {
				sum += len(word)
			}
			fmt.Println(sw.String())
		}
		fmt.Println("sum =", sum)
	}
}
