package main

import (
	"flag"
	"fmt"
	"sort"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

// LongestConsecutivePrimePartition returns the starting prime, and length
func LongestConsecutivePrimePartition2(n int) (int, int) {
	s := prime.NewSieve(n)
	primes := s.Primes()
	maxChain := 0
	maxPrime := -1
	for i, p := range primes { // for each potential prime sum
		if p == 2 {
			sum := 0
			for j := 0; j+i < len(primes); j++ {
				sum += primes[i+j]
				if j > maxChain && sum < n {
					if isP, err := s.IsPrime(sum); isP && err == nil {
						maxChain = j
						maxPrime = sum
					}
				}
			}
		} else {
			sum := p
			for j := 1; j+i+1 < len(primes); j += 2 {
				sum += primes[i+j]
				sum += primes[i+j+1]
				if j+1 > maxChain && sum < n {
					if isP, err := s.IsPrime(sum); isP && err == nil {
						maxChain = j + 1
						maxPrime = sum
					}
				}
			}
		}
	}
	return maxPrime, maxChain
}

func LongestConsecutivePrimePartition(n int) []int {
	l := []int{}
	numItems := 0
	partitions := ConsecutivePrimePartition(n)
	for _, p := range partitions {
		if len(p) > numItems {
			numItems = len(p)
			l = p
		}
	}
	return l
}

type ByLength [][]int

func (s ByLength) Len() int {
	return len(s)
}

func (s ByLength) Less(a, b int) bool {
	return len((s)[a]) < len((s)[b])
}

func (s ByLength) Swap(a, b int) {
	(s)[a], (s)[b] = (s)[b], (s)[a]
}

// Find primes up to n that can be formed by summing consecutive primes
// return a list of the sequences of primes that meet these criteria

func ConsecutivePrimePartition(n int) [][]int {
	r := make([][]int, 0)
	s := prime.NewSieve(n)
	primes := s.Primes()
	numPrimes := len(primes)
	maxPrimeLen := 0
	for i := range primes {
		sum := primes[i]
		consecutivePrimes := []int{primes[i]}
		for k := 1; k < numPrimes-i; k++ {
			if primes[k]+primes[i] > n {
				break
			}
			consecutivePrimes = append(consecutivePrimes, primes[i+k])
			sum += primes[i+k]
			if k < maxPrimeLen {
				continue
			}
			if sum < n {
				if isP, err := s.IsPrime(sum); isP && err == nil && k > maxPrimeLen {
					t := make([]int, k+1)
					copy(t, consecutivePrimes)
					r = append(r, t)
					maxPrimeLen = k
				}
			}
		}
	}
	return r
}

func ConsecutivePrimePartition2(n int) [][]int {
	r := make([][]int, 0)
	s := prime.NewSieve(n)
	primes := s.Primes()
	for _, p := range primes { // for each potential prime sum
		for i := len(primes) - 1; i >= 0; i-- { // for each potential starting point
			if primes[i] > p {
				continue // the starting prime is larger than our test prime
			}
			testp := p
			consecutivePrimes := make([]int, 0)
			j := 0
			for {
				if i-j < 0 {
					break
				}
				testp = testp - primes[i-j]
				consecutivePrimes = append(consecutivePrimes, primes[i-j])

				if testp == 0 && j > 0 {
					r = append(r, consecutivePrimes)
					break
				}
				if testp < 0 {
					break
				}
				j++

			}
		}
	}
	return r
}

func main() {
	all := flag.Bool("all", false, "show all sequences, rather than just the longest")
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		if *all {
			sequences := ConsecutivePrimePartition(arg)
			sort.Sort(ByLength(sequences))
			for _, sequence := range sequences {
				sum := 0
				for _, n := range sequence {
					sum += n
				}
			}
		} else {
			maxPrime, maxChain := LongestConsecutivePrimePartition2(arg)
			fmt.Println(maxPrime, maxChain+1)
		}
	}
}
