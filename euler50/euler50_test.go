package main

import "testing"

func TestConsecutivePrimePartition(t *testing.T) {
	n := 100
	// want := []int{13, 11, 7, 5, 3, 2}
	want := []int{2, 3, 5, 7, 11, 13}
	got := LongestConsecutivePrimePartition(n)
	if len(got) != len(want) {
		t.Errorf("LongestConsecutivePrimePartition => %v. Want: %v", got, want)
	} else {
		for i := range got {
			if got[i] != want[i] {
				t.Errorf("LongestConsecutivePrimePartition[%v] => %v. Want: %v", i, got, want)
			}
		}
	}
}
