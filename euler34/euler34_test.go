package main

import "testing"

func TestIsFactorialDigit(t *testing.T) {
	b, sum := IsFactorialDigit(40585)
	if !b {
		t.Error("expected 40585 to be a factorial digit. Got: ", sum)
	}
}
