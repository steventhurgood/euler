package main

import "fmt"

var (
	factorial [10]int
)

func init() {
	n := 1
	factorial[0] = 1
	for i := 1; i < 10; i++ {
		n *= i
		factorial[i] = n
	}
}

func IsFactorialDigit(n int) (bool, int) {
	sum := 0
	original := n
	for n > 0 {
		digit := n % 10
		sum += factorial[digit]
		// log.Print(original, " + ", digit, "!", " = ", sum)
		n = (n - digit) / 10
	}
	return original == sum, sum
}

func main() {
	factorials := [][]int{}
	for i := 10; i <= 40*factorial[9]; i++ {
		if b, sum := IsFactorialDigit(i); b {
			factorials = append(factorials, []int{i, sum})
		}
	}
	sum := 0
	for _, f := range factorials {
		fmt.Println(f)
		sum += f[0]
	}
	fmt.Println("Sum: ", sum)
}
