package main

import (
	"testing"
)

func TestCheckPerfect(t *testing.T) {
	s := NewSieve(20000)
	perfect := s.CheckPerfect(28)
	if perfect != Perfect {
		t.Error("12 should be perfect(1). Got:", perfect)
	}
	perfect = s.CheckPerfect(12)
	if perfect != Abundant {
		t.Error("12 should be abundant(2). Got:", perfect)
	}
}
