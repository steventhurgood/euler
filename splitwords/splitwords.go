package splitwords

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
)

func SplitNames(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// cases:
	// Start: fast forward to next ", then fast forward to the quote after that. Return the bit inbetween
	firstCommaIndex := bytes.IndexByte(data, '"')
	if firstCommaIndex < 0 {
		if atEOF {
			return 0, nil, errors.New(fmt.Sprint("Expected to find \" in input: ", data))
		}
		return 0, nil, nil
	}
	lastCommaIndex := bytes.IndexByte(data[firstCommaIndex+1:], '"')
	if lastCommaIndex < 0 {
		if atEOF {
			lastCommaIndex = len(data) - 1
		}
		return 0, nil, nil
	}
	name := data[firstCommaIndex+1 : lastCommaIndex+firstCommaIndex+1]
	return lastCommaIndex + firstCommaIndex + 2, name, nil
}

func ReadNames(r io.Reader) []string {
	scanner := bufio.NewScanner(r)
	names := make([]string, 0)
	scanner.Split(SplitNames)
	for scanner.Scan() {
		name := scanner.Text()
		names = append(names, name)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return names
}

func Names(r io.Reader) chan string {
	ch := make(chan string)
	scanner := bufio.NewScanner(r)
	scanner.Split(SplitNames)
	go func() {
		for scanner.Scan() {
			name := scanner.Text()
			ch <- name
		}
		close(ch)
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}()
	return ch
}
