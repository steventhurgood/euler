package splitwords

import (
	"fmt"
	"strings"
	"testing"
)

func TestReadNames(t *testing.T) {
	testString := strings.NewReader("\"MARY\", \"PATRICIA\", \"LINDA\"")
	expected := []string{"MARY", "PATRICIA", "LINDA"}
	expectedStr := fmt.Sprint(expected)
	names := ReadNames(testString)
	namesStr := fmt.Sprint(names)
	if expectedStr != namesStr {
		t.Error("Expected: ", expectedStr, " got: ", namesStr)
	}
}
