package main

import (
	"fmt"
)

func IsPythagoreanTriplet(a, b, c int) bool {
	return (a*a)+(b*b) == (c * c)
}

func main() {
	// constraint: a + b + c = 1000
	// a^2 + b^2 = c^2
	// a + b > c, thus c < 500.
	// c >= 5
	for c := 5; c < 500; c++ {
		for a := 1; a < c; a++ {
			// a^2 + b^2 = c^2
			// b = sqrt(c^2 - a^2)
			b := 1000 - c - a
			if IsPythagoreanTriplet(a, int(b), c) {
				fmt.Println(a, b, c, a*b*c)
			}
		}
	}
}
