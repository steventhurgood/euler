package prime

import ()

func Gcd(a, b int) int {
	if a == 1 || b == 1 {
		return 1
	}
	for {
		if a == b {
			return a
		}
		if a > b {
			a = a % b
		}
		if a == 0 {
			return b
		}
		if b > a {
			b = b % a
		}
		if b == 0 {
			return a
		}
	}
}

func GcdList(numbers []int) int {
	if len(numbers) < 1 {
		return 0
	}
	for {
		same := true
		first := numbers[0]
		min := numbers[0]
		mini := 0
		for i, n := range numbers {
			if n != first {
				same = false
			}
			if n < min {
				min = n
				mini = i
			}
		}
		if same {
			return first
		}
		for i := range numbers {
			if i != mini {
				numbers[i] = numbers[i] % min
			}
		}
	}
}

func Coprimes(maxA, maxB int) [][]int {
	coprimes := make([][]int, 0)
	for a := 1; a < maxA; a++ {
		for b := 1; b < maxB; b++ {
			gcd := Gcd(a, b)
			if gcd == 1 {
				coprimes = append(coprimes, []int{a, b})
			}
		}
	}
	return coprimes
}
