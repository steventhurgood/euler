package prime

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"math"
	"sort"
)

func (s *Sieve) Primes() []int {
	return s.primes
}
func (s *Sieve) buildPrimes() []int {
	r := make([]int, 0, 1024)
	for prime, isPrime := range s.isPrime {
		if isPrime {
			r = append(r, prime)
		}
	}
	sort.Ints(r)
	return r
}

type Sieve struct {
	divisable []bool
	isPrime   map[int]bool
	primes    []int
	size      int
}

type Factor map[int]int
type Divisor map[int]int

func (d Divisor) String() string {
	buf := []byte{}
	out := bytes.NewBuffer(buf)
	primes := []int{}
	for p, _ := range d {
		primes = append(primes, p)
	}
	sort.Ints(primes)
	for i, p := range primes {
		if i > 0 {
			fmt.Fprint(out, " x ")
		}
		fmt.Fprintf(out, "%d^%d", p, d[p])
	}
	return out.String()
}

func (f Factor) String() string {
	num := Unfactor(f)
	return fmt.Sprint(num)
}

type BasePower struct {
	Base  Factor
	Power int
}

func (b BasePower) String() string {
	return fmt.Sprintf("%v ^ %d", b.Base, b.Power)
}

func (s *Sieve) IsPrime(n int) (bool, error) {
	// if n > len(s.divisable) {
	//	return false, errors.New(fmt.Sprint(n, " is too big to test with sieve of size: ", len(s.divisable)))
	//}
	// return s.isPrime[n], nil
	return !s.divisable[n], nil
}

func (s *Sieve) IsLargePrime(n int) (bool, error) {
	maxPrime := s.size
	if n < maxPrime {
		return s.isPrime[n], nil
	}
	maxDiv := int(math.Sqrt(float64(n)))
	if n > 3 {
		// all primes other than 2 and 3 are of the form 6k-1 or 6k+1
		r := n % 6
		if !(r == 1 || r == 5) {
			return false, nil
		}
	}
	if maxDiv > maxPrime {
		return false, errors.New(fmt.Sprint(n, " is too big to test with sieve of size: ", len(s.divisable)))
	}
	return s.searchPrimes(n)
}

func (s *Sieve) searchPrimes(n int) (bool, error) {
	for _, p := range s.primes {
		if n%p == 0 {
			return false, nil
		}
	}
	return true, nil
}

func NewSieve(n int) *Sieve {
	s := &Sieve{
		size: n,
	}
	s.divisable = make([]bool, n)
	s.divisable[0] = true
	s.divisable[1] = true
	s.divisable[2] = false
	maxDivisor := int(math.Ceil(math.Sqrt(float64(n))))
	for i := 2; i <= maxDivisor; i++ {
		if !s.divisable[i] {
			for j := i * 2; j < n; j += i {
				s.divisable[j] = true
			}
		}
	}
	s.isPrime = make(map[int]bool)
	for i, divisable := range s.divisable {
		if !divisable {
			s.isPrime[i] = true
		}
	}
	s.primes = s.buildPrimes()
	return s
}

func (s *Sieve) PrimeDivisors(n int) []int {
	if !s.divisable[n] {
		return []int{n}
	}
	p := make([]int, 0, 100)
	maxDivisor := n / 2
	for i := 0; s.primes[i] <= maxDivisor; i++ {
		if n%s.primes[i] == 0 {
			p = append(p, s.primes[i])
		}
	}
	return p
}

func (s *Sieve) NumDivisors(n int) int {
	p := s.Primes()
	i := 0
	numPrimes := len(p)
	numDivisors := 1
	for {
		maxDivisor := int(math.Ceil(math.Sqrt(float64(n))))
		if i > maxDivisor {
			break
		}
		if i >= numPrimes {
			log.Fatal("Insufficient primes to factor:", n)
		}
		if n%p[i] == 0 {
			pow := 1
			for n%p[i] == 0 && n > 0 {
				pow++
				n = n / p[i]
			}
			numDivisors *= pow
		}
		i++
	}
	return numDivisors
}

func Unfactor(factors map[int]int) int {
	result := 1
	for prime, power := range factors {
		result *= int(math.Pow(float64(prime), float64(power)))
	}
	return result
}

func (s *Sieve) Divisors(n int) (Divisor, int) {
	if n == 0 {
		return map[int]int{}, 0
	}
	// p := s.Primes()
	p := s.primes
	divisors := make(map[int]int)
	i := 0
	numPrimes := len(p)
	for {
		if n == 1 {
			break
		}
		if i >= numPrimes {
			log.Fatal("Insufficient primes to factor:", n)
		}
		if n%p[i] == 0 {
			divisors[p[i]]++
			n = n / p[i]
		} else {
			i++
		}
	}
	numDivisors := 1
	for _, powers := range divisors {
		numDivisors *= (powers + 1)
	}
	return divisors, numDivisors
}

func subFactorsToDivisors(primes []int, factors map[int]int) []int {
	numDivisors := 1
	for _, powers := range factors {
		numDivisors *= (powers + 1)
	}
	divisors := make([]int, 0, numDivisors)
	if len(primes) == 0 {
		return []int{1}
	}
	prime := primes[0]
	maxPower := factors[prime]
	remainingDivisors := subFactorsToDivisors(primes[1:], factors)
	for i := 0; i <= maxPower; i++ {
		pow := int(math.Pow(float64(prime), float64(i)))
		for _, j := range remainingDivisors {
			divisors = append(divisors, pow*j)
		}
	}
	return divisors
}

// Take a map of prime factors/powers and expand it to divisors
func FactorsToDivisors(factors map[int]int) []int {
	primes := make([]int, 0, len(factors))
	for prime, _ := range factors {
		primes = append(primes, prime)
	}
	divisors := subFactorsToDivisors(primes, factors)
	sort.Ints(divisors)
	return divisors
}

func Sum(list []int) int {
	sum := 0
	for _, n := range list {
		sum += n
	}
	return sum
}

func (s *Sieve) ProperDivisors(a int) []int {
	factors, _ := s.Divisors(a)
	divisors := FactorsToDivisors(factors)
	return divisors[:len(divisors)-1]
}

func (s *Sieve) AmicablesUpTo(n int) []int {
	tested := make(map[int]bool)
	amicables := make([]int, 0, n)
	for i := 1; i < n; i++ {
		if tested[i] {
			continue
		}
		a, b, amicable := s.Amicable(i)
		if amicable {
			tested[a] = true
			tested[b] = true
			amicables = append(amicables, a)
			amicables = append(amicables, b)
		}
	}
	sort.Ints(amicables)
	return amicables
}

func (s *Sieve) Amicable(a int) (int, int, bool) {
	// log.Print("Amicable(", a, ")")
	divisors := s.ProperDivisors(a)
	// log.Print("Divisors: ", divisors)
	sum := Sum(divisors)
	// log.Print("Sum: ", sum)
	sumDivisors := s.ProperDivisors(sum)
	// log.Print("Sum Divisors: ", sumDivisors)
	sumSum := Sum(sumDivisors)
	// log.Print("Sum Sum: ", sumSum)
	return a, sum, a == sumSum && a != sum
}
