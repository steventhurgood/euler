package prime

import (
	"testing"
)

func TestGcd(t *testing.T) {
	n := Gcd(137*149, 137*211)
	if n != 137 {
		t.Error("expected GCD(137*149, 137(211) = 137. Got: ", n)
	}
}

func TestGcdCoprime(t *testing.T) {
	n := Gcd(15485863, 7)
	if n != 1 {
		t.Error("expected GCD(15,485,863, 7) = 1. Got: ", n)
	}
}
