package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

var (
	cache *PathCache
)

type PathCache struct {
	width, height int
	cache         []int
}

func NewPathCache(width, height int) *PathCache {
	cache := &PathCache{
		width:  width,
		height: height,
		cache:  make([]int, (width+1)*(height+1))}
	return cache
}

func (c *PathCache) Get(width, height int) int {
	offset := width*c.width + height
	length := c.cache[offset]
	if length == 0 {
		length = LatticePaths(width, height)
		c.Set(width, height, length)
	}
	return length
}

func (c *PathCache) Set(width, height, length int) {
	aOffset := width*c.width + height
	bOffset := height*c.width + width
	c.cache[aOffset] = length
	c.cache[bOffset] = length
}

func LatticePaths(width, height int) int {
	if width == 0 && height == 0 {
		return 0
	}
	if width == 0 || height == 0 {
		return 1
	}
	// both width and height are > 1; we can either go across, or down
	if width == height {
		either := LatticePaths(width-1, height)
		cache.Set(width-1, height, either)
		return 2 * either
	}
	across := cache.Get(width-1, height)
	down := cache.Get(width, height-1)
	return down + across
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) != 2 {
		log.Fatal("Usage: 15 width height")
	}
	widthArg := args[0]
	heightArg := args[1]
	width, err := strconv.ParseInt(widthArg, 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	height, err := strconv.ParseInt(heightArg, 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	cache = NewPathCache(int(width), int(height))
	fmt.Println(width, height)
	fmt.Println(LatticePaths(int(width), int(height)))
}
