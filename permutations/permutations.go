package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/pandigital"
)

func main() {
	p := flag.Int("p", 2, "Number of digits to permute in parallel")
	flag.Parse()
	for p := range pandigital.PermutationChannel(intargs.IntArgs(flag.Args()), *p) {
		fmt.Println(p)
	}
}
