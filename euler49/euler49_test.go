package main

import "testing"

func TestPermutationChecker(t *testing.T) {
	p := NewPermutation(1487)
	if !p.IsPermutation(4817) {
		t.Errorf("4817 is a permutation of 1487")
	}
	if !p.IsPermutation(8147) {
		t.Errorf("8147 is a permutation of 1487")
	}
	if p.IsPermutation(814) {
		t.Errorf("814 is not a permutation of 1487")
	}
	if p.IsPermutation(8141) {
		t.Errorf("8141 is not a permutation of 1487")
	}
	if p.IsPermutation(8145) {
		t.Errorf("8145 is not a permutation of 1487")
	}
	if p.IsPermutation(14873) {
		t.Errorf("14873 is not a permutation of 1487")
	}
}

func TestFindPrimePermutations(t *testing.T) {
	want := map[[3]int]bool{
		[3]int{1487, 4817, 8147}: true,
	}
	i := 0
	for got := range FindPrimePermutations() {
		if i > 1 {
			t.Errorf("Too many solutions: (%v) %v", i, got)
		}
		if _, ok := want[got]; !ok {
			t.Errorf("Prime Permutation[%v] => %v. Want: %v", i, got, want)
		}
		i++
	}
	if i != 2 {
		t.Errorf("Not enough results: %v", i)
	}
}
