package main

import (
	"fmt"

	"bitbucket.org/steventhurgood/euler/prime"
)

type PermutationChecker map[int]int

func NewPermutation(n int) PermutationChecker {
	p := make(PermutationChecker)
	for n > 0 {
		digit := n % 10
		p[digit]++
		n = (n - digit) / 10
	}
	return p
}

func (p PermutationChecker) IsPermutation(n int) bool {
	pCopy := make(map[int]int)
	for k, v := range p {
		pCopy[k] = v
	}
	for n > 0 {
		digit := n % 10
		if v, ok := pCopy[digit]; !ok || v <= 0 {
			return false
		}
		n = (n - digit) / 10
		pCopy[digit]--
	}
	for _, v := range pCopy {
		if v > 0 {
			return false
		}
	}
	return true
}

func FindPrimePermutations() chan [3]int {
	ch := make(chan [3]int)
	s := prime.NewSieve(10000)
	go func() {
		defer close(ch)

		for _, p := range s.Primes() {
			perm := NewPermutation(p)
			for a := 1000; a < 10000; a++ {
				if p+2*a > 9999 {
					break
				}
				a1 := p + a
				a2 := a1 + a
				if a1p, a1err := s.IsPrime(a1); a1p && a1err == nil && perm.IsPermutation(a1) {
					if a2p, a2err := s.IsPrime(a2); a2p && a2err == nil && perm.IsPermutation(a2) {
						ch <- [3]int{p, a1, a2}
					}
				}
			}
		}
	}()
	return ch
}

func main() {
	for n := range FindPrimePermutations() {
		fmt.Println(n)
	}
}
