function getValue() {
	$.getJSON("/handler/counter", function(data) {
	var counterElement = $("#counter");
		counterElement.text(data.Count);
	})
}

function logMessage(data) {
	var button = $(document.createElement("button")).addClass("btn btn-default").text("edit");
	var buttonColumn = $(document.createElement("div")).addClass("col-md-2").append(button);
	var row = $(document.createElement("div")).addClass("row");

	button.click(function(e) {
		console.log("Edit: " + data.Key);
		row.html('<div class="col-md-10"><form role="form"><div class="form-group"><label for="edit-' + data.Key + '">Edit</div>' +
			'<textarea class="form-control" id="edit-' + data.Key + '" rows="3">' + data.Message + '</textarea></div>');
		var saveButton = $(document.createElement("button")).addClass("btn btn-default").text("save");
		var div = $(document.createElement("div")).addClass("col-md-2");
		div.append(saveButton);
		row.append(div);
		console.log(data.Key)
		saveButton.click(function(e) {
			console.log("saving: " + data.Key);
			$.ajax({
				type: "PUT",
				url: "/handler/log",
				dataType: "json",
				data: JSON.stringify({"Message": $("#edit-" + data.Key).val(), "Key": data.Key})})
				.done(function(newData) {
					console.log(newData);
	                		row.html('<div class="col-md-3">' + newData.Timestamp + '</div><div class="col-md-7">' + newData.Message + '</div>');
					row.append(buttonColumn);
				});
		});
	});
	row.html('<div class="col-md-3">' + data.Timestamp + '</div><div class="col-md-7">' + data.Message + '</div>');
	row.append(buttonColumn);
	return row;
};

function prependLogMessage(data) {
	var message = logMessage(data);
	message.prependTo("#logmessages");
	return message
};

function addLogMessage(data) {
	var message = logMessage(data);
	message.appendTo("#logmessages");
	return message
};


function loadLogMessages() {
	$.getJSON("/handler/log", function(data) {
		data.forEach(function(d) {
			addLogMessage(d);
		});
	})
}

$(document).ready(function() {
$("#increment").bind("click", function () {
	$.post("/handler/counter", JSON.stringify({"Count": 1}), function(data) {
	var counterElement = $("#counter");
		counterElement.text(data.Count);
	}, dataType="json");
});

$("#decrement").bind("click", function () {
	$.post("/handler/counter", JSON.stringify({"Count": -1}), function(data) {
	var counterElement = $("#counter");
		counterElement.text(data.Count);
	}, dataType="json");
});

$("#logmessage").keyup(function(e) {
	if (e.keyCode == 13)
	{
		var val = $(this).val();
		$.post("/handler/log", JSON.stringify({"Message": val}), function(data) {
			prependLogMessage(data);
		}, dataType="json").fail(function() {
			console.log("Error");
		});

		$(this).val("");
	}
});
getValue();
loadLogMessages();
});
