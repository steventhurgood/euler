package testhttp

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/steventhurgood/euler/prime"

	"appengine"
	"appengine/datastore"
)

var (
	sieve *prime.Sieve = prime.NewSieve(10000)
)

func init() {
	http.HandleFunc("/handler/prime", primeHandler)
	http.HandleFunc("/handler/counter", jsonHandler)
	http.HandleFunc("/handler/log", logHandler)
}

func primeHandler(w http.ResponseWriter, r *http.Request) {
	primes := sieve.Primes()
	fmt.Fprint(w, "<ul>")
	for _, p := range primes {
		fmt.Fprintf(w, "<li>%d</li>", p)
	}
	fmt.Fprint(w, "</ul>")
}

type JsonResponse struct {
	Count   int
	Message string
}

type JsonRequest struct {
	Count int
}

type Counter struct {
	Count int
}

type LogEntry struct {
	Timestamp time.Time
	Message   string
}

type LogEntryUpdateRequest struct {
	Message string
	Key     datastore.Key
}

type LogEntryResponse struct {
	Timestamp time.Time
	Message   string
	Key       datastore.Key
}

func logHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		{
			c := appengine.NewContext(r)
			q := datastore.NewQuery("Log")
			logEntries := []LogEntry{}
			keys, err := q.GetAll(c, &logEntries)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			logEntryResponses := make([]LogEntryResponse, 0, len(keys))
			for i := range keys {
				r := LogEntryResponse{
					Timestamp: logEntries[i].Timestamp,
					Message:   logEntries[i].Message,
					Key:       *keys[i],
				}
				logEntryResponses = append(logEntryResponses, r)

			}
			data, err := json.Marshal(logEntryResponses)
			log.Print(logEntryResponses)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			w.Write(data)
		}
	case "PUT":
		{
			c := appengine.NewContext(r)
			e := LogEntryUpdateRequest{}
			buf := make([]byte, 8*1024)
			bytesRead, err := r.Body.Read(buf)
			if err != nil {
				http.Error(w, fmt.Sprint("Error reading body: ", err.Error()), 500)
				return
			}
			err = json.Unmarshal(buf[:bytesRead], &e)
			if err != nil {
				http.Error(w, fmt.Sprint("Error unmarshalling json: ", err.Error(), buf[:bytesRead]), 500)
				return
			}
			log.Print(e)
			newE := LogEntry{
				Timestamp: time.Now(),
				Message:   e.Message,
			}
			k := &e.Key
			key, err := datastore.Put(c, k, &newE)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			resp := LogEntryResponse{
				Message:   newE.Message,
				Timestamp: newE.Timestamp,
				Key:       *key,
			}
			log.Print("Stored: ", key)
			data, err := json.Marshal(resp)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			w.Write(data)
		}
	case "POST":
		{
			c := appengine.NewContext(r)
			e := LogEntry{}
			k := datastore.NewIncompleteKey(c, "Log", nil)
			buf := make([]byte, 8*1024)
			bytesRead, err := r.Body.Read(buf)
			if err != nil {
				http.Error(w, fmt.Sprint("Error reading body: ", err.Error()), 500)
				return
			}
			err = json.Unmarshal(buf[:bytesRead], &e)
			if err != nil {
				http.Error(w, fmt.Sprint("Error unmarshalling json: ", err.Error(), buf[:bytesRead]), 500)
				return
			}
			log.Print(e)
			e.Timestamp = time.Now()
			key, err := datastore.Put(c, k, &e)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			log.Print("Stored: ", key)
			resp := LogEntryResponse{
				Timestamp: e.Timestamp,
				Message:   e.Message,
				Key:       *key,
			}
			data, err := json.Marshal(resp)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			w.Write(data)
		}
	default:
		{
			http.Error(w, fmt.Sprint("Unknown method: ", r.Method), 500)
		}

	}
}

func getCounter(c appengine.Context, e *Counter) error {
	k := datastore.NewKey(c, "Counter", "count", 0, nil)
	log.Print("Getting: ", e)
	if err := datastore.Get(c, k, e); err != nil {
		log.Print("Error: ", err.Error())
		return err
	}
	return nil
}

func counterIncrementor(count int, e *Counter) func(appengine.Context) error {
	return func(c appengine.Context) error {
		k := datastore.NewKey(c, "Counter", "count", 0, nil)
		log.Print("Incrementing: ", e)
		err := getCounter(c, e)
		if err != nil {
			log.Print("Error: ", err.Error())
			return err
		}
		e.Count += count
		log.Print("Incremented: ", e)
		key, err := datastore.Put(c, k, e)
		log.Print("key: ", key)
		if err != nil {
			return err
		}
		return nil
	}
}

func jsonHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		{
			log.Print("handling GET")
			w.Header().Add("Content-Type", "application/json")
			c := appengine.NewContext(r)
			e := Counter{}
			err := getCounter(c, &e)
			if err != nil {
				http.Error(w, err.Error(), 500)
			}
			response := JsonResponse{
				Count:   e.Count,
				Message: "Message Received",
			}
			data, err := json.Marshal(response)
			if err != nil {
				log.Print(err)
			}
			w.Write(data)
		}
	case "POST":
		{
			log.Print("handling POST")
			c := appengine.NewContext(r)
			e := Counter{}
			addE := Counter{}
			buf := make([]byte, 8*1024)
			bytesRead, err := r.Body.Read(buf)
			if err != nil {
				http.Error(w, fmt.Sprint("Error reading body: ", err.Error()), 500)
				return
			}
			err = json.Unmarshal(buf[:bytesRead], &addE)
			if err != nil {
				http.Error(w, fmt.Sprint("Error unmarshalling json: ", err.Error(), buf[:bytesRead]), 500)
				return
			}
			e.Count += addE.Count
			datastore.RunInTransaction(c, counterIncrementor(addE.Count, &e), nil)
			data, err := json.Marshal(e)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			w.Write(data)
		}
	default:
		{
			log.Print("unsupported method: ", r.Method)
		}
	}
}
