package main

import (
	"testing"

	"bitbucket.org/steventhurgood/euler/prime"
)

func BenchmarkSmallSieve(b *testing.B) {
	b.StopTimer()
	s := prime.NewSieve(1000)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		s.Divisors(997 * 997 * 997 * 991 * 991)
	}
}

func BenchmarkLargeSieve(b *testing.B) {
	b.StopTimer()
	s := prime.NewSieve(1000000)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		s.Divisors(997 * 997 * 997 * 991 * 991)
	}
}
