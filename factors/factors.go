package main

import (
	"flag"
	"fmt"
	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

func main() {
	sieveSize := flag.Int("sieve_size", 1000, "the size of the sieve to use for finding primes")
	flag.Parse()
	s := prime.NewSieve(*sieveSize)
	for _, n := range intargs.IntArgs(flag.Args()) {
		pdivisors := s.ProperDivisors(n)
		divisors, _ := s.Divisors(n)
		fmt.Println(divisors)
		fmt.Println(pdivisors)
	}
}
