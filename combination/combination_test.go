package combination

import (
	"math"
	"math/big"
	"testing"
)

const (
	epsilon = 0.000000001 // yay, testing floating points
)

func TestCombinations(t *testing.T) {
	tests := [][3]int{
		[3]int{1, 1, 1},
		[3]int{2, 1, 2},
		[3]int{10, 1, 10},
		[3]int{10, 5, 252},
	}
	for _, test := range tests {
		want := test[2]
		got := Combinations(test[0], test[1])
		if got != want {
			t.Errorf("Combination(%d, %d) got: %d. want: %d", test[0], test[1], got, want)
		}
	}
}

type BinomTest struct {
	n, k int
	p    float64
	want float64
}

func TestBinom(t *testing.T) {
	tests := []BinomTest{
		BinomTest{2, 1, 0.5, 0.5},     // choose(2,1) * 0.5 ^ 1 * 0.5^2 = 2 * 0.5 * 0.5
		BinomTest{5, 3, 0.2, 0.0512},  // choose(5,3) * 0.2 ^ 3 * (1-0.2)^(5-3) = 10 * 0.2^3 * 0.8^2
		BinomTest{5, 5, 0.2, 0.00032}, // choose(5,5) * 0.2^5 = 1 * 0.2^5
	}
	for _, test := range tests {
		want := test.want
		got := Binom(test.n, test.k, test.p)
		if math.Abs(got-want) > epsilon {
			t.Errorf("Binom(%d, %d, %f) got: %.10f want: %.10f", test.n, test.k, test.p, got, want)
		}
	}
}

func TestRightBinom(t *testing.T) {
	tests := []BinomTest{
		BinomTest{2, 1, 0.5, 0.75},    // P(successes >= 1)
		BinomTest{5, 3, 0.2, 0.05792}, // P(successes >= 3)
		BinomTest{5, 5, 0.2, 0.00032}, // P(successes >= 5)
	}
	for _, test := range tests {
		want := test.want
		got := RightBinom(test.n, test.k, test.p)
		if math.Abs(got-want) > epsilon {
			t.Errorf("Binom(%d, %d, %f) got: %.10f want: %.10f", test.n, test.k, test.p, got, want)
		}
	}
}

func BenchmarkRightBinomThousands(b *testing.B) {
	tests := []BinomTest{
		BinomTest{1000, 500, 0.5, 0.0},
		BinomTest{1000, 2, 0.5, 0.0},
		BinomTest{1000, 990, 0.5, 0.0},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RightBinom(test.n, test.k, test.p)
		}
	}
}

func BenchmarkRightBinomTenThousands(b *testing.B) {
	tests := []BinomTest{
		BinomTest{10000, 5000, 0.5, 0.0},
		BinomTest{10000, 2, 0.5, 0.0},
		BinomTest{10000, 9900, 0.5, 0.0},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RightBinom(test.n, test.k, test.p)
		}
	}
}

func BenchmarkRightBinomHundredThousands(b *testing.B) {
	tests := []BinomTest{
		BinomTest{100000, 5000, 0.5, 0.0},
		BinomTest{100000, 2, 0.5, 0.0},
		BinomTest{100000, 9900, 0.5, 0.0},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RightBinom(test.n, test.k, test.p)
		}
	}
}

type IntervalTest struct {
	n              int
	p, interval    float64
	lbound, ubound int
}

type RatBinomTest struct {
	n, k int64
	p    *big.Rat
	want *big.Rat
}

func TestRatBinom(t *testing.T) {
	tests := []RatBinomTest{
		RatBinomTest{2, 1, big.NewRat(1, 2), big.NewRat(1, 2)},      // choose(2,1) * 0.5 ^ 1 * 0.5^2 = 2 * 0.5 * 0.5
		RatBinomTest{5, 3, big.NewRat(1, 5), big.NewRat(160, 3125)}, // choose(5,3) * 1/5 ^ 3 * (1-1/5)^(5-3) = 10 * (1/5)^3 * (4/5)^2
		RatBinomTest{5, 5, big.NewRat(1, 5), big.NewRat(1, 3125)},   // choose(5,5) * (1/5)^5 = 1 * (1/5)^5
	}
	for _, test := range tests {
		got := RatBinom(test.n, test.k, test.p)
		want := test.want
		if got.Cmp(want) != 0 {
			t.Errorf("RatBinom(%v, %v, %v) got: %v want: %v", test.n, test.k, test.p, got, want)
		}
	}

}

func BenchmarkRatBinom(b *testing.B) {
	tests := []RatBinomTest{
		RatBinomTest{2, 1, big.NewRat(1, 2), nil},
		RatBinomTest{5, 3, big.NewRat(1, 5), nil},
		RatBinomTest{5, 5, big.NewRat(1, 5), nil},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RatBinom(test.n, test.k, test.p)
		}
	}
}

func BenchmarkRatBinomHundreds(b *testing.B) {
	tests := []RatBinomTest{
		RatBinomTest{200, 1, big.NewRat(1, 2), nil},
		RatBinomTest{100, 50, big.NewRat(1, 5), nil},
		RatBinomTest{500, 499, big.NewRat(1, 5), nil},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RatBinom(test.n, test.k, test.p)
		}
	}
}

func BenchmarkRatBinomThousands(b *testing.B) {
	tests := []RatBinomTest{
		RatBinomTest{2000, 1, big.NewRat(1, 2), nil},
		RatBinomTest{1000, 500, big.NewRat(1, 5), nil},
		RatBinomTest{5000, 4999, big.NewRat(1, 5), nil},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RatBinom(test.n, test.k, test.p)
		}
	}
}

type RatBinomDistTest struct {
	n    int64
	p    *big.Rat
	want []*big.Rat
}

func TestDistRatBinom(t *testing.T) {
	tests := []RatBinomDistTest{
		RatBinomDistTest{3, big.NewRat(1, 2), []*big.Rat{big.NewRat(1, 8), big.NewRat(3, 8), big.NewRat(3, 8), big.NewRat(1, 8)}},
		RatBinomDistTest{4, big.NewRat(1, 5), []*big.Rat{big.NewRat(256, 625), big.NewRat(256, 625), big.NewRat(96, 625), big.NewRat(16, 625), big.NewRat(1, 625)}},
	}
	for _, test := range tests {
		got := DistRatBinom(test.n, test.p)
		want := test.want
		if len(got) != len(want) {
			t.Errorf("DistRatBinom(%v, %v) got: %v, want: %v", test.n, test.p, got, want)
		} else {
			for i := range got {
				if got[i].Cmp(want[i]) != 0 {
					t.Errorf("DistRatBinom(%v, %v) got: %v, want: %v", test.n, test.p, got, want)
				}
			}
		}
	}
}

func TestRatBinomCDF(t *testing.T) {
	tests := []RatBinomDistTest{
		RatBinomDistTest{3, big.NewRat(1, 2), []*big.Rat{big.NewRat(1, 8), big.NewRat(4, 8), big.NewRat(7, 8), big.NewRat(1, 1)}},
		RatBinomDistTest{4, big.NewRat(1, 5), []*big.Rat{big.NewRat(256, 625), big.NewRat(512, 625), big.NewRat(608, 625), big.NewRat(624, 625), big.NewRat(1, 1)}},
	}
	for _, test := range tests {
		got := RatBinomCDF(test.n, test.p)
		want := test.want
		if len(got) != len(want) {
			t.Errorf("RatBinomCDF(%v, %v) got: %v, want: %v", test.n, test.p, got, want)
		} else {
			for i := range got {
				if got[i].Cmp(want[i]) != 0 {
					t.Errorf("DistRatBinom(%v, %v) got: %v, want: %v", test.n, test.p, got, want)
				}
			}
		}
	}
}

func BenchmarkRatBinomCDF(b *testing.B) {
	tests := []RatBinomDistTest{
		RatBinomDistTest{10, big.NewRat(1, 2), nil},
		RatBinomDistTest{1000, big.NewRat(1, 5), nil},
	}
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			RatBinomCDF(test.n, test.p)
		}
	}
}

type ERFTest struct {
	x, want float64
}

func TestERF(t *testing.T) {
	tests := []ERFTest{
		ERFTest{0, 0},
		ERFTest{1, math.Erf(1)},
		ERFTest{2, math.Erf(2)},
		ERFTest{3, math.Erf(3)},
	}
	for _, test := range tests {
		got := ERF(test.x)
		if got != test.want {
			t.Errorf("ERF(%v) got: %v, want: %v", test.x, got, test.want)
		}
	}
}
