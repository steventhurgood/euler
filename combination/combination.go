package combination

import (
	"log"
	"math"
	"math/big"
)

// find the number of combinations: n! / (k! * (n-k)!)
func Combinations(n, k int) int {
	r := 1
	for j := n; j > k; j-- {
		r *= j
	}
	for j := n - k; j > 1; j-- {
		r /= j
	}
	return r
}

func RatPow(base *big.Rat, p int64) *big.Rat {
	pow := big.NewRat(1, 1)
	for p > 0 {
		pow.Mul(pow, base)
		p--
	}
	return pow
}

func RatBinom(n, k int64, p *big.Rat) *big.Rat {
	one := big.NewRat(1, 1)
	notP := one.Sub(one, p)

	b := big.NewInt(0).Binomial(n, k)

	binom := big.NewRat(1, 1).SetInt(b)

	pSuccess := RatPow(p, k)
	pFail := RatPow(notP, n-k)

	binom.Mul(binom, pSuccess)
	binom.Mul(binom, pFail)
	return binom
}

func DistRatBinom(n int64, p *big.Rat) []*big.Rat {
	var d []*big.Rat
	for i := int64(0); i <= n; i++ {
		d = append(d, RatBinom(n, i, p))
	}
	return d
}

func RatBinomCDF(n int64, p *big.Rat) []*big.Rat {
	cum := DistRatBinom(n, p)
	for i := 1; i < len(cum); i++ {
		cum[i].Add(cum[i], cum[i-1])
	}
	return cum
}

// Binom returns the probability that we had a number of successes out of a number of tests, where the probability of individual success is p
func Binom(tests, successes int, p float64) float64 {
	c := Combinations(tests, successes)
	p = float64(c) * math.Pow(p, float64(successes)) * math.Pow((1-p), float64(tests-successes))
	return p
}

// RightBinom returns the probability that we had minsuccesses or more success
func RightBinom(tests, minsuccesses int, p float64) float64 {
	rp := 0.0
	for successes := minsuccesses; successes <= tests; successes++ {
		rp += Binom(tests, successes, p)
	}
	return rp
}

// Gives the confidence interval for the number of successes we expect to see.
// eg., 95% confidence interval, p=0.95
func ConfidenceInterval(tests int, p, interval float64) (int, int) {
	// two sided confidence, so we want the number of successes
	// eg if 95% of the values are > 2 and < 5, return (2, 5)
	// 90% of the values are > 5%, < 95%
	log.Printf("%d tests p: %f, %f%% interval", tests, p, 100.0*interval)
	lbound := (1.0 - interval) / 2
	ubound := 1.0 - lbound
	cumProb := 0.0
	lower := 0
	log.Printf("bounds: (%f, %f)", lbound, ubound)
	for {
		b := Binom(tests, lower, p)
		cumProb += b
		log.Printf("b: %v, tests: %v, lower: %v, p: %v", b, tests, lower, p)
		log.Printf("lower bound: %v, cumProb: %v, b: %v", lower, cumProb, b)
		if cumProb > lbound {
			break
		}
		lower++
	}
	upper := lower
	for {
		upper++
		b := Binom(tests, upper, p)
		cumProb += b
		log.Printf("b: %v, tests: %v, upper: %v, p: %v", b, tests, upper, p)
		log.Printf("upper bound: %v, cumProb: %v, b: %v", upper, cumProb, b)
		if cumProb > ubound {
			break
		}
	}
	return lower, upper
}

// erf, valid for x >= 0
func ERF(x float64) float64 {
	// approximation, A&S 7.1.25. Accurate to 1.5x10^-7
	// erf x = 1 - (a1.t + a2.t^2 + a3.t^3 + a4.t^4 +a5.t^5). exp(-x^2)
	// = 1 - t(a1 + t(a2 + t(a3 + t(a4 + a5.t)))) * e^
	// t = 1/(1 + px)
	p := 0.3275911

	a1 := 0.254829592
	a2 := -0.284496736
	a3 := 1.421413741
	a4 := -1.453152027
	a5 := 1.061405429

	t := 1.0 / (1.0 + p*x)

	e := math.Exp(-x * x)
	polynomial := t * (a1 + t*(a2+t*(a3+t*(a4+a5*t))))
	return 1.0 - polynomial*e

}
