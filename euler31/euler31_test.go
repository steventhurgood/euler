package main

import (
	"testing"
)

func TestAddCoin(t *testing.T) {
	cash := Cash{
		Total: 10,
		Coins: map[int]int{
			10: 1,
		},
	}
	newCash := AddCoin(cash, 20, 1)
	if newCash.Total != 30 {
		t.Error("Expected new total to be 30. Got: ", newCash)
	}
	if len(newCash.Coins) != 2 {
		t.Error("Expected new cash to have 2 coins. Got: ", newCash)
	}
	if len(cash.Coins) != 1 {
		t.Error("cash changed: ", cash)
	}
}
