package main

import (
	"bytes"
	"flag"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type IntList []int

func (i *IntList) String() string {
	buf := bytes.NewBuffer(make([]byte, 0, 1024))
	start := false
	for _, i := range *i {
		if !start {
			buf.WriteString(", ")
		}
		buf.WriteString(fmt.Sprint(i))
	}
	return buf.String()
}

func (i *IntList) Set(value string) error {
	values := IntList{}
	for _, arg := range strings.Split(value, ",") {
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			return err
		}
		values = append(values, int(n))
	}
	*i = values
	return nil
}

var (
	coinFlag IntList = []int{1, 2, 5, 10, 20, 50, 100, 200}
)

func init() {
	flag.Var(&coinFlag, "coins", "comma-separated list of coin values")
}

type Cash struct {
	Total int
	Coins map[int]int
}

func (c Cash) String() string {
	buf := bytes.NewBuffer(make([]byte, 1024))
	buf.WriteString(fmt.Sprintf("Total: %d ", c.Total))
	coins := []int{}
	for coin := range c.Coins {
		coins = append(coins, coin)
	}
	sort.Ints(coins)
	if len(coins) > 0 {
		buf.WriteString("(")
	}
	start := true
	for _, coin := range coins {
		if c.Coins[coin] == 0 {
			continue
		}
		if !start {
			buf.WriteString(", ")
		}
		start = false
		buf.WriteString(fmt.Sprintf("%d x %dp", c.Coins[coin], coin))
	}
	if len(coins) > 0 {
		buf.WriteString(")")
	}
	return buf.String()

}

func AddCoin(cash Cash, coin, count int) Cash {
	newCash := Cash{
		Total: cash.Total + count*coin,
		Coins: make(map[int]int),
	}
	for coin, count := range cash.Coins {
		newCash.Coins[coin] = count
	}
	newCash.Coins[coin] += count
	return newCash
}

func WaysToMake(total int, coins []int) (int, []Cash) {
	validTotals := []Cash{}
	working := []Cash{
		Cash{
			Total: 0,
			Coins: make(map[int]int),
		},
	}
	workingCoins := make([]int, len(coins))
	copy(workingCoins, coins)
	count := 0
	for len(workingCoins) > 0 {
		newWorking := make([]Cash, 0, len(working))
		coin := workingCoins[0]

		for _, work := range working {
			for i := 0; work.Total+i*coin <= total; i++ {
				newCash := AddCoin(work, coin, i)
				if newCash.Total == total {
					validTotals = append(validTotals, newCash)
					count++
				} else {
					newWorking = append(newWorking, newCash)
				}
			}
		}
		working = newWorking
		workingCoins = workingCoins[1:]
	}
	return count, validTotals
}

func main() {
	total := flag.Int("total", 200, "amount to make")
	flag.Parse()
	count, ways := WaysToMake(*total, coinFlag)
	for _, way := range ways {
		fmt.Println(way)
	}
	fmt.Println(count, "ways:")
}
