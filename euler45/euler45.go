package main

import (
	"flag"
	"fmt"
	"math"

	"bitbucket.org/steventhurgood/euler/intargs"
)

/*
Triangle number: Tn = n(n+1)/2 = 1/2n^2 +1/2n ; 1/2n^2 +1/2n -Tn = 0

Pentagonal number: Pn = n(3n-1)/2 = 3/2n^2 -1/2n = Pn; 3/2n^2 -1/2n -Pn = 0

Hexagonal number:  Hn = n(2n-1) = 2n^2 -n; 2n^2 - n -Hn = 0
*/

// Return the roots of ax^2 + bx + c = 0

func TriangleNumber(n int) int {
	return (n * (n + 1)) / 2
}

func HexagonalNumber(n int) int {
	return (n * (2*n + 1))
}

func IsTriangleNumber(triangle int) (int, bool) {
	_, n := QuadraticRoots(0.5, 0.5, float64(-triangle))
	if IsInteger(n) {
		return int(n), true
	}
	return 0, false
}

func IsPentagonalNumber(pentagon int) (int, bool) {
	_, n := QuadraticRoots(1.5, -0.5, float64(-pentagon))
	if IsInteger(n) {
		return int(n), true
	}
	return 0, false
}

func IsHexagonalNumber(hexagon int) (int, bool) {
	_, n := QuadraticRoots(2.0, -1, float64(-hexagon))
	if IsInteger(n) {
		return int(n), true
	}
	return 0, false
}

func QuadraticRoots(a, b, c float64) (float64, float64) {
	rt := math.Sqrt(b*b - 4*a*c)
	i := (-b - rt) / (2 * a)
	j := (-b + rt) / (2 * a)
	return i, j
}

func IsInteger(n float64) bool {
	return n == math.Floor(n)
}

func main() {
	find := flag.Int("find", -1, "Search for a number that is triangle, hexagonal, and pentagonal")
	start := flag.Int("start", 1, "Where to start searching")
	flag.Parse()
	if *find > 0 {
		for i := *start; *find > 0; i++ {
			t := TriangleNumber(i)
			// h := HexagonalNumber(i)
			pn, isP := IsPentagonalNumber(t)
			hn, isH := IsHexagonalNumber(t)
			// tn, isT := IsTriangleNumber(t)
			if isP && isH {
				fmt.Println(t, " = T(", i, ") = P(", pn, ") = H(", hn, ")")
				*find--
			}

		}
	} else {
		for _, n := range intargs.IntArgs(flag.Args()) {
			if i, is := IsTriangleNumber(n); is {
				fmt.Println(n, "is triangle number ", i)
			}
			if i, is := IsPentagonalNumber(n); is {
				fmt.Println(n, "is pentagon number ", i)
			}
			if i, is := IsHexagonalNumber(n); is {
				fmt.Println(n, "is hexagon number ", i)
			}
		}
	}
}
