package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"math/big"
)

type Convergent struct {
	n    int
	d    *big.Int
	a    []*big.Int
	p    []*big.Int
	q    []*big.Int
	bigP []*big.Int
	bigQ []*big.Int
}

func (c Convergent) String() string {
	var b bytes.Buffer
	end := len(c.p) - 1
	fmt.Fprintf(&b, "[")
	for i := range c.a {
		fmt.Fprintf(&b, "%v", c.a[i])
		if i == 0 {
			fmt.Fprintf(&b, ";")
		} else if i < end {
			fmt.Fprintf(&b, ",")
		}
	}
	fmt.Fprintf(&b, "] ")
	for i := range c.p {
		fmt.Fprintf(&b, "%v/%v", c.p[i], c.q[i])
		if i < end {
			fmt.Fprintf(&b, " ")
		}

	}
	return b.String()
}

func IsSqrt(n *big.Int) (bool, *big.Int) {
	r := IntSqrt(n)
	is := new(big.Int).Mul(r, r)
	return is.Cmp(n) == 0, r
}

func IntSqrt(n *big.Int) *big.Int {
	x := big.NewInt(0)
	var prevX, nextX big.Int
	x.SetBit(x, n.BitLen()/2+1, 1)

	for {
		// next x = (x + n/x)/2
		nextX.Rsh(nextX.Add(x, nextX.Div(n, x)), 1)
		if nextX.Cmp(x) == 0 || nextX.Cmp(&prevX) == 0 {
			break
		}
		prevX.Set(x)
		x.Set(&nextX)
	}
	return x
}

// http://mathworld.wolfram.com/PellEquation.html
func NewConvergent(d *big.Int) Convergent {
	a0 := IntSqrt(d)
	// log.Printf("a0 = sqrt(%v) = %v", d, a0)

	p0 := a0
	// log.Printf("p0 = a0 = %v", a0)

	q0 := big.NewInt(1)
	// log.Printf("q0 = 1")

	bigP0 := big.NewInt(0)
	// log.Printf("P0 = 0")

	bigP1 := a0
	// log.Printf("P1 = a0 = %v", bigP1)

	bigQ0 := big.NewInt(1)
	// log.Printf("Q0 = 1")

	bigQ1 := big.NewInt(0)
	bigQ1.Sub(d, bigQ1.Mul(a0, a0)) // Q1 = D - a0^2
	// log.Printf("Q1 = %v - %v² = %v", d, a0, bigQ1)

	a1 := big.NewInt(0)
	a1.Quo(a1.Add(a0, bigP1), bigQ1)
	// log.Printf("a1 = (%v + %v)/%v = %v", a0, bigP1, bigQ1, a1)

	q1 := a1
	// log.Printf("q1 = a1 = %v", q1)

	p1 := big.NewInt(0)
	p1.Add(p1.Mul(a0, a1), big.NewInt(1))
	// log.Printf("p1 = %v . %v + 1 = %v", a0, a1, p1)

	c := Convergent{
		n:    2,
		d:    d,
		a:    []*big.Int{a0, a1},
		p:    []*big.Int{p0, p1},
		q:    []*big.Int{q0, q1},
		bigP: []*big.Int{bigP0, bigP1},
		bigQ: []*big.Int{bigQ0, bigQ1},
	}
	return c
}

func (c Convergent) Convergent() (*big.Int, *big.Int) {
	return c.p[c.n-1], c.q[c.n-1]
}

func IsPellSolution(d *big.Int, x *big.Int, y *big.Int) bool {
	// r = x^q -d.y^2 == 1"
	want := big.NewInt(1)
	r := big.NewInt(0)
	x2 := new(big.Int).Mul(x, x)
	y2 := new(big.Int).Mul(y, y)
	r.Sub(x2, r.Mul(d, y2))
	return r.Cmp(want) == 0
}

func (c *Convergent) Next() (*big.Int, *big.Int) {

	bigPn := big.NewInt(1)
	bigPn.Sub(bigPn.Mul(c.a[c.n-1], c.bigQ[c.n-1]), c.bigP[c.n-1])

	bigQn := big.NewInt(1)
	bigQn.Quo(bigQn.Sub(c.d, bigQn.Mul(bigPn, bigPn)), c.bigQ[c.n-1])

	an := big.NewInt(1)
	an.Quo(an.Add(c.a[0], bigPn), bigQn)

	pn := big.NewInt(1)
	pn.Add(pn.Mul(an, c.p[c.n-1]), c.p[c.n-2])

	qn := big.NewInt(1)
	qn.Add(qn.Mul(an, c.q[c.n-1]), c.q[c.n-2])
	c.a = append(c.a, an)
	c.q = append(c.q, qn)
	c.p = append(c.p, pn)
	c.bigP = append(c.bigP, bigPn)
	c.bigQ = append(c.bigQ, bigQn)
	c.n++
	return pn, qn
}

func Solve(n *big.Int) (*big.Int, *big.Int) {
	c := NewConvergent(n)
	x, y := c.Convergent()
	for !IsPellSolution(n, x, y) {
		x, y = c.Next()
	}
	return x, y
}

func main() {
	flag.Parse()
	var max_x, max_y, max_d *big.Int
	max_x = big.NewInt(0)
	for _, arg := range flag.Args() {
		d := big.NewInt(0)
		_, ok := d.SetString(arg, 10)
		if !ok {
			log.Fatalf("Error parsing int: %v", arg)
		}
		if is, _ := IsSqrt(d); is {
			log.Printf("%v is square", d)
			continue
		}
		x, y := Solve(d)
		// fmt.Println(x, y)
		fmt.Printf("%v² - %v . %v² = 1\n", x, d, y)
		if x.Cmp(max_x) > 0 {
			max_x = x
			max_y = y
			max_d = d
		}
	}
	fmt.Printf("max: %v² - %v . %v² = 1\n", max_x, max_d, max_y)
}
