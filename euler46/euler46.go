package main

import (
	"flag"
	"fmt"
	"log"
	"math"

	"bitbucket.org/steventhurgood/euler/prime"
)

func IsSquare(n float64) (bool, int) {
	s := math.Sqrt(n)
	return s == math.Floor(s), int(s)
}

func main() {
	sieveSize := flag.Int("sieve_size", 100000, "size of prime sieve")
	flag.Parse()
	s := prime.NewSieve(*sieveSize)
	primes := s.Primes()
	i := 3
	divisable := true
	goldbachs := [][]int{}
	noSolution := 0
	for divisable {
		if is, err := s.IsPrime(i); !is {
			if err != nil {
				log.Fatal(err)
			}
			divisable = false
			for _, p := range primes {
				if p > i {
					noSolution = i
					break
				}
				r := i - p
				if is, root := IsSquare(float64(r) / 2); is {
					divisable = true
					goldbachs = append(goldbachs, []int{i, p, root})
					break
				}
			}
		}
		i += 2
	}
	for _, g := range goldbachs {
		fmt.Printf("%d = %d + 2 * %d^2\n", g[0], g[1], g[2])
	}
	fmt.Printf("%d has no solution\n", noSolution)
}
