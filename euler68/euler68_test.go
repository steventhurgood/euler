package main

import "testing"

func TestExpandEmpty(t *testing.T) {
	n := NewNGon(3)
	pending, completed := Expand(n)
	for _, got := range pending {
		t.Log(got)
	}
	if len(completed) != 0 {
		t.Errorf("No NGons should have completed. Got:\n%v", completed)
	}
}

func TestExpandPartial(t *testing.T) {
	n := &NGon{
		data: [][]int{
			[]int{4, 2, 3},
			[]int{0, 3, 0},
			[]int{0, 0, 2},
		},
		n:         3,
		sum:       9,
		completed: 1,
		used:      []bool{true, false, true, true, true, false, false}, // make([]bool, n+1)
	}
	pending, completed := Expand(n)
	for _, got := range pending {
		t.Log(got)
	}
	if len(completed) != 0 {
		t.Errorf("No NGons should have completed. Got:\n%v", completed)
	}
}

func TestExpandComplete(t *testing.T) {
	n := &NGon{
		data: [][]int{
			[]int{4, 2, 3},
			[]int{5, 3, 1},
			[]int{0, 1, 2},
		},
		n:         3,
		sum:       9,
		completed: 2,
		used:      []bool{true, true, true, true, true, true, false}, // make([]bool, n+1)
	}
	pending, completed := Expand(n)
	if len(completed) == 0 {
		t.Errorf("Should have a completed solution")
	}
	for _, got := range pending {
		t.Logf("pending: %v", got)
	}
	for _, got := range completed {
		t.Logf("completed: %v", got)
	}
}
