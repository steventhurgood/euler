package main

import (
	"bytes"
	"flag"
	"fmt"
	"sort"
)

var (
	n = flag.Int("n", 3, "n-gon size")
)

type NGon struct {
	data      [][]int
	n         int // 3 for a trigon, 5 for a pentagon
	completed int
	sum       int
	used      []bool
}

func AsNumber(n *NGon) string {
	var b bytes.Buffer
	for i := range n.data {
		for j := range n.data[i] {
			fmt.Fprintf(&b, "%v", n.data[i][j])
		}
	}
	return b.String()
}

func (n *NGon) Digits() int {
	digits := 0
	for i := range n.data {
		for j := range n.data[i] {
			if n.data[i][j] >= 10 {
				digits += 2
			} else {
				digits += 1
			}
		}
	}
	return digits
}

type ByDigits []*NGon

func (n ByDigits) Len() int {
	return len(n)
}

func (n ByDigits) Less(a, b int) bool {
	return n[a].Digits() < n[b].Digits()
}

func (n ByDigits) Swap(a, b int) {
	n[a], n[b] = n[b], n[a]
}

type BySum []*NGon

func (n BySum) Len() int {
	return len(n)
}

func (n BySum) Less(a, b int) bool {
	return n[a].sum < n[b].sum
}

func (n BySum) Swap(a, b int) {
	n[a], n[b] = n[b], n[a]
}

func (n *NGon) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "%-8v", n.sum)
	for i := range n.data {
		fmt.Fprintf(&b, "%v,%v,%v", n.data[i][0], n.data[i][1], n.data[i][2])
		if i < len(n.data)-1 {
			fmt.Fprintf(&b, "; ")
		}
	}
	fmt.Fprintf(&b, " (%v)", n.Digits())
	return b.String()
}

func (n *NGon) Copy() *NGon {
	data := make([][]int, n.n)
	for i := 0; i < n.n; i++ {
		data[i] = make([]int, 3)
		copy(data[i], n.data[i])
	}
	used := make([]bool, len(n.used))
	copy(used, n.used)
	return &NGon{
		data:      data,
		n:         n.n,
		completed: n.completed,
		sum:       n.sum,
		used:      used,
	}
}

func NewNGon(n int) *NGon {
	data := make([][]int, n)
	for i := 0; i < n; i++ {
		data[i] = make([]int, 3)
	}
	return &NGon{
		data: data,
		n:    n,
		used: make([]bool, 2*n+1),
	}
}

func Expand(n *NGon) ([]*NGon, []*NGon) {
	pending := make([]*NGon, 0)
	completed := make([]*NGon, 0)
	switch n.completed {
	case 0:
		{
			imax := 1 + 2*n.n - n.n
			for i := 1; i <= imax; i++ {
				for j := 1; j <= 2*n.n; j++ {
					if j == i {
						continue
					}
					for k := 1; k <= 2*n.n; k++ {
						if k == i || k == j {
							continue
						}
						newNGon := n.Copy()
						newNGon.data[0][0] = i
						newNGon.data[0][1] = j
						newNGon.data[0][2] = k
						newNGon.data[1][1] = k
						newNGon.data[n.n-1][2] = j
						newNGon.completed = 1
						newNGon.sum = i + j + k
						newNGon.used[i], newNGon.used[j], newNGon.used[k] = true, true, true
						pending = append(pending, newNGon)
					}
				}
			}
		}
	case n.n - 1:
		{
			// this is the last item. [1] and [2] should already be completed.
			target := n.sum - n.data[n.n-1][1] - n.data[n.n-1][2]
			if target >= 1 && target <= 2*n.n && !n.used[target] && target > n.data[0][0] {
				newNGon := n.Copy()
				newNGon.data[n.n-1][0] = target
				newNGon.used[target] = true
				newNGon.completed++
				completed = append(completed, newNGon)
			} else {
			}
		}
	default:
		{
			// populate n.data[n.completed]
			// fmt.Println("Expanding: ", n)
			target := n.sum - n.data[n.completed][1]
			for i := n.data[0][0] + 1; i <= 2*n.n; i++ {
				j := target - i
				// fmt.Println("  considering: ", i, n.data[n.completed][1], j)
				if i < 1 || i > 2*n.n || j < 1 || j > 2*n.n || i == j {
					continue
				}
				if n.used[i] || n.used[j] {
					continue
				}
				newNGon := n.Copy()
				newNGon.data[n.completed][0] = i
				newNGon.data[n.completed][2] = j
				newNGon.data[n.completed+1][1] = j
				newNGon.used[i] = true
				newNGon.used[j] = true
				newNGon.completed++
				pending = append(pending, newNGon)
			}
		}
	}
	return pending, completed
}

func main() {
	flag.Parse()
	pending := []*NGon{
		NewNGon(*n),
	}
	completed := make([]*NGon, 0)
	for len(pending) > 0 {
		current := pending[0]
		pending = pending[1:len(pending)]
		nn, cc := Expand(current)
		completed = append(completed, cc...)
		pending = append(pending, nn...)
	}
	fmt.Println("***** By Number")
	for i := range completed {
		fmt.Println(completed[i])
		fmt.Println(AsNumber(completed[i]))
	}
	fmt.Println("***** By Sum")
	sort.Sort(BySum(completed))
	for i := range completed {
		fmt.Println(completed[i])
	}
	fmt.Println("***** By #Digits")
	sort.Sort(ByDigits(completed))
	for i := range completed {
		fmt.Println(completed[i])
	}
}
