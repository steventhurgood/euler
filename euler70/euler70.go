package main

import (
	"flag"
	"fmt"
	"log"

	"bitbucket.org/steventhurgood/euler/prime"
)

var (
	n          = flag.Int("n", 100, "number up to which to search for permutations")
	sieve_size = flag.Int("sieve_size", 10000000, "size of prime sieve")
)

type PhiRat struct {
	n, num, den int
}
type PhiGen []PhiRat

func (p PhiGen) Get(n int) int {
	return n * p[n].num / p[n].den
}

func NewPhiGen(n int) PhiGen {
	phi := make(PhiGen, n)
	candidates := make(PhiGen, 1, n)
	log.Printf("Generating new sieve(%v)", n)
	s := prime.NewSieve(n)
	log.Printf("Done")
	phi[0] = PhiRat{0, 0, 1}
	phi[1] = PhiRat{0, 1, 1}

	candidates[0] = PhiRat{1, 1, 1}
	for _, prime := range s.Primes() {
		log.Printf("prime: %v", prime)
		newCandidates := make([]PhiRat, 0, len(candidates))
		for i := range candidates {
			newNum := candidates[i].num * (prime - 1)
			newDen := candidates[i].den * prime
			ni := candidates[i].n
			// fmt.Printf("ni: %v (%v/%v)\n", ni, newNum, newDen)
			// phi[i] = p1^n1 . p2^n2 . ...
			for j := ni * prime; j < n; j *= prime {
				// j = p1^n1 . ... . prime^1 ; p1^n1 . ... . prime^nprime
				newCandidates = append(newCandidates, PhiRat{j, newNum, newDen})
				phi[j].num = newNum
				phi[j].den = newDen
			}
		}
		candidates = append(candidates, newCandidates...)
		// fmt.Println(candidates)
	}
	return phi
}

func FactorPhi(n int, s *prime.Sieve) int {
	// phi(n) - n . (1-1/p1) . (1-1/p2) for prime factors of n
	// phi(n) -> .. (p1 -  1)/p1 . (p2-1)/p2 ..
	factors := s.PrimeDivisors(n)
	// fmt.Printf("PrimeDivisors(%v) -> %v\n", n, factors)
	// log.Print(factors)
	num := n
	oldn := n
	den := 1
	oldden := 1
	for _, p := range factors {
		num = num * (p - 1)
		den *= p
		if num < oldn {
			log.Fatalf("Numerator overflow: %v -> %v", oldn, num)
		}
		if den < oldden {
			log.Fatalf("Denominator overflow: %v -> %v", oldden, den)
		}
	}
	return num / den
}

func IsPermutation(a, b int) bool {
	count := make([]int, 10)
	for a > 0 {
		digit := a % 10
		count[digit]++
		a /= 10
	}
	for b > 0 {
		digit := b % 10
		count[digit]--
		if count[digit] < 0 {
			return false
		}
		b = b / 10
	}
	for _, c := range count {
		if c > 0 {
			return false
		}
	}
	return true
}

func main() {
	flag.Parse()
	// s := prime.NewSieve(*sieve_size)
	count := 0
	min_nphi := -1.0
	min_phi := 0
	min_n := 0
	phiGen := NewPhiGen(*n)
	sieve := prime.NewSieve(*sieve_size)
	for i := 2; i < *n; i++ {
		// phi := FactorPhi(i, s)
		phi := phiGen.Get(i)
		// log.Printf("Phi(%v) -> %v", i, phi)
		if IsPermutation(i, phi) {
			count++
			divisors, _ := sieve.Divisors(i)
			nphi := float64(i) / float64(phi)
			log.Printf("Phi(%v) -> %v (%v)", i, phi, divisors)
			if min_nphi < 0 || nphi < min_nphi {
				min_nphi = nphi
				min_phi = phi
				min_n = i
			}
		}
	}
	fmt.Println("count: ", count)
	fmt.Println("min phi: ", min_phi)
	fmt.Println("min nphi: ", min_nphi)
	fmt.Println("min_n: ", min_n)
}
