package main

import (
	"testing"

	"bitbucket.org/steventhurgood/euler/prime"
)

func TestPhiGen(t *testing.T) {
	phi := NewPhiGen(11)
	want := []int{0, 1, 1, 2, 2, 4, 2, 6, 4, 6, 4}
	/*
		factors(4) = 2
		phi(4) = 4 . (1-1/2) = (2-1)/2 = 1, 2
	*/

	t.Log(phi)

	for i := range want {
		if got := phi.Get(i); got != want[i] {
			t.Errorf("Phi(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestIsPermutation(t *testing.T) {
	trueTests := [][]int{
		[]int{12345, 12354},
		[]int{12345, 54321},
		[]int{1111, 1111},
	}
	falseTests := [][]int{
		[]int{12345, 1234},
		[]int{12345, 123456},
		[]int{122345, 12345},
		[]int{1111, 111},
		[]int{1111, 11111},
	}
	for _, test := range trueTests {
		if !IsPermutation(test[0], test[1]) {
			t.Errorf("IsPermutation(%v, %v) -> False. Want: True", test[0], test[1])
		}
	}
	for _, test := range falseTests {
		if IsPermutation(test[0], test[1]) {
			t.Errorf("IsPermutation(%v, %v) -> True. Want: False", test[0], test[1])
		}
	}
}

func TestFactorPhi(t *testing.T) {
	n := 9708131
	want := 9701832
	s := prime.NewSieve(10000000)
	got := FactorPhi(n, s)
	if got != want {
		t.Errorf("FactorPhi(%v) -> %v. Want: %v", n, got, want)
	}
}
