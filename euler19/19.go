package main

import (
	"fmt"
	"strings"
)

type WeekDay int
type Year int
type Month int
type MonthDay int

const (
	Monday    WeekDay = iota
	Tuesday   WeekDay = iota
	Wednesday WeekDay = iota
	Thursday  WeekDay = iota
	Friday    WeekDay = iota
	Saturday  WeekDay = iota
	Sunday    WeekDay = iota
)

var (
	dayNames = map[WeekDay]string{
		Monday:    "Monday",
		Tuesday:   "Tuesday",
		Wednesday: "Wednesday",
		Thursday:  "Thursday",
		Friday:    "Friday",
		Saturday:  "Saturday",
		Sunday:    "Sunday",
	}
	monthCounts = []MonthDay{
		31, // January
		28, // February
		31, // March
		30, // April
		31, // May
		30, // June
		31, // July
		31, // August
		30, // September
		31, // October
		30, // November
		31, // December
	}
)

func IsLeapYear(year Year) bool {
	return year%4 == 0 && (year%400 == 0 || year%100 != 0)
}

type Date struct {
	year  Year
	month Month
	date  MonthDay
	day   WeekDay
}

func (d Date) String() string {
	return fmt.Sprintf("%04d-%02d-%02d, %s", d.year, d.month+1, d.date, dayNames[d.day])
}

func NewDate(year Year, month Month, date MonthDay, day WeekDay) Date {
	return Date{year: year, month: month, date: date, day: day}
}

func (d Date) Increment() Date {
	d.day = (d.day + 1) % 7
	d.date++
	monthLength := monthCounts[d.month]
	if d.month == 1 {
		if IsLeapYear(d.year) {
			monthLength = 29
		}
	}
	if d.date > monthLength {
		d.date = 1
		d.month++
		if d.month > 11 {
			d.month = 0
			d.year++

		}
	}
	return d
}

func (d Date) LessThan(compare Date) bool {
	return d.year < compare.year || d.month < compare.month || d.date < compare.date
}

func main() {
	d := Date{year: 1900, month: 0, date: 1, day: Monday}
	for d.LessThan(Date{year: 1901, month: 0, date: 1}) {
		d = d.Increment()
	}
	sum := 0
	days := make([]string, 0)
	for d.LessThan(Date{year: 2001, month: 0, date: 1}) {
		if d.date == 1 && d.day == Sunday {
			days = append(days, d.String())
			sum++
		}
		d = d.Increment()
	}
	fmt.Println(strings.Join(days, "\n"))
	fmt.Println(sum)
}
