package main

import (
	"testing"
)

func Test_IsLeapYear(t *testing.T) {
	if !IsLeapYear(16) {
		t.Error("16 is a leap year")
	}
	if !IsLeapYear(2000) {
		t.Error("800 is a leap year")
	}
	if IsLeapYear(1900) {
		t.Error("1900 is not a leap year")
	}
}

func Test_Increment(t *testing.T) {
	d := Date{year: 2000, month: 0, date: 1, day: Monday}
	d = d.Increment()
	if d.day != Tuesday {
		t.Error("Expected Monday, got", d.day)
	}
	if d.date != 2 {
		t.Error("Expected 2nd, got", d.date)
	}
}

func Test_DayRollover(t *testing.T) {
	d := Date{year: 2000, month: 0, date: 1, day: Sunday}
	d = d.Increment()
	if d.day != Monday {
		t.Error("Expected Monday(0), got", d.day)
	}
}

func Test_LeapYear(t *testing.T) {
	d := Date{year: 2000, month: 1, date: 28, day: Monday}
	d = d.Increment()
	if d.month != 1 {
		t.Error("Expected February(1), got", d.month)
	}
	if d.date != 29 {
		t.Error("Expected 29th, got", d.date)
	}
}

func Test_NonLeapYear(t *testing.T) {
	d := Date{year: 2001, month: 1, date: 28, day: Monday}
	d = d.Increment()
	if d.month != 2 {
		t.Error("Expected March(2), got", d.month)
	}
	if d.date != 1 {
		t.Error("Expected 1st, got", d.date)
	}
}

func Test_NonLeapYearCentury(t *testing.T) {
	d := Date{year: 2100, month: 1, date: 28, day: Monday}
	d = d.Increment()
	if d.month != 2 {
		t.Error("Expected March(2), got", d.month)
	}
	if d.date != 1 {
		t.Error("Expected 1st, got", d.date)
	}
}

func Test_LessThan(t *testing.T) {
	d := Date{year: 2000, month: 1, date: 1, day: Monday}
	if !d.LessThan(Date{year: 2001}) {
		t.Error("less than failed for year: 2001")
	}
	if !d.LessThan(Date{year: 2000, month: 2}) {
		t.Error("less than failed for month: March")
	}
	if !d.LessThan(Date{year: 2000, month: 1, date: 2}) {
		t.Error("less than failed for date: 2nd")
	}
}

func Test_DateString(t *testing.T) {
	d := Date{year: 2000, month: 1, date: 1, day: Monday}
	dString := d.String()
	expected := "2000-02-01, Monday"
	if dString != expected {
		t.Error(dString, "!=", expected)
	}
}
