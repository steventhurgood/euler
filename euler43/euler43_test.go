package main

import (
	"testing"

	"bitbucket.org/steventhurgood/euler/pandigital"
)

func IsTestSubdivisable(t *testing.T) {
	n := []int{1, 4, 0, 6, 3, 5, 7, 2, 8, 9}
	if !IsSubDivisable(n) {
		t.Error("Expected ", n, " to be subdivisable")
	}
}

func BenchmarkParallelPermutationSubdivisors(b *testing.B) {
	digits := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	for i := 0; i < b.N; i++ {
		for p := range pandigital.PermutationChannel(digits, 2) {
			if IsSubDivisable(p) {
				pandigital.SliceToInt(p)
			}
		}
	}
}
