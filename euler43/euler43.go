package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/pandigital"
)

func SubDivisableFilter(in chan []int) chan int {
	ch := make(chan int)
	go func() {
		for d := range in {
			if IsSubDivisable(d) {
				ch <- pandigital.SliceToInt(d)
			}
		}
		close(ch)
	}()
	return ch
}

func IsSubDivisable(digits []int) bool {
	sub := [][2]int{
		[2]int{1, 2},
		[2]int{2, 3},
		[2]int{3, 5},
		[2]int{4, 7},
		[2]int{5, 11},
		[2]int{6, 13},
		[2]int{7, 17},
	}
	for _, s := range sub {
		start, end, d := s[0], s[0]+3, s[1]
		n := pandigital.SliceToInt(digits[start:end])
		if n%d != 0 {
			return false
		}
	}
	return true
}

func main() {
	parallel := flag.Bool("parallel", false, "should we run in parallel or not")
	parallelDigits := flag.Int("parallel_digits", 2, "number of digits to permute in parallel")
	flag.Parse()
	digits := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	divisable := []int{}
	sum := 0
	if *parallel {
		for p := range pandigital.PermutationChannel(digits, *parallelDigits) {
			if IsSubDivisable(p) {
				n := pandigital.SliceToInt(p)
				sum += n
				divisable = append(divisable, pandigital.SliceToInt(p))
			}
		}
	} else {
		for _, p := range pandigital.Permutations(digits) {
			if IsSubDivisable(p) {
				n := pandigital.SliceToInt(p)
				sum += n
				divisable = append(divisable, pandigital.SliceToInt(p))
			}
		}
	}
	for _, d := range divisable {
		fmt.Println(d)
	}
	fmt.Println("Sum: ", sum)
}
