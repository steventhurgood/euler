package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
)

type BigNum []byte

func (n BigNum) String() string {
	reversed := make([]byte, len(n))
	for i := 0; i < len(n); i++ {
		reversed[i] = '0' + n[len(n)-1-i]
	}
	b := bytes.NewBuffer(reversed)
	return b.String()
}

func (a BigNum) Add(b BigNum) BigNum {
	i := 0
	sum := make([]byte, 0)
	carry := byte(0)
	for i < len(a) || i < len(b) {
		aVal := byte(0)
		bVal := byte(0)
		if i < len(a) {
			aVal = a[i]
		}
		if i < len(b) {
			bVal = b[i]
		}
		r := aVal + bVal + carry
		carry = 0
		if r >= 10 {
			r = r - 10
			carry = 1
		}
		sum = append(sum, r)
		i++
	}
	if carry > 0 {
		sum = append(sum, carry)
	}
	return sum
}

func LineToByteArray(s string) BigNum {
	numBytes := make([]byte, 0, len(s))
	for i := len(s) - 1; i >= 0; i-- {
		char := s[i]
		if char < '0' || char > '9' {
			log.Fatal("error parsing int: ", i)
		}
		numBytes = append(numBytes, char-'0')
	}
	return numBytes
}

func ReadNumbers() []string {
	lines := make([]string, 0, 100)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return lines
}

func main() {
	lines := ReadNumbers()
	sum := make(BigNum, 0)
	for _, line := range lines {
		log.Print(line)
		sum = sum.Add(LineToByteArray(line))
	}
	fmt.Println(sum)
}
