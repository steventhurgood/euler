package main

import (
	"log"
	"time"
)

func main() {
	for i := 0; i < 10; i++ {
		go func() {
			time.Sleep(1000 * time.Millisecond)
			log.Print("i: ", i)
		}()
	}
	time.Sleep(20 * time.Second)
}
