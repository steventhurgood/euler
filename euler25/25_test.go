package main

import (
	"fmt"
	"testing"
)

func TestFibbonaci(t *testing.T) {
	ch := Fibbonaci()
	expected := []BigNum{NewBigNum(1), NewBigNum(1), NewBigNum(2), NewBigNum(3), NewBigNum(5), NewBigNum(8)}
	for n, val := range expected {
		actual := <-ch
		if !val.Equals(actual) {
			t.Error("Expected item ", n+1, " to be: ", val, "; got: ", actual)
		}
	}
}

func TestBigNumEquals(t *testing.T) {
	a := NewBigNum(12345)
	b := NewBigNum(12345)
	c := NewBigNum(22345)
	if !a.Equals(b) {
		t.Error("expected ", a, " == ", b)
	}
	if a.Equals(c) {
		t.Error("expected ", a, " != ", c)
	}
}

func TestBigNum(t *testing.T) {
	n := NewBigNum(123)
	expectedBytes := []byte{3, 2, 1}
	expectedStr := fmt.Sprint(expectedBytes)
	if n.NumDigits != 3 {
		t.Error("Expected 3 digits; got:", n)
	}
	actualStr := fmt.Sprint(n.Digits)
	if expectedStr != actualStr {
		t.Error("Expected: ", expectedStr, "; got: ", actualStr)
	}

}

func TestBigNumString(t *testing.T) {
	n := NewBigNum(12345)
	if n.String() != "12345" {
		t.Error("Converted 12345 to string, got:", n.String())
	}
}

func TestBigNumAdd(t *testing.T) {
	testCases := [][]BigNum{
		[]BigNum{NewBigNum(123), NewBigNum(456), NewBigNum(579)},
		[]BigNum{NewBigNum(119), NewBigNum(112), NewBigNum(231)},
		[]BigNum{NewBigNum(999), NewBigNum(999), NewBigNum(1998)},
		[]BigNum{NewBigNum(12), NewBigNum(1234), NewBigNum(1246)},
		[]BigNum{NewBigNum(1234), NewBigNum(12), NewBigNum(1246)},
	}
	for _, testCase := range testCases {
		a := testCase[0]
		b := testCase[1]
		c := testCase[2]
		val := a.Add(b)
		t.Log(a, b, c, val)
		if !val.Equals(c) {
			t.Error("Expected ", a, " + ", b, " == ", c, "; got: ", val)
		}
	}
}
