package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"strconv"
)

type BigNum struct {
	NumDigits int
	Digits    []byte
}

func (b BigNum) String() string {
	reverse := make([]byte, len(b.Digits))
	for i := 0; i < len(b.Digits); i++ {
		reverse[len(b.Digits)-1-i] = b.Digits[i] + '0'
	}
	buf := bytes.NewBuffer(reverse)
	return buf.String()
}

func (b BigNum) Equals(c BigNum) bool {
	if b.NumDigits != c.NumDigits {
		return false
	}
	if len(b.Digits) != len(c.Digits) {
		return false
	}
	for i := range b.Digits {
		if b.Digits[i] != c.Digits[i] {
			return false
		}
	}
	return true
}
func (b BigNum) Add(c BigNum) BigNum {
	//   [1, 2, 3] +
	// + [3, 4, 5]
	// = [4, 6, 8]
	//
	//   [9, 1, 1]
	// + [2, 1, 1]
	// = [1, 3, 1]
	//
	//   [9, 9, 9]
	// + [9, 9, 9]
	// = [8, 9, 9, 1]
	//
	//   [1, 2]
	// + [1, 2, 3, 4]
	// = [2, 4, 3, 4]
	//
	//   [1, 2, 3, 4]
	// + [1, 2]
	// = [2, 4, 3, 4]
	newDigits := make([]byte, 0)
	numDigits := 0
	carry := byte(0)
	for i := 0; i < len(b.Digits) || i < len(c.Digits); i++ {
		bVal := byte(0)
		cVal := byte(0)
		if i < len(b.Digits) {
			bVal = b.Digits[i]
		}
		if i < len(c.Digits) {
			cVal = c.Digits[i]
		}
		newVal := bVal + cVal + carry
		newDigit := newVal % 10
		newDigits = append(newDigits, newDigit)
		numDigits++
		carry = (newVal - newDigit) / 10
	}
	if carry > 0 {
		newDigits = append(newDigits, carry)
		numDigits++
	}
	return BigNum{
		NumDigits: numDigits,
		Digits:    newDigits,
	}
}

func NewBigNum(n int64) BigNum {
	numDigits := 0
	digits := make([]byte, 0)
	if n == 0 {
		return BigNum{
			NumDigits: 1,
			Digits:    []byte{0},
		}
	}
	for n > 0 {
		char := n % int64(10)
		digits = append(digits, byte(char))
		n = (n - char) / 10
		numDigits++
	}
	return BigNum{
		NumDigits: numDigits,
		Digits:    digits,
	}
}

func Fibbonaci() chan BigNum {
	a, b := NewBigNum(1), NewBigNum(1)
	ch := make(chan BigNum)
	go func() {
		for {
			ch <- a
			a, b = b, a.Add(b)
		}
	}()
	return ch
}

func main() {
	minDigits := flag.Int("mindigits", -1, "Find the Fibbonaci number with the minimum number of digits")
	showAll := flag.Bool("showall", false, "Show the entire")
	flag.Parse()
	if *minDigits > 0 {
		ch := Fibbonaci()
		i := 1
		for {
			val := <-ch
			if i%100 == 0 {
				fmt.Println(i, val.NumDigits, val)
			}
			if val.NumDigits >= *minDigits {
				fmt.Println(i, val.NumDigits, val)
				return
			}
			i++
		}
		fmt.Println("min digits:", minDigits)
	}
	for _, arg := range flag.Args() {
		ch := Fibbonaci()
		n, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		nums := make([]BigNum, 0)
		val := NewBigNum(1)
		for i := 0; i < int(n); i++ {
			val = <-ch
			if *showAll {
				nums = append(nums, val)
			}
		}
		if *showAll {
			for _, num := range nums {
				fmt.Println(num)
			}
		} else {
			fmt.Println(val.NumDigits, val)
		}
	}
}
