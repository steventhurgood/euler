package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
)

type House int

const (
	Heart   House = iota
	Diamond House = iota
	Spade   House = iota
	Club    House = iota
)

type Card struct {
	value int
	suite House
}

func (a Card) Equal(b Card) bool {
	return a.value == b.value && a.suite == b.suite
}

func NewCard(code string) Card {
	value, suite := code[0], code[1]
	var c Card
	switch {
	case value == 'A':
		{
			c.value = 14
		}
	case value == 'K':
		{
			c.value = 13
		}
	case value == 'Q':
		{
			c.value = 12
		}
	case value == 'J':
		{
			c.value = 11
		}
	case value == 'T':
		{
			c.value = 10
		}
	case value >= '2' && value <= '9':
		{
			c.value = int(value - '0')
		}
	default:
		{
			log.Fatalf("Invalid card: %v", code)
		}
	}
	switch suite {
	case 'H':
		{
			c.suite = Heart
		}
	case 'D':
		{
			c.suite = Diamond
		}
	case 'S':
		{
			c.suite = Spade
		}
	case 'C':
		{
			c.suite = Club
		}
	default:
		{
			log.Fatalf("Invalid card: %v", code)
		}
	}
	return c
}

// Hand is sorted
type Hand []Card

func (a Hand) Equal(b Hand) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i].value != b[i].value || a[i].suite != b[i].suite {
			return false
		}
	}
	return true
}

type ByValue []Card

func (h ByValue) Len() int {
	return len(h)
}

func (h ByValue) Less(a, b int) bool {
	return h[a].value < h[b].value
}

func (h ByValue) Swap(a, b int) {
	h[a], h[b] = h[b], h[a]
}

func NewHand(cards []Card) Hand {
	h := make(Hand, len(cards))
	copy(h, cards)
	sort.Sort(ByValue(h))
	return h
}

type HandName int

type HandType struct {
	name HandName
	high Card
}

func GetHandType2(hand Hand) HandType {
	valueCount := make([]int, 15)
	countCount := make([]int, 6)
	handHigh := make([]Card, 10)
	consecutiveValue, sameSuite := 1, 1

	for i, card := range hand {
		valueCount[card.value]++
		if card.value > handHigh[HIGH].value {
			handHigh[HIGH] = card
		}
		if valueCount[card.value] == 2 {
			if card.value > handHigh[ONE_PAIR].value {
				handHigh[ONE_PAIR] = card
			}
		}
		if valueCount[card.value] == 3 {
			if card.value > handHigh[ONE_PAIR].value {
				handHigh[THREE_KIND] = card
			}
		}
		if valueCount[card.value] == 4 {
			if card.value > handHigh[FOUR_KIND].value {
				handHigh[FOUR_KIND] = card
			}
		}
		if i > 0 {
			if hand[i].value == hand[i-1].value+1 {
				consecutiveValue++
				handHigh[STRAIGHT] = card
			}
			if hand[i].suite == hand[i-1].suite {
				sameSuite++
				handHigh[FLUSH] = card
			}
		}
	}
	for _, count := range valueCount {
		countCount[count]++
	}
	switch {
	case consecutiveValue == 5 && sameSuite == 5:
		{
			return HandType{STRAIGHT_FLUSH, handHigh[FLUSH]}
		}
	case countCount[4] == 1:
		{
			return HandType{FOUR_KIND, handHigh[FOUR_KIND]}
		}
	case countCount[2] == 1 && countCount[3] == 1:
		{
			return HandType{FULL_HOUSE, handHigh[THREE_KIND]}
		}
	case sameSuite == 5:
		{
			return HandType{FLUSH, handHigh[FLUSH]}
		}
	case consecutiveValue == 5:
		{
			return HandType{STRAIGHT, handHigh[STRAIGHT]}
		}
	case countCount[3] == 1:
		{
			return HandType{THREE_KIND, handHigh[THREE_KIND]}
		}
	case countCount[2] == 2:
		{
			return HandType{TWO_PAIR, handHigh[ONE_PAIR]}
		}
	case countCount[2] == 1:
		{
			return HandType{ONE_PAIR, handHigh[ONE_PAIR]}
		}
	}
	return HandType{HIGH, handHigh[HIGH]}
}

func GetHandType(h Hand) HandType {
	var t HandType
	if is, high := IsStraightFlush(h); is {
		t.name = STRAIGHT_FLUSH
		t.high = high
		return t
	}
	if is, high := IsFourKind(h); is {
		t.name = FOUR_KIND
		t.high = high
		return t
	}
	if is, high := IsFullHouse(h); is {
		t.name = FULL_HOUSE
		t.high = high
		return t
	}
	if is, high := IsFlush(h); is {
		t.name = FLUSH
		t.high = high
		return t
	}
	if is, high := IsStraight(h); is {
		t.name = STRAIGHT
		t.high = high
		return t
	}
	if is, high := IsThreeKind(h); is {
		t.name = THREE_KIND
		t.high = high
		return t
	}
	if is, high := IsTwoPair(h); is {
		t.name = TWO_PAIR
		t.high = high
		return t
	}
	if is, high := IsPair(h); is {
		t.name = ONE_PAIR
		t.high = high
		return t
	}
	t.name = HIGH
	t.high = h[len(h)-1]
	return t
}

const (
	HIGH           HandName = iota
	ONE_PAIR       HandName = iota
	TWO_PAIR       HandName = iota
	THREE_KIND     HandName = iota
	STRAIGHT       HandName = iota
	FLUSH          HandName = iota
	FULL_HOUSE     HandName = iota
	FOUR_KIND      HandName = iota
	STRAIGHT_FLUSH HandName = iota
	ROYAL_FLUSH    HandName = iota
)

func IsPair(hand Hand) (bool, Card) {
	valueCount := make([]int, 15)
	for _, card := range hand {
		valueCount[card.value]++
		if valueCount[card.value] == 2 {
			return true, card
		}
	}
	return false, Card{}
}

func IsTwoPair(hand Hand) (bool, Card) {
	valueCount := make([]int, 15)
	countCount := make([]int, 5)
	for _, card := range hand {
		valueCount[card.value]++
		countCount[valueCount[card.value]]++
		if countCount[2] == 2 {
			return true, card
		}
	}
	return false, Card{}
}

func IsThreeKind(hand Hand) (bool, Card) {
	valueCount := make([]int, 15)
	for _, card := range hand {
		valueCount[card.value]++
		if valueCount[card.value] == 3 {
			return true, card
		}
	}
	return false, Card{}
}

func IsStraight(hand Hand) (bool, Card) {
	consecutive := 0
	for i := 1; i < len(hand); i++ {
		if hand[i].value == hand[i-1].value+1 {
			consecutive++
		}
	}
	if consecutive == 4 {
		return true, hand[len(hand)-1]
	}
	return false, Card{}
}

func IsFlush(hand Hand) (bool, Card) {
	for i := 1; i < len(hand); i++ {
		if hand[i].suite != hand[i-1].suite {
			return false, Card{}
		}
	}
	return true, hand[len(hand)-1]
}

func IsFullHouse(hand Hand) (bool, Card) {
	valueCount := make([]int, 15)
	countCount := make([]int, 6)
	var high Card
	for _, card := range hand {
		valueCount[card.value]++
		if valueCount[card.value] == 3 {
			high = card
		}
	}
	for _, count := range valueCount {
		countCount[count]++
	}
	if countCount[3] > 0 && countCount[2] > 0 {
		return true, high
	}
	return false, Card{}
}

func IsFourKind(hand Hand) (bool, Card) {
	valueCount := make([]int, 15)
	for _, card := range hand {
		valueCount[card.value]++
		if valueCount[card.value] == 4 {
			return true, card
		}
	}
	return false, Card{}
}

func IsStraightFlush(hand Hand) (bool, Card) {
	for i := 1; i < len(hand); i++ {
		if hand[i].value != hand[i-1].value+1 || hand[i].suite != hand[i-1].suite {
			return false, Card{}
		}
	}
	return true, hand[len(hand)-1]
}

func ReadCards(description string) []Card {
	cards := make([]Card, 0)
	for _, card := range strings.Split(description, " ") {
		cards = append(cards, NewCard(card))
	}
	return cards
}

func Winner(a, b Hand) int {
	if len(a) != len(b) {
		log.Fatalf("Bad Hands: %v - %v", a, b)
	}
	aHandType := GetHandType2(a)
	bHandType := GetHandType2(b)
	if aHandType.name > bHandType.name {
		return 1
	}
	if aHandType.name < bHandType.name {
		return 2
	}
	if aHandType.high.value > bHandType.high.value {
		return 1
	}
	if aHandType.high.value < bHandType.high.value {
		return 2
	}
	for i := len(a) - 1; i >= 0; i-- {
		if a[i].value > b[i].value {
			return 1
		}
		if b[i].value > a[i].value {
			return 2
		}
	}
	return 0
}

func ReadHands(url string) [][]Hand {
	resp, err := http.Get(url)
	hands := make([][]Hand, 0)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	s := bufio.NewScanner(resp.Body)
	for s.Scan() {
		line := s.Text()
		a := line[:14]
		b := line[15:29]
		hands = append(hands, []Hand{
			NewHand(ReadCards(a)),
			NewHand(ReadCards(b)),
		})
	}
	return hands
}

func main() {
	url := flag.String("url", "https://projecteuler.net/project/resources/p054_poker.txt", "url to retrieve hands from")
	flag.Parse()
	hands := ReadHands(*url)
	winners := make([]int, 2)
	for _, handPair := range hands {
		winner := Winner(handPair[0], handPair[1])
		winners[winner-1]++
		fmt.Println(winner, handPair[0], handPair[1])
	}
	fmt.Println(winners)
}
