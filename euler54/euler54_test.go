package main

import "testing"

var (
	// 5H 5C 6S 7S KD,Two Pair
	twoHand = NewHand([]Card{
		Card{5, Heart},
		Card{5, Club},
		Card{6, Spade},
		Card{7, Spade},
		Card{13, Diamond},
	})
	// 2C 5C 7D 8S QH, High
	highHand = NewHand([]Card{
		Card{2, Club},
		Card{5, Club},
		Card{7, Diamond},
		Card{8, Spade},
		Card{12, Heart},
	})
	// 2C 2H 5C 8D 8S, Two Pair, 8 hight
	twoPairHand = NewHand([]Card{
		Card{2, Club},
		Card{2, Heart},
		Card{5, Club},
		Card{8, Diamond},
		Card{8, Spade},
	})
	threeKindHand = NewHand([]Card{
		Card{2, Club},
		Card{3, Heart},
		Card{11, Club},
		Card{11, Diamond},
		Card{11, Spade},
	})
	straightHand = NewHand([]Card{
		Card{3, Heart},
		Card{4, Diamond},
		Card{5, Spade},
		Card{6, Club},
		Card{7, Heart},
	})
	notStraightHand = NewHand([]Card{
		Card{3, Heart},
		Card{4, Diamond},
		Card{5, Spade},
		Card{6, Club},
		Card{8, Heart},
	})
	flushHand = NewHand([]Card{
		Card{3, Heart},
		Card{5, Heart},
		Card{6, Heart},
		Card{8, Heart},
		Card{11, Heart},
	})
	notFlushHand = NewHand([]Card{
		Card{3, Heart},
		Card{5, Heart},
		Card{6, Heart},
		Card{8, Heart},
		Card{11, Diamond},
	})
	fullHouseHand = NewHand([]Card{
		Card{3, Heart},
		Card{3, Spade},
		Card{7, Heart},
		Card{7, Spade},
		Card{7, Diamond},
	})
	fullHouseHand2 = NewHand([]Card{
		Card{3, Heart},
		Card{3, Spade},
		Card{3, Club},
		Card{7, Spade},
		Card{7, Diamond},
	})
	fourKindHand = NewHand([]Card{
		Card{2, Spade},
		Card{3, Heart},
		Card{3, Diamond},
		Card{3, Spade},
		Card{3, Club},
	})
	straightFlushHand = NewHand([]Card{
		Card{10, Heart},
		Card{11, Heart},
		Card{12, Heart},
		Card{13, Heart},
		Card{14, Heart},
	})
)

type CardTest struct {
	code string
	want Card
}

func TestNewCard(t *testing.T) {
	tests := []CardTest{
		CardTest{"5H", Card{5, Heart}},
		CardTest{"TD", Card{10, Diamond}},
		CardTest{"AS", Card{14, Spade}},
	}
	for _, test := range tests {
		got := NewCard(test.code)
		if !got.Equal(test.want) {
			t.Errorf("%v != %v", got, test.want)
		}
	}
}

type HandTest struct {
	// cards := []Card{Card{10, Heart}, Card{3, Spade}, Card{5, Diamond}}
	cards []Card
	want  []Card
}

func TestNewHand(t *testing.T) {
	tests := []HandTest{
		HandTest{[]Card{Card{5, Heart}, Card{2, Spade}, Card{12, Diamond}}, []Card{Card{2, Spade}, Card{5, Heart}, Card{12, Diamond}}},
	}
	for _, test := range tests {
		got := NewHand(test.cards)
		if !got.Equal(test.want) {
			t.Errorf("NewHand(%v) -> %v. Want: %v", test.cards, got, test.want)
		}
	}
}

func TestIsPair(t *testing.T) {
	is, high := IsPair(twoHand)
	if !is || high.value != 5 {
		t.Errorf("Hand: %v => one pair, high 5. Got: (%v) %v", twoHand, is, high)
	}
	is, high = IsPair(highHand)
	if is {
		t.Errorf("Hand: %v => !one pair. Got: (%v) %v", highHand, is, high)
	}
}

func TestIsTwoPair(t *testing.T) {
	is, high := IsTwoPair(twoPairHand)
	if !is || high.value != 8 {
		t.Errorf("Hand: %v => two pair, high 8. Got: (%v) %v", twoPairHand, is, high)
	}
}

func TestIsThreeKind(t *testing.T) {
	is, high := IsThreeKind(threeKindHand)
	if !is || high.value != 11 {
		t.Errorf("Hand: %v => three kind pair, high 8. Got: (%v) %v", threeKindHand, is, high)
	}
}

func TestIsStraight(t *testing.T) {
	is, high := IsStraight(straightHand)
	if !is || high.value != 7 {
		t.Errorf("Hand: %v => straight, high 7. Got: (%v) %v", straightHand, is, high)
	}
	is, high = IsStraight(notStraightHand)
	if is {
		t.Errorf("Hand: %v => not straight. Got: (%v) %v", straightHand, is, high)
	}
}

func TestIsFlush(t *testing.T) {
	is, high := IsFlush(flushHand)
	if !is || high.value != 11 {
		t.Errorf("Hand: %v => flush, high 11. Got: (%v) %v", flushHand, is, high)
	}
	is, high = IsFlush(notFlushHand)
	if is {
		t.Errorf("Hand: %v => !flush. Got: (%v) %v", notFlushHand, is, high)
	}
}

func TestIsFullHouse(t *testing.T) {
	is, high := IsFullHouse(fullHouseHand)
	if !is || high.value != 7 {
		t.Errorf("Hand: %v => full house, high 7. Got: (%v) %v", fullHouseHand, is, high)
	}
	is, high = IsFullHouse(fullHouseHand2)
	if !is || high.value != 3 {
		t.Errorf("Hand: %v => full house, high 3. Got: (%v) %v", fullHouseHand2, is, high)
	}
}

func TestIsFourKind(t *testing.T) {
	is, high := IsFourKind(fourKindHand)
	if !is || high.value != 3 {
		t.Errorf("Hand: %v => four kind, high 3. Got: (%v) %v", fourKindHand, is, high)
	}
}

func TestIsStraightFlush(t *testing.T) {
	is, high := IsStraightFlush(straightFlushHand)
	if !is || high.value != 14 {
		t.Errorf("Hand: %v => straight flush, high 14. Got: (%v) %v", straightFlushHand, is, high)
	}
}

type HandTypeTest struct {
	hand     string
	handType HandType
}

func TestGetHandType(t *testing.T) {
	tests := []HandTypeTest{
		HandTypeTest{"5H 5C 6S 7S KD", HandType{ONE_PAIR, Card{0, 5}}},
		HandTypeTest{"2C 3S 8S 8D TD", HandType{ONE_PAIR, Card{0, 8}}},
		HandTypeTest{"5D 8C 9S JS AC", HandType{HIGH, Card{0, 14}}},
		HandTypeTest{"2C 5C 7D 8S QH", HandType{HIGH, Card{0, 12}}},
		HandTypeTest{"2D 9C AS AH AC", HandType{THREE_KIND, Card{0, 14}}},
		HandTypeTest{"3D 6D 7D TD QD", HandType{FLUSH, Card{0, 12}}},
		HandTypeTest{"4D 6S 9H QH QC", HandType{ONE_PAIR, Card{0, 12}}},
		HandTypeTest{"2H 2D 4C 4D 4S", HandType{FULL_HOUSE, Card{0, 4}}},
		HandTypeTest{"2H 2D 2C 4D 4S", HandType{FULL_HOUSE, Card{0, 2}}},
		HandTypeTest{"2H 2D 4C 4D 5S", HandType{TWO_PAIR, Card{0, 4}}},
		HandTypeTest{"2H 3D 4C 5D 6S", HandType{STRAIGHT, Card{0, 6}}},
		HandTypeTest{"2H 3H 4H 5H 6H", HandType{STRAIGHT_FLUSH, Card{0, 6}}},
		HandTypeTest{"2H 2D 2S 2C 4S", HandType{FOUR_KIND, Card{0, 2}}},
	}
	for _, test := range tests {
		hand := NewHand(ReadCards(test.hand))
		got := GetHandType(hand)
		if got.name != test.handType.name {
			t.Errorf("Hand: %v => %v. Want: %v", test.hand, got, test.handType)

		}
	}
}

func TestGetHandType2(t *testing.T) {
	tests := []HandTypeTest{
		HandTypeTest{"5H 5C 6S 7S KD", HandType{ONE_PAIR, Card{0, 5}}},
		HandTypeTest{"2C 3S 8S 8D TD", HandType{ONE_PAIR, Card{0, 8}}},
		HandTypeTest{"5D 8C 9S JS AC", HandType{HIGH, Card{0, 14}}},
		HandTypeTest{"2C 5C 7D 8S QH", HandType{HIGH, Card{0, 12}}},
		HandTypeTest{"2D 9C AS AH AC", HandType{THREE_KIND, Card{0, 14}}},
		HandTypeTest{"3D 6D 7D TD QD", HandType{FLUSH, Card{0, 12}}},
		HandTypeTest{"4D 6S 9H QH QC", HandType{ONE_PAIR, Card{0, 12}}},
		HandTypeTest{"2H 2D 4C 4D 4S", HandType{FULL_HOUSE, Card{0, 4}}},
		HandTypeTest{"2H 2D 2C 4D 4S", HandType{FULL_HOUSE, Card{0, 2}}},
		HandTypeTest{"2H 2D 4C 4D 5S", HandType{TWO_PAIR, Card{0, 4}}},
		HandTypeTest{"2H 3D 4C 5D 6S", HandType{STRAIGHT, Card{0, 6}}},
		HandTypeTest{"2H 3H 4H 5H 6H", HandType{STRAIGHT_FLUSH, Card{0, 6}}},
		HandTypeTest{"2H 2D 2S 2C 4S", HandType{FOUR_KIND, Card{0, 2}}},
	}
	for _, test := range tests {
		hand := NewHand(ReadCards(test.hand))
		got := GetHandType2(hand)
		if got.name != test.handType.name {
			t.Errorf("Hand: %v => %v. Want: %v", test.hand, got, test.handType)

		}
	}
}

func TestReadCards(t *testing.T) {
	cards := "5H 5C 6S 7S KD"
	want := twoHand
	got := ReadCards(cards)
	if len(got) != len(want) {
		t.Errorf("ReadCards(%v) => %v. Want: %v", cards, got, want)
	}
	for i := range got {
		if got[i].value != want[i].value || got[i].suite != want[i].suite {
			t.Errorf("ReadCards(%v) => %v. Want: %v", cards, got, want)
		}
	}
}

type WinnerTest struct {
	a, b   string
	winner int
}

func TestWinner(t *testing.T) {
	tests := []WinnerTest{
		WinnerTest{"5H 5C 6S 7S KD", "2C 3S 8S 8D TD", 2},
		WinnerTest{"5D 8C 9S JS AC", "2C 5C 7D 8S QH", 1},
		WinnerTest{"2D 9C AS AH AC", "3D 6D 7D TD QD", 2},
		WinnerTest{"4D 6S 9H QH QC", "3D 6D 7H QD QS", 1},
		WinnerTest{"2H 2D 4C 4D 4S", "3C 3D 3S 9S 9D", 1},
	}
	for _, test := range tests {
		a := NewHand(ReadCards(test.a))
		b := NewHand(ReadCards(test.b))
		want := test.winner
		got := Winner(a, b)
		if got != want {
			t.Errorf("Winner(%v, %v) => %v. Want: %v", a, b, got, want)
		}
	}

}
