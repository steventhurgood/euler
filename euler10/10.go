package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

type Sieve struct {
	factorable []bool
}

func NewSieve(n int) *Sieve {
	s := &Sieve{
		factorable: make([]bool, n),
	}
	// we say that 0 and 1 are not primes.
	copy(s.factorable[:2], []bool{true, true})
	for i := 2; i < n; i++ {
		if !s.factorable[i] {
			for j := 2 * i; j < n; j += i {
				s.factorable[j] = true
			}
		}
	}
	return s
}

func (s *Sieve) Primes() []int {
	log.Print("sieve:", s.factorable)
	primes := make([]int, 0, 100)
	for i, factorable := range s.factorable {
		if !factorable {
			primes = append(primes, i)
		}
	}
	return primes
}

func (s *Sieve) Print() string {
	return fmt.Sprintf("%v", s.factorable)
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		num, err := strconv.ParseInt(arg, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		s := NewSieve(int(num))
		primes := s.Primes()
		sum := 0
		for _, p := range primes {
			sum += p
		}
	}
}
