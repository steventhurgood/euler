package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"os"

	"bitbucket.org/steventhurgood/euler/splitwords"
)

// triangle word: sum of letter values of numbers is a triangle number.
// eg., : sky = 19 + 11 + 25 = 55
// 55 is the 10th triangle number.

func WordValue(word string) int {
	sum := 0
	for _, rune := range word {
		sum += int(rune-'A') + 1
	}
	return sum
}

// type TriangleNumberTester map[int]int
type TriangleNumberTester struct {
	test   map[int]int
	maxNum int
	maxN   int
}

type TriangleWord struct {
	word string
	sum  int
	n    int
}

func (t TriangleWord) String() string {
	return fmt.Sprintf("%s (sum: %d) Triangle Number: %d", t.word, t.sum, t.n)
}

func (t *TriangleNumberTester) IsTriangleNumber(n int) (bool, int) {
	if n > t.maxNum {
		for n > t.maxNum {
			t.Grow(n - t.maxNum)
		}
	}
	if t, ok := t.test[n]; ok {
		return true, t
	}
	return false, 0
}

func NewTriangleNumberTester(n int) TriangleNumberTester {
	t := make(map[int]int)
	i := 1
	number := 0
	for i = 1; i <= n; i++ {
		number = int((i * (i + 1)) / 2)
		t[number] = i
	}
	return TriangleNumberTester{
		test:   t,
		maxNum: number,
		maxN:   i - 1,
	}
}

func (t *TriangleNumberTester) Grow(n int) *TriangleNumberTester {
	i := 1
	number := 0
	for i = t.maxN + 1; i <= t.maxN+1+n; i++ {
		number = int((i * (i + 1)) / 2)
		(*t).test[number] = i
	}
	log.Print("Grown to: ", number, "(", i-1, ")")
	t.maxNum = number
	t.maxN = i - 1
	return t
}

func IsTriangleWord(word string) bool {
	in := make(chan string)
	out := TriangleWordFilter(in, 4)
	go func() {
		in <- word
		close(in)
	}()
	_, ok := <-out
	return ok
}

func TriangleWordFilter(in chan string, maxDigits int) chan TriangleWord {
	// max number need to test is 'zzzzzz' = 26*n
	// triangle number is t = 0.5 * n(n+1).
	// So to have a number larger than t, we need to be be:
	// k > t; t ~ 0.5 * n^2, 2k = n^2; n = sqrt(2t)

	size := int(math.Sqrt(float64(2 * 26 * maxDigits)))
	t := NewTriangleNumberTester(size)
	ch := make(chan TriangleWord)
	go func() {
		for w := range in {
			numValue := WordValue(w)
			if ok, n := t.IsTriangleNumber(numValue); ok {
				ch <- TriangleWord{
					word: w,
					sum:  numValue,
					n:    n,
				}
			}
		}
		close(ch)
	}()
	return ch
}

func main() {
	// url := flag.String("url", "http://projecteuler.net/project/words.txt", "url from which to read words")
	url := flag.String("url", "", "url from which to read words")
	flag.Parse()
	var input io.Reader
	if *url != "" {
		resp, err := http.Get(*url)
		if err != nil {
			log.Fatal(err)
		}
		input = resp.Body
	} else {
		input = os.Stdin
	}
	count := 0
	// for name := range splitwords.Names(input) {
	for name := range TriangleWordFilter(splitwords.Names(input), 20) {
		fmt.Println(name)
		count++
	}
	fmt.Println("Count: ", count)
}
