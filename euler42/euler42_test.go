package main

import "testing"

func TestTriangleNumberTester(t *testing.T) {
	expected := []int{1, 3, 6, 10, 15, 21, 28, 36}
	got := NewTriangleNumberTester(8)
	if len(got.test) != len(expected) {
		t.Error("expected: ", expected, " got: ", got, "(", len(expected), ")")
	}
	for _, v := range expected {
		if _, ok := got.test[v]; !ok {
			t.Error("expected: ", expected, " got: ", got)
		}
	}
	if b, _ := got.IsTriangleNumber(36); !b {
		t.Error("Expected 55 to be a triangle number.")
	}
	if b, _ := got.IsTriangleNumber(29); b {
		t.Error("Expected 29 to not be a triangle number.")
	}
}

func TestIsTriangleWord(t *testing.T) {
	w := "SKY"
	b := IsTriangleWord(w)
	if !b {
		t.Error(w, " is a triangle word")
	}
}

func TestWordValue(t *testing.T) {
	w := "SKY"
	v := WordValue(w)
	if v != 55 {
		t.Error("Expected value of ", w, " == 55; got: ", v)
	}
}
