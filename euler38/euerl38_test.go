package main

import "testing"

func TestConcatNumber(t *testing.T) {
	n := []int{123, 456}
	expected := 123456
	c := ConcatNumber(n)
	if c != expected {
		t.Error("expected: ", expected, " got: ", c)
	}
}

func TestIsPandigital(t *testing.T) {
	good := []int{123, 456, 789}
	if !IsPandigital(good) {
		t.Error(good, " is pandigital")
	}
	bad := []int{1234, 5678, 91}
	if IsPandigital(bad) {
		t.Error(bad, " is not pandigital")
	}
	bad = []int{1234, 5608, 91}
	if IsPandigital(bad) {
		t.Error(bad, " is not pandigital")
	}
	bad = []int{1234, 5678}
	if IsPandigital(bad) {
		t.Error(bad, " is not pandigital")
	}
}

func TestNumberRange(t *testing.T) {
	tests := [][3]int{
		[3]int{3, 10, 999},
		[3]int{5, 0, 9},
	}
	for _, test := range tests {
		start, end := NumberRange(test[0])
		if start != test[1] && end != test[2] {
			t.Error("Expected start=", test[1], " end=", test[2], " got start=", start, " end=", end)
		}
	}
}

func TestLargestNDigits(t *testing.T) {
	n := 1
	expected := 9
	largest := LargestNDigits(n)
	if largest != expected {
		t.Error("Expected largest digit of ", n, " characters to be: ", expected, " got: ", largest)
	}
	n = 3
	expected = 999
	largest = LargestNDigits(n)
	if largest != expected {
		t.Error("Expected largest digit of ", n, " characters to be: ", expected, " got: ", largest)
	}
}

func TestSmallestNDigits(t *testing.T) {
	n := 1
	expected := 1
	smallest := SmallestNDigits(n)
	if smallest != expected {
		t.Error("Expected smallest digit of ", n, " characters to be: ", expected, " got: ", smallest)
	}
	n = 3
	expected = 100
	smallest = SmallestNDigits(n)
	if smallest != expected {
		t.Error("Expected smallest digit of ", n, " characters to be: ", expected, " got: ", smallest)
	}
}

func TestRangeProducts(t *testing.T) {
	n, num := 3, 2
	expected := []int{2, 4, 6}
	numberRange, _ := RangeProducts(n, num)
	if len(numberRange) != len(expected) {
		t.Error("expected: ", expected, " got: ", numberRange)
	}
	for i := range numberRange {
		if numberRange[i] != expected[i] {
			t.Error("expected: ", expected, " got: ", numberRange)
		}
	}
}
