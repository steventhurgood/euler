package main

import (
	"flag"
	"fmt"
)

func IsPalindrome(n []int) bool {
	// {1,2,3} ->[0,1] {1,2,21} -> [0,1]
	end := len(n) - 1
	midpoint := len(n) / 2
	for i := 0; i < midpoint; i++ {
		if n[i] != n[end-i] {
			return false
		}
	}
	return true
}

// convert a number to a different base, n
// valid numbers have no right-most 0s.
func ValidBaseConvert(n, base int) ([]int, bool) {
	rightmost := true
	digits := []int{}
	for n > 0 {
		digit := n % base
		if rightmost && digit == 0 {
			return nil, false
		}
		rightmost = false
		digits = append(digits, digit)
		n = (n - digit) / base
	}
	end := len(digits) - 1
	for i := 0; i < len(digits)/2; i++ {
		digits[i], digits[end-i] = digits[end-i], digits[i]
	}
	return digits, true
}

func main() {
	upto := flag.Int("upto", 100, "test numbers up to upto")
	flag.Parse()
	palindromes := [][][]int{}
	for i := 1; i < *upto; i += 2 {
		base10, valid := ValidBaseConvert(i, 10)
		if !valid {
			continue
		}
		base2, valid := ValidBaseConvert(i, 2)
		if !valid {
			continue
		}
		if IsPalindrome(base10) && IsPalindrome(base2) {
			palindromes = append(palindromes,
				[][]int{
					[]int{i},
					base10,
					base2,
				})
		}
	}
	sum := 0
	for _, p := range palindromes {
		fmt.Println(p)
		sum += p[0][0]
	}
	fmt.Println("Sum: ", sum)
}
