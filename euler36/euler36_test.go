package main

import "testing"

func TestInvalidBase(t *testing.T) {
	n := 1230
	digits, valid := ValidBaseConvert(n, 10)
	t.Log(digits)
	if valid {
		t.Error(n, " is invalid in base 10. Got: ", digits)
	}
	n = 128
	digits, valid = ValidBaseConvert(n, 2)
	t.Log(digits)
	if valid {
		t.Error(n, " is invalid in base 2. Got: ", digits)
	}
}

func TestValidBaseConver(t *testing.T) {
	n := 1023
	expectedBase10 := []int{1, 0, 2, 3}
	expectedBase2 := []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

	digits, valid := ValidBaseConvert(n, 10)
	t.Log(digits)
	if !valid {
		t.Error("expected valid: ", expectedBase10, " got: ", digits)
	}
	if len(digits) != len(expectedBase10) {
		t.Error("expected: ", expectedBase10, " got: ", digits)
	}
	for i := range digits {
		if digits[i] != expectedBase10[i] {
			t.Error("expected: ", expectedBase10, " got: ", digits)
		}

	}

	digits, valid = ValidBaseConvert(n, 2)
	t.Log(digits)
	if !valid {
		t.Error("expected valid: ", expectedBase2, " got: ", digits)
	}
	if len(digits) != len(expectedBase2) {
		t.Error("expected: ", expectedBase2, " got: ", digits)
	}
	for i := range digits {
		if digits[i] != expectedBase2[i] {
			t.Error("expected: ", expectedBase2, " got: ", digits)
		}
	}
}

func TestIsPalindrome(t *testing.T) {
	valid := [][]int{
		[]int{1, 2, 3, 2, 1},
		[]int{1, 2, 3, 3, 2, 1},
		[]int{1},
	}
	invalid := [][]int{
		[]int{1, 2, 3, 1},
	}
	for _, n := range valid {
		if !IsPalindrome(n) {
			t.Error("Expected: ", n, " to be valid")
		}
	}
	for _, n := range invalid {
		if IsPalindrome(n) {
			t.Error("Expected: ", n, " to be invalid")
		}
	}
}
