package main

import (
	"flag"
	"fmt"
)

type Sieve struct {
	factorable []bool
}

// Create a new sieve with 0 and 1 set to factorable, 2 as prime
func NewSieve() *Sieve {
	return &Sieve{factorable: []bool{true, true, false}}
}

// grow the sieve
func (s *Sieve) extend(n int) {
	max := len(s.factorable) + n
	newSieve := make([]bool, max)
	copy(newSieve[:len(s.factorable)], s.factorable)
	for i := 2; i < max; i++ {
		if !newSieve[i] {
			// log.Print("max =", max)
			for j := 2; j*i < max; j++ {
				//log.Print(i * j, "=", i, "*", j)
				newSieve[i*j] = true
			}
		}
	}
	s.factorable = newSieve
}

func (s *Sieve) String() string {
	return fmt.Sprint(s.factorable)
}

func (s *Sieve) primes() []int {
	r := make([]int, 0, 100)
	for i := 2; i < len(s.factorable); i++ {
		if !s.factorable[i] {
			r = append(r, i)
		}
	}
	return r
}

func main() {
	n := flag.Int("n", 13195, "the number whose largest prime factor to find")
	s := NewSieve()
	incremental := 100
	flag.Parse()
	p := s.primes()
	for {
		if len(p) >= *n {
			break
		}
		s.extend(incremental)
		p = s.primes()
	}
	fmt.Println(p[:*n])
}
