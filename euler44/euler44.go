package main

import (
	"flag"
	"log"
	"math"
)

/*
pentagonal number Pn = n(3n-1)/2 = (3n^2-n)/2

3/2n^2 - 1/2n -Pn = 0

n = 1/2 +- sqrt(1/4 - 4 * (3/2) * Pn)
    --------------------------------
                   3


find pairs of numbers j, k where:
Pj is pentagonal, Pk is pentagonal,
P(j+k) is pentagonal, P(k-j) is pentagonal, and the difference:

D = Pk - Pj is minimised.

Hence a stop condition is when You've found a pair, k and j such that D is less than the difference between the current Pk and P(k+1).

*/

func IsPentagonalNumber(n int) (int, bool) {
	rt := math.Sqrt(0.25 + 6*float64(n))
	a := (0.5 + rt) / 3.0
	return int(a), math.Floor(a) == a
}

func PentagonalNumber(n int) int {
	return (n * (3*n - 1)) / 2
}

func main() {
	flag.Parse()
	i, j := 1, 1
OuterLoop:
	for {
		Pi := PentagonalNumber(i)
		for j = i - 1; j > 0; j-- {
			Pj := PentagonalNumber(j)
			sum := Pi + Pj
			diff := Pi - Pj
			sumN, isPentSum := IsPentagonalNumber(sum)
			diffN, isPentDiff := IsPentagonalNumber(diff)
			if isPentSum && isPentDiff {
				log.Print("i: ", i, "j: ", j, "Pi: ", Pi, " Pj: ", Pj, " sum: ", sum, isPentSum, "[", sumN, "]", " diff: ", diff, isPentDiff, "[", diffN, "]")
				break OuterLoop
			}
		}
		i++
	}
}
