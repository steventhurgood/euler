package main

import "testing"

func TestIsPentagonalNumber(t *testing.T) {
	expected := []int{1, 5, 12, 22, 35, 51, 70, 92, 117, 145}
	for i, e := range expected {
		n, is := IsPentagonalNumber(e)
		if !is {
			t.Error("expected: ", e, " is pengtagonal number. Got: ", n)
		}
		if i != n-1 {
			t.Error("expected: ", e, " is pengtagonal number ", i, ". Got: ", n)
		}
	}
}
