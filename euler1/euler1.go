package main

import (
"flag"
"fmt"
"log"
)

func ThreeFiveSum(upto int) int {
sum := 0
	for i := 1; i < upto; i++ {
	if i % 3 == 0 || i % 5 == 0 {
	log.Print(i)
	sum += i
	}
	}
	return sum
}

func main() {
	upto := flag.Int("upto", 10, "The number up to which we sum all the values divisable by 3 or 5")
	flag.Parse()

	sum := ThreeFiveSum(*upto)
	fmt.Println(sum)
}
