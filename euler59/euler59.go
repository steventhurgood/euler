package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func GetCipherText(url string) []byte {
	ciphertext := make([]byte, 0)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(resp.Body)
	for s.Scan() {
		line := s.Text()
		for _, n := range strings.Split(line, ",") {
			n = strings.TrimSpace(n)
			intn, err := strconv.ParseInt(n, 10, 8)
			if err != nil {
				log.Fatal(err)
			}
			ciphertext = append(ciphertext, byte(intn))
		}

	}
	return ciphertext
}

func CharsUsed(chars []byte, skip, offset int) []int {
	count := make([]int, 256)
	for i := offset; i < len(chars); i += skip {
		count[chars[i]]++
	}
	return count
}

func NumChars(counts []int) int {
	count := 0
	for _, c := range counts {
		if c != 0 {
			count++
		}
	}
	return count
}

func PrintCharHistogram(counts []int) {
	for i, c := range counts {
		if c > 0 {
			fmt.Printf("%v: ", i)
			for j := 0; j < c; j++ {
				fmt.Print("x")
			}
			fmt.Print("\n")
		}
	}
}

func MostPopular(counts []int) byte {
	maxi, max := 0, 0
	for i, c := range counts {
		if c > max {
			max = c
			maxi = i
		}
	}
	return byte(maxi)
}

func GuessKey(counts []int) byte {
	p := MostPopular(counts)
	fmt.Println("Most common: ", p)
	return p ^ ' '
}

func Decode(ciphertext, key []byte) []byte {
	keyLen := len(key)
	decoded := make([]byte, len(ciphertext))
	for i := range ciphertext {
		offset := i % keyLen
		decoded[i] = ciphertext[i] ^ key[offset]
	}
	return decoded
}

var (
	url = flag.String("url", "https://projecteuler.net/project/resources/p059_cipher.txt", "url containing ciphertext")
)

func main() {
	key := make([]byte, 3)
	flag.Parse()
	c := GetCipherText(*url)
	fmt.Println(c)
	for i := 0; i < 3; i++ {
		count := CharsUsed(c, 3, i)
		fmt.Println("Chars used for ", i, ": ", NumChars(count))
		PrintCharHistogram(count)
		key[i] = GuessKey(count)
	}
	fmt.Println("key: ", key)
	sum := 0
	decoded := Decode(c, key)
	for _, char := range decoded {
		sum += int(char)
	}
	fmt.Println(decoded)
	fmt.Println(string(decoded))
	fmt.Println("Sum: ", sum)
}
