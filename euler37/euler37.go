package main

import (
	"flag"
	"fmt"
	"log"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

// for an integer 1234 return a list: 4, 34, 234
func TruncateLeft(n int) []int {
	digits := []int{}
	multiplier := 1
	current := 0
	for n > 9 {
		digit := n % 10
		n = (n - digit) / 10
		current = current + digit*multiplier
		multiplier *= 10
		digits = append(digits, current)
	}
	return digits
}

// for an integer 1234 return a list: 1, 12, 123
func TruncateRight(n int) []int {
	digits := []int{}
	for n > 9 {
		digit := n % 10
		n = (n - digit) / 10
		digits = append(digits, n)
	}
	return digits
}

func main() {
	upto := flag.Int("upto", -1, "numbers to test")
	flag.Parse()
	if *upto > 1 {
		s := prime.NewSieve(*upto)
		truncatablePrimes := []int{}
		for _, p := range s.Primes() {
			if p < 10 {
				continue
			}
			allPrime := true
			for _, t := range TruncateLeft(p) {
				if isPrime, err := s.IsPrime(t); !isPrime && err == nil {
					allPrime = false
				}
			}
			for _, t := range TruncateRight(p) {
				if isPrime, err := s.IsPrime(t); !isPrime && err == nil {
					allPrime = false
				}
			}
			if allPrime {
				truncatablePrimes = append(truncatablePrimes, p)
			}
		}
		sum := 0
		for _, tp := range truncatablePrimes {
			sum += tp
			fmt.Println(tp)
		}
		fmt.Println("sum: ", sum)
	}
	s := prime.NewSieve(10000)
	for _, arg := range intargs.IntArgs(flag.Args()) {
		if isPrime, err := s.IsPrime(arg); !isPrime && err != nil {
			fmt.Println(arg, "is not prime")
			continue
		}

		allPrime := true
		for _, t := range TruncateLeft(arg) {
			if isPrime, err := s.IsPrime(t); !isPrime && err == nil {
				allPrime = false
				fmt.Println(t, " is not prime")
			} else {
				if err != nil {
					log.Print(err)
				}
				fmt.Println(t, " is prime (left)")
			}
		}
		for _, t := range TruncateRight(arg) {
			if isPrime, err := s.IsPrime(t); !isPrime && err == nil {
				allPrime = false
				fmt.Println(t, " is not prime")
			} else {
				if err != nil {
					log.Print(err)
				}
				fmt.Println(t, " is prime (right)")
			}
		}
		if allPrime {
			fmt.Println("all prime")
		}
	}
}
