package main

import "testing"

func TestTruncateLeft(t *testing.T) {
	n := 1234
	expected := []int{
		4,
		34,
		234,
	}
	truncated := TruncateLeft(n)
	if len(truncated) != len(expected) {
		t.Error("expected: ", expected, " got: ", truncated)
	}
	for i := range truncated {
		if truncated[i] != expected[i] {
			t.Error("expected: ", expected, " got: ", truncated)
		}
	}
}

func TestTruncateRight(t *testing.T) {
	n := 1234
	expected := []int{
		123,
		12,
		1,
	}
	truncated := TruncateRight(n)
	if len(truncated) != len(expected) {
		t.Error("expected: ", expected, " got: ", truncated)
	}
	for i := range truncated {
		if truncated[i] != expected[i] {
			t.Error("expected: ", expected, " got: ", truncated)
		}
	}
}
