package main

import (
	"testing"

	"bitbucket.org/steventhurgood/euler/prime"
)

func TestIsPangdigital(t *testing.T) {
	if i := IsPandigital(987654103, 9); i {
		t.Error("987654103 is not pandigital")
	}

	if i := IsPandigital(987654321, 9); !i {
		t.Error("987654321 is pandigital")
	}
	if i := IsPandigital(4321, 4); !i {
		t.Error("4321 is pandigital")
	}
	if i := IsPandigital(1237, 4); i {
		t.Error("1237 is not pandigital")
	}
}

func TestIsprime(t *testing.T) {
	s := prime.NewSieve(10000)
	if i := IsPrime(2143, s.Primes()); !i {
		t.Error("2143 is prime")
	}
}

func TestTestNum(t *testing.T) {
	s := prime.NewSieve(10000)
	if i := TestNum(2143, 4, s.Primes()); !i {
		t.Error("2143 is both prime and pandigital")
	}
}

func TestFilter(t *testing.T) {
	in := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	expected := []int{2, 4, 6, 8}
	isEven := func(n int) bool { return n%2 == 0 }
	ch := make(chan int)
	out := Filter(ch, isEven)
	go func() {
		for _, n := range in {
			ch <- n
		}
		close(ch)
	}()
	i := 0
	for n := range out {
		if n != expected[i] {
			t.Error("Expected filtered[", i, "] == ", expected[i], " got: ", n, " (", expected, ")")
		}
		i++
	}
}
