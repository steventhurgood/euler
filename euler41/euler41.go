package main

import (
	"flag"
	"fmt"
	"log"
	"math"

	"bitbucket.org/steventhurgood/euler/pandigital"
	"bitbucket.org/steventhurgood/euler/prime"
)

func IsPandigital(n, digits int) bool {
	seenDigits := [10]bool{true}
	count := 0
	for n > 0 {
		digit := n % 10
		if seenDigits[digit] {
			return false
		}
		if digit > digits {
			return false
		}
		seenDigits[digit] = true
		count++
		n = (n - digit) / 10
	}
	return count == digits
}

func IsPrime(n int, primes []int) bool {
	max := int(math.Sqrt(float64(n)))
	if max > primes[len(primes)-1] {
		log.Fatal("unsufficient primes for ", n, ": ", max, " > ", primes[len(primes)-1], "[", len(primes), "]")
	}
	for _, p := range primes {
		if n%p == 0 {
			return false
		}
		if p > max {
			return true
		}
	}
	return true
}

func IsPrimeFilter(primes []int) func(int) bool {
	return func(n int) bool {
		return IsPrime(n, primes)
	}
}

func TestNum(n, digits int, primes []int) bool {
	return IsPandigital(n, digits) && IsPrime(n, primes)
}

func TestNumFilter(digits int, primes []int) func(int) bool {
	return func(n int) bool {
		if n%6000 == 1 {
			log.Print(n)
		}
		return IsPandigital(n, digits) && IsPrime(n, primes)
	}
}

func DigitRange(n int) (int, int) {
	if n == 1 {
		return 0, 9
	}
	r := 1
	for ; n > 1; n-- {
		r *= 10
	}
	return r, (r * 10) - 1
}

type StoppableChannel struct {
	ch      chan int
	stop    chan bool
	stopped bool
}

func Pandigitals(n int) StoppableChannel {
	ch := make(chan int)
	stop := make(chan bool)
	go func() {
		for _, p := range pandigital.PandigitalNumbers(n) {
			ch <- p
		}
		stop <- true
	}()
	return StoppableChannel{
		ch:      ch,
		stop:    stop,
		stopped: false,
	}
}

// return a channel of integers of potential primes of length n
func NDigitPotentialPrimes(n int) StoppableChannel {
	ch := make(chan int)
	stop := make(chan bool)
	start, end := DigitRange(n)
	start = start - (start % 6)
	go func() {
		for i := start; i < end; i += 6 {
			ch <- i - 1
			ch <- i + 1
		}
		stop <- true
	}()
	return StoppableChannel{ch, stop, false}
}

func Filter(in chan int, filter func(int) bool) chan int {
	ch := make(chan int)
	go func() {
		for {
			value, ok := <-in
			if !ok {
				close(ch)
				break
			}
			if filter(value) {
				ch <- value
			}
		}
	}()
	return ch
}

func DeMux(c []StoppableChannel) chan int {
	ch := make(chan int)
	go func() {
		allStopped := false
		for !allStopped {
			allStopped = true
			for i := range c {
				if !c[i].stopped {
					allStopped = false
					select {
					case _ = <-c[i].stop:
						{
							c[i].stopped = true
						}
					case num := <-c[i].ch:
						{
							ch <- num
						}
					default:
						{
						}
					}
				}
			}
		}
		close(ch)
	}()
	return ch
}

func main() {
	maxDigits := flag.Int("max_digits", 9, "maximum number of digits to use when finding pandigital primes")
	flag.Parse()
	maxPrime := int(math.Sqrt(987654321))
	s := prime.NewSieve(2 * maxPrime)
	primes := s.Primes()
	pandigitalPrimes := []int{}

	primeSources := make([]StoppableChannel, 0, 10)
	for digits := 4; digits <= *maxDigits; digits++ {
		c := Pandigitals(digits)
		c.ch = Filter(c.ch, IsPrimeFilter(primes))
		primeSources = append(primeSources, c)
	}
	for i := range DeMux(primeSources) {
		pandigitalPrimes = append(pandigitalPrimes, i)
	}
	for _, p := range pandigitalPrimes {
		fmt.Println(p)
	}
}
