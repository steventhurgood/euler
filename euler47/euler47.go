package main

import (
	"flag"
	"fmt"
	"log"

	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
)

func Distinct(divisors []prime.Divisor) bool {
	seen := map[[2]int]bool{}
	for _, d := range divisors {
		for prime, power := range d {
			if seen[[2]int{prime, power}] {
				return false
			}
			seen[[2]int{prime, power}] = true
		}
	}
	return true
}

func main() {
	sieveSize := flag.Int("sieve_size", 1000000, "size of prime sieve")
	consecutive := flag.Int("consecutive", 0, "number of consecutive distinctly factorizable numbers to find")
	distinct := flag.Int("distinct", 0, "number of distinct factors")
	flag.Parse()
	s := prime.NewSieve(*sieveSize)

	divisors := make([]prime.Divisor, *consecutive)
	if *consecutive > 0 {
		if *distinct == 0 {
			*distinct = *consecutive
		}
		i := 3
		found := false
		counted := 0
		for !found {
			if i%1000 == 0 {
				log.Print(i, "..", i+1000)
			}
			d, _ := s.Divisors(i)
			if len(d) != *distinct {
				i++
				counted = 0
				divisors = make([]prime.Divisor, *consecutive)
				continue
			}
			counted++
			divisors[i%*consecutive] = d
			if counted >= *consecutive && Distinct(divisors) {
				found = true
			}
			i++
		}
		for _, d := range divisors {
			fmt.Println(prime.Unfactor(d), d)
		}
	} else {

		for _, n := range intargs.IntArgs(flag.Args()) {
			factors, _ := s.Divisors(n)
			divisors = append(divisors, factors)
			fmt.Println(factors)
		}
		if Distinct(divisors) {
			fmt.Println("distinct")
		}
	}
}
