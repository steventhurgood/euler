package main

import (
	"bitbucket.org/steventhurgood/euler/prime"
	"testing"
)

func DuplicatesContains(duplicates []prime.BasePower, aFactors prime.Factor, b int) bool {
	for _, d := range duplicates {
		if d.Power == b {
			if len(d.Base) == len(aFactors) {
				same := true
				for prime, power := range aFactors {
					dupPower := d.Base[prime]
					if dupPower != power {
						same = false
					}
				}
				if same {
					return true
				}
			}
		}
	}
	return false
}

func TestDuplicateContains(t *testing.T) {
	duplicates := []prime.BasePower{
		prime.BasePower{ // (2^2.3^4)^8 == 324^8
			Base: prime.Factor{
				2: 2,
				3: 4,
			},
			Power: 8,
		},
		prime.BasePower{
			Base: prime.Factor{
				2: 1,
				3: 2,
			},
			Power: 16,
		},
	}
	expected := prime.BasePower{
		Base: prime.Factor{
			2: 1,
			3: 2,
		},
		Power: 16,
	}
	notExpected := prime.BasePower{
		Base: prime.Factor{
			2: 1,
			3: 2,
			5: 1,
		},
		Power: 16,
	}
	if !DuplicatesContains(duplicates, expected.Base, expected.Power) {
		t.Error(duplicates, " should contain ", expected)
	}
	if DuplicatesContains(duplicates, notExpected.Base, notExpected.Power) {
		t.Error(duplicates, " should not contain ", notExpected)
	}
}

func TestFindDuplicates(t *testing.T) {
	a, b := 8, 10
	// duplicates of 4^15 should include:
	//   4 ^ 15
	//   2 ^ 30
	expected := []prime.BasePower{
		prime.BasePower{ // (2^2.3^4)^8 == 324^8
			Base: prime.Factor{
				2: 2,
			},
			Power: 15,
		},
		prime.BasePower{
			Base: prime.Factor{
				2: 1,
			},
			Power: 30,
		},
	}

	s := prime.NewSieve(10000)
	factors, _ := s.Divisors(a)
	duplicates := FindDuplicates(a, b, 100, factors)
	if len(duplicates) != 2 {
		t.Error("expected 2 duplicates. Got: ", duplicates)
	}
	for _, testDup := range expected {
		if !DuplicatesContains(duplicates, testDup.Base, testDup.Power) {
			t.Error("expected ", duplicates, " to contain ", testDup)
		}
	}
}
