package main

import (
	"flag"
	"fmt"
	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
	"log"
	// "math"
)

func MinPower(factors map[int]int) int {
	minPower := -1
	for _, power := range factors {
		if minPower < 0 {
			minPower = power
		} else {
			if power < minPower {
				minPower = power
			}
		}
	}
	return minPower
}

func DividesAll(factor int, factors map[int]int, b int) bool {
	dividesall := true
	for _, power := range factors {
		if (power*b)%factor != 0 {
			dividesall = false
			break
		}
	}
	return dividesall
}

func FindDuplicates(a, b, max int, factors map[int]int) []prime.BasePower {
	minPower := MinPower(factors)
	duplicates := []prime.BasePower{}
	maxPower := minPower * b
	for testB := b + 1; testB <= maxPower; testB++ {
		dividesall := DividesAll(testB, factors, b)
		// log.Print("testing: ", testB)
		if dividesall {
			// log.Print("divides: ", factors, b)
			if testB < max {
				newFactor := make(prime.Factor)
				bPrime := testB
				for prime, power := range factors {
					newFactor[prime] = (power * b) / testB
				}
				duplicates = append(duplicates, prime.BasePower{Base: newFactor, Power: bPrime})
			}
		}
	}
	return duplicates
}

func CountDuplicates(a, b, max int, factors map[int]int) int {
	minPower := MinPower(factors)
	maxPower := minPower * b
	num := 0
	for testB := b + 1; testB <= maxPower; testB++ {
		dividesall := DividesAll(testB, factors, b)
		// log.Print("testing: ", testB)
		if dividesall {
			// log.Print("divides: ", factors, b)
			if testB < max {
				num++
			}
		}
	}
	return num
}

func HasDuplicates(a, b, max int, factors map[int]int) bool {
	minPower := MinPower(factors)
	maxPower := minPower * b
	for testB := b + 1; testB <= maxPower; testB++ {
		dividesall := DividesAll(testB, factors, b)
		// log.Print("testing: ", testB)
		if dividesall {
			// log.Print("divides: ", factors, b)
			if testB < max {
				return true
			}
		}
	}
	return false
}

func main() {
	sieveSize := flag.Int("sieve_size", 100000, "size of sieve")
	uptoA := flag.Int("upto_a", -1, "test values of a up to upto_a, for a^b")
	uptoB := flag.Int("upto_b", -1, "test values of b up to upto_b, for a^b")
	flag.Parse()
	s := prime.NewSieve(*sieveSize)
	count := 0
	if *uptoA > 0 && *uptoB > 0 {
		for a := 2; a < *uptoA; a++ {
			factors, _ := s.Divisors(a)
			for b := 2; b < *uptoB; b++ {
				// result := math.Pow(float64(a), float64(b))
				if !HasDuplicates(a, b, *uptoB, factors) {
					count += 1
					// fmt.Println(a, "^", b, "=", result)
				} else {
					// duplicates := FindDuplicates(a, b, *uptoB, factors)
					// for _, d := range duplicates {
					// 	fmt.Println("dup: ", a, "^", b, "=", result, "(", d, ")")
					// }
				}
			}
		}
		fmt.Println("count: ", count)
	}
	args := intargs.IntArgs(flag.Args())
	if len(args)%2 != 0 {
		log.Fatal("usage: euler29 a b")
	}
	for i := 0; i < len(args); i += 2 {
		a, b := args[0+i], args[1+i]
		factors, _ := s.Divisors(a)
		duplicates := FindDuplicates(a, b, 100, factors)
		for _, d := range duplicates {
			fmt.Println(d)
		}
	}
}
