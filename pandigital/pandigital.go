package pandigital

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/intargs"
)

func SliceToInt(digits []int) int {
	r := 0
	for _, d := range digits {
		r = (r * 10) + d
	}
	return r
}

func PandigitalNumbers(numDigits int) []int {
	digits := make([]int, numDigits)
	pandigitals := make([]int, 0)
	for i := 1; i <= numDigits; i++ {
		digits[i-1] = i
	}
	for _, p := range Permutations(digits) {
		pandigitals = append(pandigitals, SliceToInt(p))
	}
	return pandigitals
}

func Permutations(digits []int) [][]int {
	if len(digits) == 1 {
		permutations := make([][]int, len(digits))
		for i, d := range digits {
			permutations[i] = []int{d}
		}
		return permutations
	}
	permutations := make([][]int, 0)
	for i, d := range digits {
		remaining := make([]int, len(digits)-1)
		for j := range digits {
			if i == j {
				continue
			}
			if j < i {
				remaining[j] = digits[j]
			} else {
				remaining[j-1] = digits[j]
			}
		}

		for _, p := range Permutations(remaining) {
			newPerm := make([]int, len(p)+1)
			newPerm[0] = d
			copy(newPerm[1:], p)
			permutations = append(permutations, newPerm)
		}
	}
	return permutations
}

func PermutationChannel(digits []int, parallelDigits int) chan []int {
	ch := make(chan []int)
	done := make(chan bool)
	l := len(digits)
	if l < parallelDigits {
		parallelDigits = l
	}
	if parallelDigits == 0 {
		ch := make(chan []int)
		go func() {
			p := Permutations(digits)
			for _, d := range p {
				ch <- d
			}
			close(ch)
		}()
		return ch
	}
	for i, d := range digits {
		permuD := d
		permuI := i
		go func() {
			remaining := make([]int, len(digits)-1)
			for j := range digits {
				if permuI == j {
					continue
				}
				if j < permuI {
					remaining[j] = digits[j]
				} else {
					remaining[j-1] = digits[j]
				}
			}

			for p := range PermutationChannel(remaining, parallelDigits-1) {
				newPerm := make([]int, len(p)+1)
				newPerm[0] = permuD
				copy(newPerm[1:], p)
				ch <- newPerm
			}
			done <- true
		}()
	}
	go func() {
		for l > 0 {
			<-done
			l--
		}
		close(ch)
	}()
	return ch
}

func main() {
	flag.Parse()
	for _, n := range intargs.IntArgs(flag.Args()) {
		ps := PandigitalNumbers(n)
		for _, p := range ps {
			fmt.Println(p)
		}
	}
}
