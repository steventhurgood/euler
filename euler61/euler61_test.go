package main

import "testing"

func TestTriangleNumber(t *testing.T) {
	want := []int{0, 1, 3, 6, 10, 15}
	for i := 1; i < 6; i++ {
		got := TriangleNumber(i)
		if got != want[i] {
			t.Errorf("TriangleNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestSquareNumber(t *testing.T) {
	want := []int{0, 1, 4, 9, 16, 25}
	for i := 1; i < 6; i++ {
		got := SquareNumber(i)
		if got != want[i] {
			t.Errorf("SquareNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestPentagonNumber(t *testing.T) {
	want := []int{0, 1, 5, 12, 22, 35}
	for i := 1; i < 6; i++ {
		got := PentagonNumber(i)
		if got != want[i] {
			t.Errorf("PentagonNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestHexagonNumber(t *testing.T) {
	want := []int{0, 1, 6, 15, 28, 45}
	for i := 1; i < 6; i++ {
		got := HexagonNumber(i)
		if got != want[i] {
			t.Errorf("HexagonNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestHeptagonNumber(t *testing.T) {
	want := []int{0, 1, 7, 18, 34, 55}
	for i := 1; i < 6; i++ {
		got := HeptagonNumber(i)
		if got != want[i] {
			t.Errorf("HeptagonNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}

func TestOctagonNumber(t *testing.T) {
	want := []int{0, 1, 8, 21, 40, 65}
	for i := 1; i < 6; i++ {
		got := OctagonNumber(i)
		if got != want[i] {
			t.Errorf("OctagonNumber(%v) -> %v. Want: %v", i, got, want[i])
		}
	}
}
