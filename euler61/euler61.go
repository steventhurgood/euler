package main

import (
	"bytes"
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/intargs"
)

func TriangleNumber(n int) int {
	return n * (n + 1) / 2
}

func SquareNumber(n int) int {
	return n * n
}

func PentagonNumber(n int) int {
	return n * (3*n - 1) / 2
}

func HexagonNumber(n int) int {
	return n * (2*n - 1)
}

func HeptagonNumber(n int) int {
	return n * (5*n - 3) / 2
}

func OctagonNumber(n int) int {
	return n * (3*n - 2)
}

type NGon struct {
	nGonType int
	value    int
}

type NGonList []NGon

type NGonListList []NGonList

func (n NGonListList) FindCycle(depth int) (bool, NGonList) {
	for prefix := range n {
		seen := make([]bool, depth)
		found, cycle := n.FindCyclePrefix(prefix, prefix, seen, depth)
		if found {
			return found, cycle
		}
	}
	return false, NGonList{}
}

func (n NGonListList) FindCyclePrefix(prefix, wantPrefix int, seen []bool, depth int) (bool, NGonList) {
	if depth == 0 {
		for _, s := range seen {
			if !s {
				return false, NGonList{}
			}
		}
		return prefix == wantPrefix, NGonList{}
	}
	for _, ngon := range n[prefix] {

		if ngon.nGonType < len(seen) && !seen[ngon.nGonType] {
			// ngon.value
			nextPrefix := ngon.value % 100
			newSeen := make([]bool, len(seen))
			copy(newSeen, seen)
			newSeen[ngon.nGonType] = true
			found, cycle := n.FindCyclePrefix(nextPrefix, wantPrefix, newSeen, depth-1)
			if found {
				newCycle := NGonList{ngon}
				newCycle = append(newCycle, cycle...)
				return true, newCycle
			}
		}
	}
	return false, NGonList{}
}

func (n NGonListList) String() string {
	var b bytes.Buffer
	for prefix, ngons := range n {
		fmt.Fprintf(&b, "%02v: ", prefix)
		for _, ngon := range ngons {
			fmt.Fprintf(&b, "(%v-gon) -> %v ", ngon.nGonType, ngon.value)
		}
		fmt.Fprintf(&b, "\n")
	}
	return b.String()
}

func GenerateNGons() NGonListList {
	ngons := make(NGonListList, 100) // prefixes 00 -> 99

	// make triangles
	for i := 0; ; i++ {
		t := TriangleNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{0, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	for i := 0; ; i++ {
		t := SquareNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{1, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	for i := 0; ; i++ {
		t := PentagonNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{2, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	for i := 0; ; i++ {
		t := HexagonNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{3, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	for i := 0; ; i++ {
		t := HeptagonNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{4, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	for i := 0; ; i++ {
		t := OctagonNumber(i)
		if t > 9999 {
			break
		}
		if t > 999 {
			prefix := t / 100
			nextNum := NGon{5, t}
			ngons[prefix] = append(ngons[prefix], nextNum)
		}
	}
	return ngons
}

func main() {
	flag.Parse()
	ngons := GenerateNGons()
	fmt.Println(ngons)
	for _, arg := range intargs.IntArgs(flag.Args()) {
		found, cycle := ngons.FindCycle(arg)
		if found {
			fmt.Println("cycle: ", cycle)
			sum := 0
			for _, ngon := range cycle {
				sum += ngon.value
			}
			fmt.Println("sum: ", sum)
		}
	}

}
