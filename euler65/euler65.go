package main

import (
	"flag"
	"fmt"
	"math/big"

	"bitbucket.org/steventhurgood/euler/intargs"
)

// Returns the a0, a1, ... sequence of the continuous fraction representation of E:
// https://oeis.org/A003417
func A(n int) int64 {
	// [2; 1, 2, 1, 1, 4, ...
	// 0 = 2 - if n == 0, return 2
	// 1 = 1 - (if n - 2) % 3 == 0  - return 2 * ((n-2)/3) + 1
	// 2 = 2 - else return 1
	// 3 = 1
	// 4 = 1
	// 5 = 4
	// 6 = 1
	// 7 = 1
	// 8 = 6
	if n == 0 {
		return 2
	}
	if (n-2)%3 == 0 {
		return int64(2 * (((n - 2) / 3) + 1))
	}
	return 1
}

func EConvergent(n int) (*big.Int, *big.Int) {
	// p := make([]uint64, n+1)
	// q := make([]uint64, n+1)
	p := make([]*big.Int, n+1)
	q := make([]*big.Int, n+1)
	p[0] = big.NewInt(A(0))
	q[0] = big.NewInt(1)
	if n > 0 {
		p[1] = big.NewInt(3)
		q[1] = big.NewInt(1)
		for i := 2; i <= n; i++ {
			// fmt.Printf("p[%v] := %v  * %v + %v\n", i, A(i), p[i-1], p[i-2])
			// p[i] = A(i)*p[i-1] + p[i-2]
			p[i] = big.NewInt(0)
			p[i].Mul(big.NewInt(A(i)), p[i-1])
			p[i].Add(p[i], p[i-2])
			// fmt.Printf("q[%v] := %v  * %v + %v\n", i, A(i), q[i-1], q[i-2])
			// q[i] = A(i)*q[i-1] + q[i-2]
			q[i] = big.NewInt(0)
			q[i].Mul(big.NewInt(A(i)), q[i-1])
			q[i].Add(q[i], q[i-2])
		}
	}
	return p[n], q[n]
}

func DigitSum(n *big.Int) *big.Int {
	sum := big.NewInt(0)
	for n.Cmp(big.NewInt(0)) > 0 {
		r := big.NewInt(0)
		digit := big.NewInt(0)
		r.DivMod(n, big.NewInt(10), digit)
		sum.Add(sum, digit)
		n = r
	}
	return sum
}

func main() {
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		n, d := EConvergent(arg)
		fmt.Printf("%v/%v\n", n, d)
		fmt.Printf("numerator sum: %v\n", DigitSum(n))
	}
}
