package main

import (
	"math/big"
	"testing"
)

func TestA(t *testing.T) {
	want := []int64{
		2, 1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, 1, 1, 10,
	}
	for i := range want {
		got := A(i)
		if got != want[i] {
			t.Errorf("A(%v) -> %v. Want: %v", i, got, want)
		}
	}
}
func TestEConvergent(t *testing.T) {
	// https://oeis.org/A007676
	want_num := []int64{
		2, 3, 8, 11, 19, 87, 106, 193, 1264, 1457, 2721, 23225, 25946, 49171, 517656, 566827, 1084483, 13580623, 14665106, 28245729, 410105312, 438351041, 848456353, 14013652689, 14862109042, 28875761731, 534625820200, 563501581931, 1098127402131, 22526049624551,
	}
	// https://oeis.org/A007677
	want_denom := []int64{
		1, 1, 3, 4, 7, 32, 39, 71, 465, 536, 1001, 8544, 9545, 18089, 190435, 208524, 398959, 4996032, 5394991, 10391023, 150869313, 161260336, 312129649, 5155334720, 5467464369, 10622799089, 196677847971, 207300647060, 403978495031, 8286870547680, 8690849042711,
	}
	for i := range want_num {
		gotp, gotq := EConvergent(i)
		if gotp.Cmp(big.NewInt(want_num[i])) != 0 || gotq.Cmp(big.NewInt(want_denom[i])) != 0 {
			t.Errorf("Econvergent(%v) -> %v/%v. Want: %v/%v", i, gotp, gotq, want_num[i], want_denom[i])
		}
	}
}
