package main

import (
	"flag"
	"fmt"

	"bitbucket.org/steventhurgood/euler/intargs"
)

func IntToSlice(n int) []int {
	out := make([]int, 0, 10)
	for n > 0 {
		digit := n % 10
		out = append(out, digit)
		n = n / 10
	}
	return out
}

func SameDigits(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	var count [10]int
	for _, d := range a {
		count[d]++
	}
	for _, d := range b {
		count[d]--
	}
	for _, d := range count {
		if d != 0 {
			return false
		}
	}
	return true
}

// MinMultiplier - find the smallest x such that n*2 .. n*x all contain the same digits as n
func MinMultiplier(a int) int {
	i := 2
	aSlice := IntToSlice(a)
	for {
		b := a * i
		bSlice := IntToSlice(b)
		if !SameDigits(aSlice, bSlice) {
			return i - 1
		}
		i++
	}
}

// FindMInMultiplier - find the minimum number that can be multiplied by 1..n and retain the same digits.
// return the number, and the maximum number of multiplications possible.
func FindMinMultiplier(n int) (int, int) {
	i := 1
	for {
		m := MinMultiplier(i)
		if m >= n {
			return i, m
		}
		i++
	}
}

func main() {
	flag.Parse()
	for _, arg := range intargs.IntArgs(flag.Args()) {
		n, m := FindMinMultiplier(arg)
		fmt.Printf("%v (%v)\n", n, m)
		for i := 2; i <= m; i++ {
			fmt.Println(n * i)
		}
	}
}
