package main

import (
	"reflect"
	"testing"
)

func TestIntToSlice(t *testing.T) {
	n := 123450
	want := []int{0, 5, 4, 3, 2, 1}
	got := IntToSlice(n)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("IntToSlice(%v) => %v. Want: %v", n, got, want)
	}
}

func TestSameDigits(t *testing.T) {
	trueTests := [][][]int{
		[][]int{[]int{1, 2, 3, 4, 5}, []int{5, 4, 3, 2, 1}},
		[][]int{[]int{1, 0, 0, 0, 5}, []int{0, 0, 0, 5, 1}},
	}
	falseTests := [][][]int{
		[][]int{[]int{1, 2, 3, 4, 5}, []int{1, 2, 3, 4}},
		[][]int{[]int{1, 2, 3, 4, 5}, []int{1, 2, 3, 4, 6}},
		[][]int{[]int{1, 2, 3, 3, 4}, []int{1, 2, 3, 3, 3, 4}},
	}
	for _, test := range trueTests {
		a, b := test[0], test[1]
		if !SameDigits(a, b) {
			t.Errorf("SameDigits(%v, %v) = false. Expected true", a, b)
		}
	}
	for _, test := range falseTests {
		a, b := test[0], test[1]
		if SameDigits(a, b) {
			t.Errorf("SameDigits(%v, %v) = true. Expected false", a, b)
		}
	}
}

func TestMinMultiplier(t *testing.T) {
	n := 125874
	want := 2
	got := MinMultiplier(n)
	if got != want {
		t.Errorf("MinMultiplier(%v) => %v. Want: %v", n, got, want)
	}
}
