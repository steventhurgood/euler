package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

var ()

type Triangle [][]int

func (t *Triangle) String() string {
	b := bytes.NewBuffer([]byte{})
	for _, row := range *t {
		for _, value := range row {
			_, err := b.WriteString(fmt.Sprintf("%v ", value))
			if err != nil {
				log.Fatal(err)
			}
		}
		b.WriteString("\n")
	}
	return b.String()
}

type Direction int

const (
	Left  Direction = iota
	Right Direction = iota
	Stop  Direction = iota
)

type Path []Direction

type PathCost struct {
	cost int
	path Path
}

func (t *Triangle) FindMaxPaths() PathCost {
	if len(*t) <= 1 {
		return PathCost{}
	}
	row := len(*t) - 1
	paths := make([]PathCost, row+1)
	for i, v := range (*t)[row] {
		paths[i] = PathCost{
			cost: v,
			path: Path{Stop},
		}
	}
	for row = row - 1; row >= 0; row-- {
		newPaths := make([]PathCost, row+1)
		// for each item in the current row, we can either go left or right.
		// for left, the path and cost would be paths[i]
		// for right, the path and cost would be paths[i+1]
		for i, v := range (*t)[row] {
			left := paths[i]
			right := paths[i+1]
			maxPath := left
			maxDirection := Left
			if right.cost > left.cost {
				maxPath = right
				maxDirection = Right
			}
			newPath := Path{maxDirection}
			for _, dir := range maxPath.path {
				newPath = append(newPath, dir)
			}
			newPaths[i] = PathCost{
				cost: v + maxPath.cost,
				path: newPath,
			}
		}
		paths = newPaths
	}
	log.Print(paths)

	maxPath := PathCost{
		cost: -1,
		path: Path{},
	}
	for _, path := range paths {
		if path.cost > maxPath.cost {
			maxPath = path
		}
	}

	return maxPath
}

func (t *Triangle) AddRow(row []int) (*Triangle, error) {
	currentRows := len(*t)
	// if there are currently 0 rows, then the next row must be 1 long. Likewise for n rows, need n+1 entries.
	if len(row) != currentRows+1 {
		return nil, errors.New(fmt.Sprint("Adding row ", currentRows+1, "got incorrect length", len(row)))
	}
	*t = append(*t, row)
	return t, nil
}

func (t *Triangle) Get(row, column int) (int, error) {
	if row > len(*t) {
		return -1, errors.New(fmt.Sprint("Attempt to access row", row, "of triangle with", len(*t), "rows"))
	}
	if column > row {
		return -1, errors.New(fmt.Sprint("Attempt to access column", column, "of row", row))
	}
	return (*t)[row][column], nil
}

func ReadTriangle(r io.Reader) *Triangle {
	tri := &Triangle{}
	s := bufio.NewScanner(r)
	var err error
	for s.Scan() {
		line := s.Text()
		items := make([]int, 0, len(line))
		for _, w := range strings.Split(line, " ") {
			n, err := strconv.ParseInt(w, 10, 32)
			if err != nil {
				log.Fatal(err)
			}
			items = append(items, int(n))
		}
		tri, err = tri.AddRow(items)
		if err != nil {
			log.Fatal(err)
		}
	}
	return tri
}

func main() {
	tri := ReadTriangle(os.Stdin)
	path := tri.FindMaxPaths()
	fmt.Print(tri)
	fmt.Print(path)
}
