package main

import (
	"bytes"
	"testing"
)

func Test_AddRow_Error(t *testing.T) {
	tri := &Triangle{}
	tri, err := tri.AddRow([]int{1, 2})
	if err == nil {
		t.Error("Expected error when adding 2-long row as first row:", tri)
	}
	t.Log(tri)
}

func Test_AddRow(t *testing.T) {
	tri := &Triangle{}
	tri, err := tri.AddRow([]int{1})
	if err != nil {
		t.Error(err)
	}
	v, err := tri.Get(0, 0)
	if err != nil {
		t.Error(err)
	}
	if v != 1 {
		t.Error("Expected 1, got", v, tri)
	}
	t.Log(tri)
}

func Test_AddRow_Many(t *testing.T) {
	tri := &Triangle{}
	tri, _ = tri.AddRow([]int{1})
	tri, _ = tri.AddRow([]int{2, 3})
	tri, _ = tri.AddRow([]int{4, 5, 6})
	v, err := tri.Get(2, 1)
	if err != nil {
		t.Error(err)
	}
	if v != 5 {
		t.Error("Expected 5, got", v, tri)
	}
	t.Log(tri)
}

func Test_ReadTriangle(t *testing.T) {
	input := bytes.NewBufferString("1\n2 3\n4 5 6")
	tri := ReadTriangle(input)
	v, err := tri.Get(2, 1)
	if err != nil {
		t.Error(err)
	}
	if v != 5 {
		t.Error("Expected 5, got", v, tri)
	}
	t.Log(tri)
}

func Test_FindMaxPaths(t *testing.T) {
	input := bytes.NewBufferString("1\n2 3\n4 5 6")
	tri := ReadTriangle(input)
	path := tri.FindMaxPaths()
	if path.cost != 10 {
		t.Error("Expected cost 10, got", path.cost)
	}
	if len(path.path) < 3 {
		t.Error("Expected path: Right, Right, Stop. Got", path.path)
	}
	if path.path[0] != Right && path.path[1] != Right && path.path[2] != Stop {
		t.Error("Expected path: Right, Right. Got", path.path)
	}
}
