package main

import (
	"flag"
	"fmt"
	"math"
)

func IntPower(base, power int) int {
	n := 1
	for i := 0; i < power; i++ {
		n *= base
	}
	return n
}

func DigitPower(n, power int) int {
	sum := 0
	for n > 0 {
		digit := n % 10
		n = (n - digit) / 10
		sum += IntPower(digit, power)
	}
	return sum
}

func NumDigits(n int) int {
	digits := math.Ceil(math.Log10(float64(n)))
	return int(digits)
}

// The maximum digit that can be used for a number with this many digits.
func MaxDigits(power int) (int, int) {
	// the maximum number you can generate with n digits is ... ; while this number is less than n.
	n := 1
	for ; NumDigits(n*IntPower(9, power)) > n; n++ {
	}
	return n, n * IntPower(9, power)
}

func main() {
	power := flag.Int("p", 5, "power")
	maxDigits := flag.Bool("maxdigits", false, "print the max number of digits for power p")
	flag.Parse()
	if *maxDigits {
		digits, power := MaxDigits(*power)
		fmt.Println(digits, power)
		return
	}
	matches := []int{}
	_, maxPower := MaxDigits(*power)
	fmt.Println("max number:", maxPower)
	for i := 2; i < maxPower; i++ {
		v := DigitPower(i, *power)
		if v == i {
			matches = append(matches, i)
		}
	}
	sum := 0
	for _, m := range matches {
		fmt.Println(m)
		sum += m
	}
	fmt.Println("Sum:", sum)
}
