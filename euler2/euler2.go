package main

import (
	"flag"
	"fmt"
)

func Fib() chan int {
	ch := make(chan int)
	a, b := 1, 2
	go func() {
		for {
			r := a
			a = b
			b = a + r
			ch <- r
		}
	}()
	return ch
}

func main() {
	var upto = flag.Int("upto", 10, "the number up to which we want fibbonaci numbers to sum")
	flag.Parse()
	fib := Fib()
	sum := 0
	for n := <- fib; n < *upto; n = <- fib {
		if n % 2 == 0 {
		  sum += n
		}
	}
	fmt.Println(sum)
}
