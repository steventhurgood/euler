package main

// quadratic : n^2 + (a)n + b
// if a == b, n =a a : a * a + (an) + a, then no need to check n >= a
// if a !=b, n=ab -> (abab) + aab abb is divisable by a
//           n=a ->  a^a + (an) + b , only necessarily divisable if b = k.a for some k
//           n=b -> b^b + ab + b, only necessarily divisable if b = k.a for some k ; b%a == 0
//	     so if max |a| = 1000, and max |b| = 1000 then max ab = 1000,000, so need primes
//           up to 1M, so need to check numbers up to 1k

import (
	"flag"
	"fmt"
	"log"
	"math"
	"math/big"
)

type Sieve []bool

func NewSieve(n int) Sieve {
	log.Print("Making Sieve of Size: ", n)
	divisable := make(Sieve, n)
	divisable[0] = true
	divisable[1] = true

	maxNum := int(math.Floor(math.Sqrt(float64(n))))
	for i := 2; i <= maxNum; i++ {
		if !divisable[i] {
			for j := i + i; j < n; j += i {
				if i == 41 {
				}
				divisable[j] = true
			}
		}
	}
	return divisable
}

func (s *Sieve) IsPrime(n int) bool {
	if n < 0 {
		return false
	}
	i := big.NewInt(int64(n))
	return i.ProbablyPrime(100)
	if n >= len(*s) {
		log.Fatal("Cannot look up ", n, "in Sieve of size ", len(*s))
	}
	return !(*s)[n]
}

func FindMaxConsecutivePrimes(maxA, maxB int) (int, int, int, *Sieve) {
	// maxVal := (maxA * maxA * maxB * maxB) + (maxA * maxA) + maxB
	maxVal := 10000
	s := NewSieve(maxVal)
	maxConsecutive := -1
	maxConsecutiveA := -1
	maxConsecutiveB := -1

	for a := 1 - maxA; a < maxA; a++ {
		for b := 1 - maxB; b < maxB; b++ {
			if a%100 == 0 && b%100 == 0 {
				log.Print("a: ", a, " b: ", b)
			}
			consecutive := s.CountConsecutivePrimes(a, b)
			if consecutive > maxConsecutive {
				maxConsecutive = consecutive
				maxConsecutiveA = a
				maxConsecutiveB = b
			}
		}
	}
	return maxConsecutive, maxConsecutiveA, maxConsecutiveB, &s
}

func (s *Sieve) FindConsecutivePrimes(a, b int) (int, []int) {
	maxNumConsecutive := -1
	// f(n) - (n^2) + (an) + b
	isConsecutive := false
	consecutiveStart, maxConsecutiveStart, consecutiveEnd := -1, -1, -1
	numConsecutive := -1
	for n := 0; n < a*b; n++ {
		val := (n * n) + (a * n) + b
		if s.IsPrime(val) {
			if !isConsecutive {
				isConsecutive = true
				consecutiveStart = n
				numConsecutive = 1
			} else {
				numConsecutive++
			}
		} else {
			if isConsecutive {
				isConsecutive = false
				if numConsecutive > maxNumConsecutive {
					maxNumConsecutive = numConsecutive
					maxConsecutiveStart = consecutiveStart
					consecutiveEnd = n
				}
			}
		}
	}
	consecutive := make([]int, 0, consecutiveEnd-maxConsecutiveStart)
	for n := maxConsecutiveStart; n < consecutiveEnd; n++ {
		val := (n * n) + (a * n) + b
		consecutive = append(consecutive, val)
	}
	return maxConsecutiveStart, consecutive
}

func (s *Sieve) CountConsecutivePrimes(a, b int) int {
	maxNumConsecutive := -1
	// f(n) - (n^2) + (an) + b
	isConsecutive := false
	numConsecutive := -1
	for n := 0; n < a*b; n++ {
		val := (n * n) + (a * n) + b
		if s.IsPrime(val) {
			if !isConsecutive {
				isConsecutive = true
				numConsecutive = 1
			} else {
				numConsecutive++
			}
		} else {
			if isConsecutive {
				isConsecutive = false
				if numConsecutive > maxNumConsecutive {
					maxNumConsecutive = numConsecutive
				}
			}
		}
	}
	return maxNumConsecutive
}

func main() {
	maxA := flag.Int("max_a", 10, "maximum value of |a|")
	maxB := flag.Int("max_b", 10, "maximum value of |b|")
	flag.Parse()
	max, a, b, s := FindMaxConsecutivePrimes(*maxA, *maxB)
	n, primes := s.FindConsecutivePrimes(a, b)
	fmt.Println("max =", max, ", a =", a, ", b =", b, ", n =", n, ", primes =", primes)
}
