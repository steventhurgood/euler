package main

import (
	"testing"
)

var (
	primes []int = []int{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}
)

func TestIsPrime(t *testing.T) {
	testPrimes := make(map[int]bool)
	for _, p := range primes {
		testPrimes[p] = true
	}
	s := NewSieve(100)
	for i := 1; i < 100; i++ {
		_, expectedPrime := testPrimes[i]
		actual := s.IsPrime(i)
		if expectedPrime != actual {
			t.Error("Testing ", i, " expected: ", expectedPrime, " actual: ", actual)
		}
	}
}

func TestCountConsecutivePrimes(t *testing.T) {
	a := 1
	b := 41

	maxVal := (a * a * b * b) + (a * a) + b
	s := NewSieve(maxVal)
	m := s.CountConsecutivePrimes(a, b)
	if m != 40 {
		t.Error("Expected 40 consecutive primes. Got: ", m)
	}

}
