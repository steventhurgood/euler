package main

import (
	"flag"
	"fmt"
	"bitbucket.org/steventhurgood/euler/intargs"
	"bitbucket.org/steventhurgood/euler/prime"
	"log"
)

func Quadratic(n, a, b int) int {
	return n*n + a*n + b
}
func LongQuadratic(a, b int, s *prime.Sieve) int {
	n := 0
	for {
		v := Quadratic(n, a, b)
		isPrime, err := s.IsPrime(v)
		if err != nil {
			log.Print("error. a: ", a, " b: ", b, " n: ", n, " v: ", v)
			log.Print(err)
		}
		if !isPrime {
			break
		}
		n++
	}
	return n
}

func main() {
	upto := flag.Int("upto", -1, "test quadratics up to")
	sieveSize := flag.Int("sieve_size", 100000, "the size of the sieve to use")
	flag.Parse()

	s := prime.NewSieve(*sieveSize)
	if *upto > 0 {
		longest := -1
		longestA := 0
		longestB := 0
		for b := 0; b < *upto; b++ {
			isPrime, err := s.IsPrime(b)
			if err != nil {
				log.Print(err)
				continue
			}
			if !isPrime {
				continue
			}
			for a := 0; a < *upto; a++ {
				gcd := prime.Gcd(a, b)
				if gcd == 1 || gcd > longest {
					long := LongQuadratic(a, b, s)
					if long > longest {
						longest = long
						longestA = a
						longestB = b
					}
					long = LongQuadratic(-a, b, s)
					if long > longest {
						longest = long
						longestA = -a
						longestB = b
					}
					long = LongQuadratic(a, -b, s)
					if long > longest {
						longest = long
						longestA = a
						longestB = -b
					}
					long = LongQuadratic(-a, -b, s)
					if long > longest {
						longest = long
						longestA = -a
						longestB = -b
					}
				}
			}
		}
		fmt.Println("longest a: ", longestA, " b: ", longestB, " length: ", longest, "product: ", longestA*longestB)
	} else {
		args := intargs.IntArgs(flag.Args())
		if len(args) != 2 {
			log.Fatal("Usage: euler27 a b")
		}
		n := 0
		a, b := args[0], args[1]
		fmt.Println("n^2 + ", a, "n + ", b)
		for {
			v := Quadratic(n, a, b)
			isPrime, err := s.IsPrime(v)
			if err != nil {
				log.Print("error. a: ", a, " b: ", b, " n: ", n, " v: ", v)
				log.Print(err)
			}
			fmt.Println(n, v, isPrime)
			if !isPrime {
				break
			}
			n++
		}
	}
}
