package main

import (
	"reflect"
	"testing"
)

var (
	tests = [][]int{
		[]int{0, 1},
		[]int{1, 1},
		[]int{9, 1},
		[]int{99, 2},
		[]int{12345, 5},
	}
	maxN       = 99999999
	maxNDigits = 8
)

func TestLogDigits(t *testing.T) {
	for _, test := range tests {
		n := test[0]
		want := test[1]
		got := LogDigits(n)
		if got != want {
			t.Errorf("LogDigits(%v) => %v. Want: %v", n, got, want)
		}
	}
}

func TestWhileDigits(t *testing.T) {
	for _, test := range tests {
		n := test[0]
		want := test[1]
		got := WhileDigits(n)
		if got != want {
			t.Errorf("WhileDigits(%v) => %v. Want: %v", n, got, want)
		}
	}
}

func BenchmarkLogDigits(b *testing.B) {
	for i := 0; i < b.N; i++ {
		n := i % maxN
		LogDigits(n)
	}
}

func BenchmarkWhileDigits(b *testing.B) {
	for i := 0; i < b.N; i++ {
		n := i % maxN
		WhileDigits(n)
	}
}

func TestNewDigitCounter(t *testing.T) {
	want := DigitCounter{
		[2]int{9, 1},
		[2]int{99, 2},
		[2]int{999, 3},
	}
	got := NewDigitCounter(3)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("NewDigitCounter(%v) => %v. Want: %v", 3, got, want)
	}
}

func TestLinearDigits(t *testing.T) {
	count := NewDigitCounter(5)
	for _, test := range tests {
		n := test[0]
		want := test[1]
		got := count.LinearDigits(n)
		if got != want {
			t.Errorf("count.LinearDigits(%v) => %v. Want: %v", n, got, want)
		}
	}
}

/*func TestBinarySearchDigits(t *testing.T) {
	count := NewDigitCounter(5)
	for _, test := range tests {
		n := test[0]
		want := test[1]
		got := count.BinarySearchDigits(n)
		if got != want {
			t.Errorf("count.BinarySearchDigits(%v) => %v. Want: %v", n, got, want)
		}
	}
} */

func BenchmarkLinearCountDigits(b *testing.B) {
	b.StopTimer()
	count := NewDigitCounter(maxNDigits)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		n := i % maxN
		count.LinearDigits(n)
	}
}

func TestPrintDigit(t *testing.T) {
	for _, test := range tests {
		n := test[0]
		want := test[1]
		got := PrintDigit(n)
		if got != want {
			t.Errorf("WhileDigits(%v) => %v. Want: %v", n, got, want)
		}
	}
}

func BenchmarkPrintDigits(b *testing.B) {
	for i := 0; i < b.N; i++ {
		n := i % maxN
		PrintDigit(n)
	}
}
