package main

import (
	"fmt"
	"math"
)

func LogDigits(n int) int {
	if n == 0 || n == 1 {
		return 1
	}
	return int(math.Ceil(math.Log10(float64(n))))
}

func WhileDigits(n int) int {
	count := 0
	if n == 0 {
		return 1
	}
	for n > 0 {
		n /= 10
		count++
	}
	return count
}

// array of max n ot number of digits. eg:
// {9, 1}, {99, 2}, {999, 3}, {9999, 4}, ....

type DigitCounter [][2]int

func NewDigitCounter(maxDigits int) DigitCounter {
	counter := make(DigitCounter, maxDigits)
	base := 10
	for i := 0; i < maxDigits; i++ {
		counter[i] = [2]int{base - 1, i + 1}
		base *= 10
	}
	return counter
}

func (c DigitCounter) LinearDigits(n int) int {
	i := 0
	for c[i][0] < n {
		i++
	}
	return c[i][1]
}

/*
 0  1   2    3     4
[9, 99, 999, 9999, 99999]
[0-9], [10-99], [100-999], [1000-9999], ...

min=0, max=5
mid=2 [999]999

if n <= c[mid]:
  max = mid
else:
  min = mid
mid = (min + max)/2

1: max=2, mid=1; max=1; mid=0 : answer = 0
9: max=2, mid=1; max=1; mid=0 : answer = 0
60: max=2, mid=1; max=1; mid=0: answer = 1
99: max=2, mid=1; answer = 1
12345: min=2, mid=3; min=3, mid=4; min=4

if n <= c[mid] : return mid
if n > c[mid]: return mid+1
*/

func (c DigitCounter) BinarySearchDigits(n int) int {
	min, max := 0, len(c)
	mid := 0
	for min != max-1 {
		mid = (min + max) / 2
		fmt.Println(min, mid, max, n)
		if n <= c[mid][0] {
			max = mid
		} else {
			min = mid
		}
	}
	fmt.Println("found: ", min, mid, max, n)
	return c[min][1]
}

func PrintDigit(n int) int {
	s := fmt.Sprint(n)
	return len(s)
}
